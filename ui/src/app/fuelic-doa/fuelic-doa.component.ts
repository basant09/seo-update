import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';

@Component({
  selector: 'app-fuelic-doa',
  templateUrl: './fuelic-doa.component.html',
  styleUrls: ['./fuelic-doa.component.css']
})
export class FuelicDoaComponent implements OnInit {
  highlights : any;
  about    : any;
  mobileFeatured : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {
    /* Page title */
    this.pageTitleService.setTitle("Drive Off Alert");

    /* Page subTitle */
    this.pageTitleService.setSubTitle("Drive-Off-Alert integrated with highly efficient Automatic License Plate Recognition to prevent fuel theft.");

    this.service.getDoaContent().
    subscribe(response => {this.highlights = response},
      err       => console.log(err),
      ()        =>this.highlights
    );

    this.service.getDoaDetail().
    subscribe(response => {this.about = response},
      err      => console.log(err),
      ()       => this.about
    );
    this.service.getDoaContent().
    subscribe(response => {this.mobileFeatured = response},
      err      => console.log(err),
      ()       => this.mobileFeatured
    );
  }

  ngOnInit() {
    this.titleService.setTitle('Drive-Off-Alert Solution for Petrol Stations - Oodak');
   this.meta.addTag({name: 'description', content: 'We have partnered with Drive-off-alert to provide a fully automated solution for fuel theft prevention. Drive-off-alert is fully integrated with Automatic License plate detection technology and raise alert for the possible offenders in real-time.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

}
