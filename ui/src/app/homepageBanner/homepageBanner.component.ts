import { Component, OnInit } from '@angular/core';
import { ChkService } from '../service/chk.service';
declare var $ : any;

@Component({
  selector: '[chankya-homepageBanner]',
  templateUrl: './homepageBanner.component.html',
  styleUrls: ['./homepageBanner.component.scss']
})

export class homepageBannerComponent implements OnInit {
  bannerItems : any;
  constructor(private service:ChkService ) {

  }
  ngOnInit() {
    this.service.getHomeBanner().
    subscribe(response => {this.bannerItems = response},
      err      => console.log(err),
      ()       => this.bannerItems
    );
  }

}
