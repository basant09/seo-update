import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
@Component({
  selector: 'chankya-lpr',
  templateUrl: './lpr.component.html',
  styleUrls: ['./lpr.component.scss']
})
export class LprComponent implements OnInit {

  /* Variables */
  lprContent : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {

    /* Page title */
    this.pageTitleService.setTitle(" Automatic License Plate Recognition ");

    /* Page subTitle */
    this.pageTitleService.setSubTitle(" State of the art ALPR technology for advance Security, Surveillance, Crime Prevention and Access Controls ");

    this.service.getLprContent().
      subscribe(response => {this.lprContent = response},
                err => console.log(err),
                () => this.lprContent
            );

  }

  ngOnInit() {
    this.titleService.setTitle('Automatic Number Plate Recognition Melbourne | ALPR and ANPR System');
    this.meta.addTag({name: 'description', content: 'Oodak offer accurate, simple, cost-effective and efficient ALPR technology with advance security, surveillance, crime prevention and access controls. We provide the ALPR solutions for Parking lot and Shopping Malls.'});
   this.meta.addTag({name: 'robots', content: 'index, follow'});

  }

}
