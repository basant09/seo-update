import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
@Component({
  selector: 'chankya-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  /* Variables */
  contact : any;

  constructor( private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta ) {

    /* Page title */
    this.pageTitleService.setTitle(" Lets Get In Touch ");

    /* Page subTitle */
    this.pageTitleService.setSubTitle("");

    this.service.getContactContent().
      subscribe(response => {this.contact = response},
                err      => console.log(err),
                ()       => this.contact
            );
  }

  ngOnInit() {
    this.titleService.setTitle('Contact Us - Oodak');
   this.meta.addTag({name: 'description', content: 'Connect with Oodak, for commercial & residential CCTV surveillance solutions with face detection, license plate recognition, vandalism protection, security alarms system in Melbourne.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  
  }

}
