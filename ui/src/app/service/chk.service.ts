import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import 'rxjs/Rx';




@Injectable()
export class ChkService {

   constructor( private http: Http) { }

   /*
    * Get the content of home page.
    */
   getHomeContent()
   {
      return this.http.get("assets/data/home.json").map(response => response.json().data);
   }

   /*
    * Get services
    */
   getServices()
   {
     return this.http.get("assets/data/service.json").map(response => response.json().data);
   }


   /*
    * Get posts
    */
   getPosts()
   {
      return this.http.get("assets/data/fs-grid.json").map(response => response.json().data);
   }

   /*
    * Get project gallary
    */
   getProjectGallary()
   {
      return this.http.get("assets/data/project-gallary.json").map(response => response.json().data);
   }


   /*
    * Get blog column2.
    */
   getBlogColumn2()
   {
      return this.http.get("assets/data/blog-column2.json").map(response => response.json().data);
   }

   /*
    * Get blog column3.
    */
   getBlogColumn3()
   {
      return this.http.get("assets/data/blog-column3.json").map(response => response.json().data);
   }

   /*
    * Get blog listing sidebar.
    */
   getBlogListingSideBar()
   {
      return this.http.get("assets/data/blog-listing-sidebar.json").map(response => response.json().data);
   }


   /*
    * Get blog masonary2
    */
   getBlogMasonary2()
   {
      return this.http.get("assets/data/blog-masonary2.json").map(response => response.json().data);
   }

   /*
    * Get blog masonary3
    */
   getBlogMasonary3()
   {
      return this.http.get("assets/data/blog-masonary3.json").map(response => response.json().data);
   }

   /*
    * Get blog no sidebar
    */
   getBlogNoSideBar()
   {
      return this.http.get("assets/data/blog-no-sidebar.json").map(response => response.json().data);
   }

   /*
    * Get blog sidebar
    */
   getBlogSidebar()
   {
      return this.http.get("assets/data/blog-sidebar.json").map(response => response.json().data);
   }

   /*
    * Get blog sidebar
    */
   getPopularPosts()
   {
      return this.http.get("assets/data/popular-posts.json").map(response => response.json().data);
   }

   /*
    * Get instagram images
    */
   getInstagramImages()
   {
      return this.http.get("assets/data/instagram.json").map(response => response.json().data);
   }

   /*
    * Get featured post
    */
   getFeaturedPost()
   {
      return this.http.get("assets/data/featured.json").map(response => response.json().data);
   }

   /*
    * Get testimonial
    */
   getTestimonial()
   {
      return this.http.get("assets/data/testimonial.json").map(response => response.json().data);
   }

   /*
    * Get portfolio-v1.
    */
   getPortfolioV1()
   {
    return this.http.get("assets/data/portfolio-v1.json").map(response => response.json().data);
   }

   /*
    * Get portfolio-v2.
    */
   getPortfolioV2()
   {
      return this.http.get("assets/data/portfolio-v2.json").map(response => response.json().data);
   }

   /*
    * Get portfolio-v3.
    */
   getPortfolioV3()
   {
      return this.http.get("assets/data/portfolio-v3.json").map(response => response.json().data);
   }

  /*
   * Get the content for Visit Management Page.
   */
  getVisitManagement()
  {
      return this.http.get("assets/data/visit-management.json").map(response => response.json().data);
   }

   /*
   * Get the content of pricing page.
   */
   getPricingPageContent()
   {
    return this.http.get("assets/data/pricing.json").map(response => response.json().data);
   }


   /*
   * Get the content of features page.
   */
   getFeaturesContent()
   {
    return this.http.get("assets/data/features.json").map(response => response.json().data);
   }

   /*
   * Get the content of Parking Management page.
   */
   getParkingManagementContent()
   {
    return this.http.get("assets/data/parking-management.json").map(response => response.json().data);
   }

/*
   * Get the content of Business Setup page.
   */
   getBusinessSetupContent()
   {
    return this.http.get("assets/data/setup.json").map(response => response.json().data);
   }

/*
   * Get the content of LPR page.
   */
   getLprContent()
   {
    return this.http.get("assets/data/lpr.json").map(response => response.json().data);
   }

/*
   * Get the content of Industries page.
   */
   getIndustries()
   {
    return this.http.get("assets/data/industries.json").map(response => response.json().data);
   }

   /*
   * Get the content of CAC page.
   */
   getCacContent()
   {
    return this.http.get("assets/data/cac.json").map(response => response.json().data);
   }

   /*
   * Get the categories.
   */
   getcategories()
   {
    return this.http.get("assets/data/categories.json").map(response => response.json().data);
   }

   /*
   * Get the content of contact page.
   */
   getContactContent()
   {
    return this.http.get("assets/data/contact_new.json").map(response => response.json().data);
   }

   /*
   * Get the content of search page.
   */
   getSearchContent()
   {
    return this.http.get("assets/data/search.json").map(response => response.json().data);
   }

   /*
   * Get the team.
   */
   getTeam()
   {
      return this.http.get("assets/data/tech-expertise.json").map(response => response.json().data);
   }

   /*
    * Get about.
    */
   getAbout()
   {
      return this.http.get("assets/data/about.json").map(response => response.json().data);
   }

   /*
   * Get the content of support page.
   */
   getSupportContent()
   {
    return this.http.get("assets/data/support.json").map(response => response.json().data);
   }

   /*
    * Get home testimonial
    */
   getHomeTestimonial()
   {
      return this.http.get("assets/data/home-testimonial.json").map(response => response.json().data);
   }

  /*
   * Get the content of home page.
   */
    getHomeBanner()
    {
      return this.http.get("assets/data/home-banner.json").map(response => response.json().data);
    }
   /*
    * Get footer logo list
    */
   getFooterLogoList()
   {
      return this.http.get("assets/data/footer-logo.json").map(response => response.json().data);
   }

  /*
   * Get the social share list
   */
   getSocialShare()
   {
    return this.http.get("assets/data/social-share.json").map(response => response.json().data);
   }

  /*
   * Get the recent comments
   */
   getRecentComments()
   {
    return this.http.get("assets/data/recent-comments.json").map(response => response.json().data);
   }

   /*
    * Get contact us widgets
    */
   getContactUsWidgets()
   {
      return this.http.get("assets/data/contact-us-widgets.json").map(response => response.json().data);
   }

   /*
    * Get cart
    */
   getCart()
   {
      return this.http.get("assets/data/cart.json").map(response => response.json().data);
   }

   /*
    * Get tweets
    */
   getTweets()
   {
      return this.http.get("assets/data/latest-tweets.json").map(response => response.json().data);
   }

   /*
    * Get tags
    */
   getTags()
   {
      return this.http.get("assets/data/tags.json").map(response => response.json().data);
   }

   /*
    * Get archive
    */
   getArchive()
   {
      return this.http.get("assets/data/archive.json").map(response => response.json().data);
   }

   /*
    * Get about the author
    */
   getAboutAuthor()
   {
      return this.http.get("assets/data/about-author.json").map(response => response.json().data);
   }

   /*
    * Get about the author
    */
   getTabContent()
   {
      return this.http.get("assets/data/tab-content.json").map(response => response.json().data);
   }

   /*
    * Get products list
    */
   getProductsList()
   {
      return this.http.get("assets/data/products-list.json").map(response => response.json().data);
   }

   /*
    * Get related products list
    */
   getRelatedProducts()
   {
      return this.http.get("assets/data/related-products.json").map(response => response.json().data);
   }

   /*
   Get App Download Section
    */
    getFuelicAppDownload()
    {
      return this.http.get("assets/data/fuelic-app-download.json").map(response => response.json().data);
    }
    /*
     Get Fuelic Station Login Section
    */
    getFuelicLoginFS()
    {
      return this.http.get("assets/data/fuelic-login-fs.json").map(response => response.json().data);
    }
    /*
         Get Fuelic Station Login Section
        */
    getFuelicLoginFM()
    {
      return this.http.get("assets/data/fuelic-login-fm.json").map(response => response.json().data);
    }

   /* Get Content for Fuelic Pages PG Highlights */
    getFuelicHighlightsPG()
    {
      return this.http.get("assets/data/fuelic-features.json").map(response => response.json().data);
    }

    /* Get Content for RFID content */
    getRfidContent()
    {
      return this.http.get("assets/data/rfid.json").map(response => response.json().data);
    }

  /* Get Content for EAI */
    getEaiContent()
    {
      return this.http.get("assets/data/eai.json").map(response => response.json().data);
    }
  /* Get Content for EAI detail*/
    getEaiDetail()
    {
      return this.http.get("assets/data/eai-detail.json").map(response => response.json().data);
    }
  /* Get Content for Integration */
    getIntegrationContent()
    {
      return this.http.get("assets/data/integration.json").map(response => response.json().data);
    }
  /* Get Content for Integration detail*/
    getIntegrationDetail()
    {
      return this.http.get("assets/data/integration-detail.json").map(response => response.json().data);
    }

   /* Get Content for Fuelic DOA */
    getDoaContent()
    {
      return this.http.get("assets/data/doa.json").map(response => response.json().data);
    }

    /* Get Content for Fuelic Pages  PG*/
    getFuelicPageContentPG()
    {
      return this.http.get("assets/data/fuelic-summary.json").map(response => response.json().data);
    }

  /* Get Content for DOA detail*/
    getDoaDetail()
    {
      return this.http.get("assets/data/doa-detail.json").map(response => response.json().data);
    }

    /* Get Content for Fuelic Pages FM Highlights */
    getFuelicHighlightsFM()
    {
      return this.http.get("assets/data/fuelic-highlights-fm.json").map(response => response.json().data);
    }

    /* Get Content for Fuelic Pages  FM*/
    getFuelicPageContentFM()
    {
      return this.http.get("assets/data/fuelic-page-content-fm.json").map(response => response.json().data);
    }

    /* Get Content for Fuelic Pages FS Highlights */
    getFuelicHighlightsFS()
    {
      return this.http.get("assets/data/fuelic-highlights-fs.json").map(response => response.json().data);
    }

    /* Get Content for Fuelic Pages  FS*/
    getFuelicPageContentFS()
    {
      return this.http.get("assets/data/fuelic-page-content-fs.json").map(response => response.json().data);
    }

    /* Submit Request Demo Form*/
    submitRDForm(formData)
    {
    const httpOptions = {
    headers: new Headers({
      'Content-Type':  'application/json'
    })
  };
      return this.http.post('https://flrnhjouze.execute-api.us-east-1.amazonaws.com/Production/contact-us', formData, httpOptions)
        .map(response => response.json().data);
    }
}
