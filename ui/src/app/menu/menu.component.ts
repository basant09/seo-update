import { Component, OnInit, HostListener, Inject, } from '@angular/core';
import { MenuItems } from '../core/menu/menu-items/menu-items';
import { Router } from '@angular/router';
declare var $ : any;
@Component({
  selector: '[chankya-menu]',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})


export class MenuComponent implements OnInit {
	searchactive: boolean = false;

  constructor(public menuItems: MenuItems, public router: Router) {
  }

	ngOnInit() {}

	getLocation(){
		if ((window.location.hostname).includes('fuelic')) {
      return ('FUELIC');
    }
  else{
    return ('OODAK');
  }
	}

  searchActiveFunction(){
		this.searchactive = !this.searchactive;
	}

	onClickOutside(event:Object) {
    if(event && event['value'] === true) {
      this.searchactive = false;
    }
   }
  scrolltoNPD(e)
  {
    const currPath = window.location.pathname;
    if(currPath ==='/home'){
      document.getElementById('fuelicNPD').scrollIntoView();
    } else{
      window.location.pathname = '/home'
    }
  }
  scrollToLink(scroll_target)
  {
    const currPath = window.location.pathname;
    if(currPath === '/home'){
      document.getElementById(scroll_target).scrollIntoView();
      //window.scrollBy(0, -10);
    }
    else{
      //window.location.pathname = link;
      //document.getElementById(scroll).scrollIntoView();
    }
  }
}
