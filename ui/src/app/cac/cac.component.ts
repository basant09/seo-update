import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
@Component({
  selector: 'chankya-cac',
  templateUrl: './cac.component.html',
  styleUrls: ['./cac.component.scss']
})
export class CacComponent implements OnInit {

   /* Variables */
  cac : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {

      /* Page title */
      this.pageTitleService.setTitle(" Community Safety Solutions ");

      /* Page subTitle */
      this.pageTitleService.setSubTitle("We are one of the solution providers for Community Safety Solutions. We work with Community Against Crime (or CAC) which is a Not-For-Profit organisation and run various programs to control crime.");

      this.service.getCacContent().
       subscribe(response => {this.cac = response},
                  err => console.log(err),
                  () => this.cac
              );
  }

  ngOnInit() {
    this.titleService.setTitle('Community Safety Solutions - Oodak');
   this.meta.addTag({name: 'description', content: 'We are one of the solution providers in Melbourne for Community Safety Solutions. We work with Community Against Crime (CAC) to provide Technical Solutions & Support for Community Safety.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

}
