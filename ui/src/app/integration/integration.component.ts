import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
@Component({
  selector: 'app-integration',
  templateUrl: './integration.component.html',
  styleUrls: ['./integration.component.css']
})
export class IntegrationComponent implements OnInit {
  highlights : any;
  about    : any;
  mobileFeatured : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {
    /* Page title */
    this.pageTitleService.setTitle("Integration with Enterprise Applications");

    /* Page subTitle */
    this.pageTitleService.setSubTitle("Right tools and skillset to integrate with your Enterprise Applications");

    this.service.getIntegrationContent().
    subscribe(response => {this.highlights = response},
      err       => console.log(err),
      ()        =>this.highlights
    );

    this.service.getIntegrationDetail().
    subscribe(response => {this.about = response},
      err      => console.log(err),
      ()       => this.about
    );
    this.service.getIntegrationContent().
    subscribe(response => {this.mobileFeatured = response},
      err      => console.log(err),
      ()       => this.mobileFeatured
    );
  }

  ngOnInit() {
    this.titleService.setTitle('Integration with Enterprise Applications - Oodak');
   this.meta.addTag({name: 'description', content: 'We build and tailor the applications to fit the needs of your business. We have strong experience in leading integration technologies such as webMethods, Tibco, Mulesoft etc. thus leveraging the experience to design appropriate solutions for the consultants.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

}
