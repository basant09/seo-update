import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
@Component({
  selector: 'app-eai',
  templateUrl: './eai.component.html',
  styleUrls: ['./eai.component.css']
})
export class EAIComponent implements OnInit {
  highlights : any;
  about    : any;
  mobileFeatured : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {
    /* Page title */
    this.pageTitleService.setTitle("Enterprise Architecture Consulting");

    /* Page subTitle */
    this.pageTitleService.setSubTitle("Your Enterprise need to be in safe hands to deliver progressive solutions");

    this.service.getEaiContent().
    subscribe(response => {this.highlights = response},
      err       => console.log(err),
      ()        =>this.highlights
    );

    this.service.getEaiDetail().
    subscribe(response => {this.about = response},
      err      => console.log(err),
      ()       => this.about
    );
    this.service.getEaiContent().
    subscribe(response => {this.mobileFeatured = response},
      err      => console.log(err),
      ()       => this.mobileFeatured
    );
  }

  ngOnInit() {
    this.titleService.setTitle('Enterprise Architecture Consulting - Oodak');
   this.meta.addTag({name: 'description', content: 'With our strong experience in developing baseline architecture and building the blueprint for the target architecture while ensuring to align with the objectives, goals and business strategies. Our team has capability to tailor enterprise frameworks and make them work in Agile environment.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

}
