import { Injectable } from '@angular/core';

/*
 * Menu interface
 */
export interface Menu {
	state: string;
	name?: string;
	type?: string;
	icon?: string;
	children?: ChildrenItems[];
}

/*
 * Children menu interface
 */
export interface ChildrenItems {
  	state: string;
  	name: string;
  	type?: string;
}

const HEADERMENUITEMS = [
  {
      state: "home",
      name: "Home",
      type:"link"
   },
  {
      state:"",
      name:"Solutions",
      type:"sub",
      icon: 'fa fa-caret-down',
      children: [
         { state: 'visitor-management', name: 'Visitor Management', type:"link"},
         { state: 'parking-management', name: 'Parking Management', type:"link"},
         { state: 'cac', name: 'Community Safety', type:"link"},
         { state: 'doa', name: 'Fuel Theft Prevention', type:"link"},
         { state: 'pay-go', name: 'Pay At Pump', type:"link"},
         { state: 'services', name: 'CCTV Surveillance', type:"link"}
      ]
   },
   {
      state:"",
      name:"Technology",
      type:"sub",
      icon: 'fa fa-caret-down',
      children: [
         { state: 'lpr', name: 'LPR Controls', type:"link"},
         { state: 'rfid', name: 'RFID Controls', type:"link"},
         { state: 'pay-go', name: 'Pay At Pump', type:"link"}
      ]
   },
{
      state:"",
      name:"Enterprise",
      type:"sub",
      icon: 'fa fa-caret-down',
      children: [
         { state: 'integration', name: 'Integration', type:"link"},
         { state: 'eai', name: 'Architecture', type:"link"}
      ]
   },
   {
      state:"industries",
      name:"Industries",
      type:"link",
     /* icon: 'fa fa-caret-down',
      children: [
         { state: 'apartment', name: 'Hotels & Apartments', type:"link"},
         { state: 'corporates', name: 'Corporates', type:"link"},
         { state: 'business', name: 'Business Parks', type:"link"},
         { state: 'council', name: 'Councils', type:"link"},
         { state: 'edu', name: 'Educational Institutes', type:"link"},
         { state: 'hospitals', name: 'Hospitals', type:"link"},
         { state: 'fuel_stations', name: 'Service Stations', type:"link"},
         { state: 'residents', name: 'Home Residents', type:"link"}
      ]
    */
   },
  {
      state:"services",
      name:"Services",
      type:"link",
      /*icon: 'fa fa-caret-down',
      children: [
         { state: 'cctv', name: 'CCTV Surveillance', type:"link"},
         { state: 'pos', name: 'Point Of Sales', type:"link"},
         { state: 'alarm', name: 'Alarm System', type:"link"},
         { state: 'signage', name: 'Digital Signage', type:"link"},
         { state: 'phone', name: 'IP Phones', type:"link"},
         { state: 'comms', name: 'Networking', type:"link"},
         { state: 'server', name: 'Server Setup', type:"link"},
         { state: 'av', name: 'Audio Visual Setup', type:"link"}
      ]
    */
   },
/*{
    state:"pay-go",
    name:"pp",
    type:"link"
  },
   {
      state:"fuel-station",
      name:"FS",
      type:"link"
   },
*/

    ];

const FOOTERMENU = [
   {
      state: "home",
      name: "Home",
      type:"link"
   },

   {
      state:"",
      name:"About Us",
      type:"link"
   },

      /*
   {
      state:"blog",
      name:"Blog",
      type:"link"
   },*/
   {
      state:"contact",
      name:"Contact",
      type:"link"
   },
   {
      state:"",
      name:"Career",
      type:"link"
   }
]

const EXPLOREMENU = [
   {
      state: "home",
      name: "Dashboard",
      type:"link"
   },
   {
      state: "sign-in",
      name: "Sign In",
      type:"link"
   },
   {
      state: "sign-up",
      name: "Sign Up",
      type:"link"
   },
   {
      state: "helpdesk",
      name: "Helpdesk",
      type:"link"
   },
   {
      state: "privacy-policy",
      name: "Privacy Policy",
      type:"link"
   },
   {
      state: "terms-conditions",
      name: "Terms & Conditions ",
      type:"link"
   }
];

const FOOTERMENU2 = [
  {
    state:"about",
    name:"About Us",
    type:"link"
  },

  /*
  {
    state:"blog",
    name:"Blog",
    type:"link"
  },
  */
  {
    state:"contact",
    name:"Contact",
    type:"link"
  },
  {
    state:"terms-conditions",
    name:"Terms & Conditions",
    type:"link"
  },
  {
    state:"privacy-policy",
    name:"Privacy Policy",
    type:"link"
  }
];

@Injectable()
export class MenuItems {

	 /*
		* Get all header menu
		*/
	 getMainMenu(): Menu[] {
			return HEADERMENUITEMS;
	 }
   /*
    * Get footer menu
    */
   getFooterMenu(): Menu[] {
      return FOOTERMENU;
   }

   /*
    * Get the explore menu
    */
   getExploreMenu(): Menu[] {
      return EXPLOREMENU;
   }

   /*
    * Get the footer2
    */
   getFooter2(): Menu[] {
      return FOOTERMENU2;
   }

}
