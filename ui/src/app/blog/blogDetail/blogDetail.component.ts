import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { ChkService } from '../../service/chk.service';

@Component({
  selector: 'chankya-blogDetail',
  templateUrl: './blogDetail.component.html',
  styleUrls: ['./blogDetail.component.scss']
})
export class BlogDetailComponent implements OnInit {

   /* Variables */
  blogDetail       : any;
  categories       : any;
  popularPosts     : any;
  instagramGallary : any;
  tags             : any;
  link             :any;

	constructor(private pageTitleService: PageTitleService, private service:ChkService) {



	  /* Page subTitle */
	  this.pageTitleService.setSubTitle(" ");

	  	this.service.getPosts().
	   	subscribe(response => {this.blogDetail = response,this.findIndex(response)},
            err => console.log(err),
            () => this.blogDetail
        );
	}

  ngOnInit() {
  }
  findIndex(response){
    this.link = window.location.href.substring(window.location.href.indexOf("blog-detail")+12);    /* ur id */

    let index = response.findIndex(x => x.link == this.link);

    if(index>=0){    /* if there is value in the array that matches the id value*/
      this.blogDetail = this.blogDetail[index];
      /* Page title */
      this.pageTitleService.setTitle(this.blogDetail.title);

    }
  }
}
