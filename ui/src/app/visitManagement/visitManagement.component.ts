import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import { Title, Meta }     from '@angular/platform-browser';

@Component({
  selector: 'chankya-visitManagement',
  templateUrl: './visitManagement.component.html',
  styleUrls: ['./visitManagement.component.scss']
})
export class visitManagementComponent implements OnInit {

   /* Variables */
   visitManagement : any;

   constructor(private service:ChkService, private pageTitleService:PageTitleService, private titleService: Title,
      private meta: Meta) {

      /* Page title */
      this.pageTitleService.setTitle(" Visitor Management");

      /* Page subTitle */
      this.pageTitleService.setSubTitle(" Complete solution to manage visitors, authorised vehicles and commercial vehicles in a restricted premises ");

      this.service.getVisitManagement().
         subscribe(response => {this.visitManagement = response},
                     err    => console.log(err),
                     ()     => this.visitManagement
                  );
   }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( "Vis Man" );
  }

   ngOnInit() {
      this.titleService.setTitle('Visitor Management System in Melbourne - Oodak');
   this.meta.addTag({name: 'description', content: 'Our visitor management systme provide the complete solution to manage visitors which include Visit Authorisation, Access Controls using LPR, Access Controls using RFID, Integrated Surveillance & Security, Track Overstaying Commercial Vehicles and Web Based Controls.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
   }

}
