import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  constructor(private pageTitleService: PageTitleService) {
    /* Page title */
    this.pageTitleService.setTitle("Terms & Conditions");

    /* Page subTitle */
    this.pageTitleService.setSubTitle(" ");
  }

  ngOnInit() {
  }

}
