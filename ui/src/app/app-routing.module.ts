import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent }   from './main/main.component';
import { HomeComponent } from './home/home.component';
import { PricingComponent } from './pricing/pricing.component';
import { ContactComponent } from './contact/contact.component';
import { ParkingManagementComponent } from './parkingManagement/parkingManagement.component';
import { SetupComponent } from './setup-services/setup.component';
import { IndustriesComponent } from './industries/industries.component';
import { CacComponent } from './cac/cac.component';
import { AboutComponent } from './about/about.component';
import { SearchComponent } from './search/search.component';
import { SupportComponent } from './support/support.component';
import { sidebarWidgetsComponent } from './sidebarWidgets/sidebarWidgets.component';
import {fleetmanagerComponent} from "./fleet-manager/fleet-manager.component";
import {fuelstationComponent} from "./fuel-station/fuel-station.component";
import {visitManagementComponent} from "./visitManagement/visitManagement.component";
import {LprComponent} from "./lpr/lpr.component";
import {paygoComponent} from "./pay-go/pay-go.component";
import { BlogColumn3Component } from './blog/blogColumn3/blogColumn3.component';
import { BlogDetailComponent } from './blog/blogDetail/blogDetail.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import {FuelicDoaComponent} from "./fuelic-doa/fuelic-doa.component";
import {EAIComponent} from "./eai/eai.component";
import {IntegrationComponent} from "./integration/integration.component";
import {RfidComponent} from "./rfid/rfid.component";

export const AppRoutes: Routes = [{
   path: '',
   redirectTo: 'home',
   pathMatch: 'full',
   },{
      path: '',
      component: MainComponent,
      children: [
         {
            path: 'home',
            component: HomeComponent
         },
         {
            path: 'visit',
            component: PricingComponent
         },
         {
            path: 'pricing',
            component: PricingComponent
         },{
            path: 'contact',
            component: ContactComponent
         },{
            path:'fleet-manager',
            component:fleetmanagerComponent
         },{
            path:'fuel-station',
            component:fuelstationComponent
          },{
            path:'visitor-management',
            component:visitManagementComponent
          },
          {
            path:'lpr',
            component:LprComponent
          },
        {
          path:'doa',
          component:FuelicDoaComponent
        },
        {
          path:'eai',
          component:EAIComponent
        },
       {
          path:'integration',
          component:IntegrationComponent
        },
        {
          path:'rfid',
          component:RfidComponent
        },
        {
            path:'pay-go',
            component:paygoComponent
          },{
            path:'about',
            component:AboutComponent
         },{
            path:'search',
            component:SearchComponent
         },{
            path:'support',
            component:SupportComponent
         },{
          path:'parking-management',
          component:ParkingManagementComponent
        },{
          path:'services',
          component:SetupComponent
        },{
          path:'industries',
          component:IndustriesComponent
        },{
          path:'cac',
          component:CacComponent
        },{
          path:'privacy-policy',
          component:PrivacyComponent
        },{
          path:'terms-conditions',
          component:TermsComponent
        },{
            path: '',
            loadChildren: './portfolio/portfolio.module#PortfolioModule'
         }, {
            path: '',
            loadChildren: './testimonial/testimonial.module#TestimonialModule'
         }, {
            path: 'sidebar-widgets',
            component:sidebarWidgetsComponent
         },{
            path: '',
            loadChildren: './session/session.module#SessionModule'
         },{
            path: '',
            loadChildren: './shop/shop.module#ShopModule'
         },
        {
          path: 'blog',
          component:BlogColumn3Component
        },
        {
          path: 'blog-detail/:link',
          component:BlogDetailComponent
        }
      ]
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(AppRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
