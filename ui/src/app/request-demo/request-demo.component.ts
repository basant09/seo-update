import { Component, OnInit, ViewContainerRef  } from '@angular/core';
import { ChkService } from '../service/chk.service';
import { ToastsManager } from 'ng2-toastr';
@Component({
  selector: '[app-request-demo]',
  templateUrl: './request-demo.component.html',
  styleUrls: ['./request-demo.component.css']
})

export class RequestDemoComponent implements OnInit {
  formdata:any;
  uEmail:any;
  uName: any;
  uContact: any;
  uComment: any;
  time: any;
  showSpinner:any;
  constructor(public toastr: ToastsManager, vcr: ViewContainerRef, private service:ChkService) {
    this.formdata = {};
    this.time = new Date();
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  }
  processForm(){
    this.formdata = {
      fromAddress:"\"Oodak-Sales\" <sales@oodak.com>",
      toAddress:"sales@oodak.com, manoj.kholia@theavancer.com.au, sudhir.dwivedi@theavancer.com.au",
      ccAddress:"",
      bccAddress:"",
      subject:"Demo Request from "+this.uEmail,
      encoding:"UTF-8",
      mailFormat:"plaintext",
      content:"Hi Team,\n" + "\n" + "Customer with the following details has requested product demo on "+this.time + "\n" +
      "Email id: " +this.uEmail+ "\n" +
      "Message: "+ this.uComment + "\n" +
      "Name: " + this.uName + "\n" +
      "Contact No: "+ this.uContact
    }
    this.showSpinner = true;
    if(this.uEmail && this.uName && this.uContact && this.uComment){
      this.service.submitRDForm(this.formdata).subscribe(response => {
          this.showSpinner = false;
          this.toastr.success('Thanks for showing your interest. Team Fuelic will get back to you soon!', 'Success!');
          this.uEmail="";this.uName="";this.uContact="";this.uComment="";
          setTimeout(function () {
            document.getElementById("requestDemoDropdown").classList.remove("show");
          },3000);
        },
        err => console.log(err)
      );
    } else {
      this.toastr.error('Please Fill all the required information!', 'Alert!');
      this.showSpinner = false;
    }

  }
}
