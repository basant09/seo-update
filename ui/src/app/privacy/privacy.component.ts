import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {

  constructor(private pageTitleService: PageTitleService) {
    /* Page title */
    this.pageTitleService.setTitle("Privacy Policy");

    /* Page subTitle */
    this.pageTitleService.setSubTitle(" ");
  }

  ngOnInit() {
  }

}
