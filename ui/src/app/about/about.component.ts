import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';

@Component({
  selector: 'chankya-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

   /* Variables */
   services : any;
   about    : any;
   team     : any;
   contact  : any;

   constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {

      /* Page title */
      this.pageTitleService.setTitle("About Us");

      /* Page subTitle */
      this.pageTitleService.setSubTitle(" ");

      this.service.getAbout().
         subscribe(response => {this.about = response},
                   err      => console.log(err),
                   ()       => this.about
               );
   }


   ngOnInit() {
    this.titleService.setTitle('About - CCTV Security Cameras Installers in Melbourne');
  	this.meta.addTag({name: 'description', content: 'Connect with Oodak, for commercial & residential CCTV surveillance solutions with face detection, license plate recognition, vandalism protection, security alarms system in Melbourne.'});
    this.meta.addTag({name: 'robots', content: 'index, follow'});
  }


   /*
   * Social links
   */
  socialDetails : any = [
    { url: 'https://www.facebook.com/', icon : 'fa-facebook'},
    { url: '', icon : 'fa-twitter text-info'},
    { url: '', icon : 'fa-pinterest text-danger'},
  ]

  /*
   * Classes of social ul, li
   */
  socialsClasses : any = {ulClass:"", liClass:"", linkClass:""}

}
