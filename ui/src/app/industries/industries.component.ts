import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
@Component({
  selector: 'chankya-industries',
  templateUrl: './industries.component.html',
  styleUrls: ['./industries.component.scss']
})
export class IndustriesComponent implements OnInit {

   /* Variables */
  industries : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {

      /* Page title */
      this.pageTitleService.setTitle(" Industries We Serve ");

      /* Page subTitle */
      this.pageTitleService.setSubTitle("We offer parking management, visitor management, surveillance & security solutions for range of industries");

      this.service.getIndustries().
       subscribe(response => {this.industries = response},
                  err => console.log(err),
                  () => this.industries
              );
  }

  ngOnInit() {
    this.titleService.setTitle('Industries We Serve - Oodak');
   this.meta.addTag({name: 'description', content: 'We offer parking management, visitor management, surveillance & security solutions for range of various industries such as hotels & apartments, shopping mall & parking, council & communities, school & colleges, fuel stations, national parks, businessess & business parks, hospitals and toll plaza.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

}
