import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
declare var $ : any;

@Component({
  selector: 'chankya-fleet-manager',
  templateUrl: './fleet-manager.component.html',
  styleUrls: ['./fleet-manager.component.scss']
})
export class fleetmanagerComponent implements OnInit {

  /* Variables */
  highlights : any;
  about    : any;
  loginFuelic : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService) {

    /* Page title */
    this.pageTitleService.setTitle(" Fuelic Fleet");

    /* Page subTitle */
    this.pageTitleService.setSubTitle("How Fuelic Fleet Works?");

    this.service.getFuelicHighlightsFM().
    subscribe(response => {this.highlights = response},
      err       => console.log(err),
      ()        =>this.highlights
    );

    this.service.getFuelicPageContentFM().
    subscribe(response => {this.about = response},
      err      => console.log(err),
      ()       => this.about
    );
    this.service.getFuelicLoginFM().
    subscribe(response => {this.loginFuelic = response},
      err      => console.log(err),
      ()       => this.loginFuelic
    );
  }


  ngOnInit() {
  }

  /*
  * Social links
  */
  socialDetails : any = [
    { url: 'https://www.facebook.com/', icon : 'fa-facebook'},
    { url: '', icon : 'fa-twitter text-info'},
  ]

  /*
   * Classes of social ul, li
   */
  socialsClasses : any = {ulClass:"", liClass:"", linkClass:""}

}
