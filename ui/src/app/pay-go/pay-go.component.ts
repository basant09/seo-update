import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
declare var $ : any;

@Component({
  selector: 'chankya-pay-go',
  templateUrl: './pay-go.component.html',
  styleUrls: ['./pay-go.component.scss']
})
export class paygoComponent implements OnInit {

  /* Variables */
  highlights : any;
  about    : any;
  mobileFeatured : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {

    /* Page title */
    this.pageTitleService.setTitle("Fuelic Pay@Pump");

    /* Page subTitle */
    this.pageTitleService.setSubTitle("Innovative solution for Fuel stations to integrate pump controllers with smartphone application, therefore allowing fuel stations to run unmanned without major hardware investment.");

    this.service.getFuelicHighlightsPG().
    subscribe(response => {this.highlights = response},
      err       => console.log(err),
      ()        =>this.highlights
    );

    this.service.getFuelicPageContentPG().
    subscribe(response => {this.about = response},
      err      => console.log(err),
      ()       => this.about
    );
    this.service.getFuelicAppDownload().
    subscribe(response => {this.mobileFeatured = response},
      err      => console.log(err),
      ()       => this.mobileFeatured
    );

  }


  ngOnInit() {
    this.titleService.setTitle('Smartphone Based Fuelic Pay@Pump - Oodak');
   this.meta.addTag({name: 'description', content: 'Fuelic pay@pump is a innovative solution for Fuel stations with smartphone application that allow customers to pay in few clicks. Its fully integrated with the major pump controllers across Australia & NZ, therefore allowing fuel stations to run unmanned without any major hardware investment.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

  /*
  * Social links
  */
  socialDetails : any = [
    { url: 'https://www.facebook.com/', icon : 'fa-facebook'},
    { url: '', icon : 'fa-twitter text-info'},
  ]

  /*
   * Classes of social ul, li
   */
  socialsClasses : any = {ulClass:"", liClass:"", linkClass:""}

}
