import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
@Component({
  selector: 'chankya-parkingManagement',
  templateUrl: './parkingManagement.component.html',
  styleUrls: ['./parkingManagement.component.scss']
})
export class ParkingManagementComponent implements OnInit {

  /* Variables */
  parkingManagementContent : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {

    /* Page title */
    this.pageTitleService.setTitle(" Parking Management ");

    /* Page subTitle */
    this.pageTitleService.setSubTitle(" Manage your paid, restricted access and limited duration parking - All one one platform! ");

    this.service.getParkingManagementContent().
      subscribe(response => {this.parkingManagementContent = response},
                err => console.log(err),
                () => this.parkingManagementContent
            );

  }

  ngOnInit() {

    this.titleService.setTitle('Parking Management System in Melbourne - Oodak');
   this.meta.addTag({name: 'description', content: 'Our semi or fully automated parking solutions allow your paid, restricted acces and limited duration parking. We provides Intelligent parking management system with modern features.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

}
