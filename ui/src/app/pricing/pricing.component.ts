import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';

@Component({
  selector: 'chankya-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  /* Variables */
  pricingContent : any;

  constructor( private pageTitleService: PageTitleService, private service:ChkService ) {

    /* Page title */
    this.pageTitleService.setTitle(" Home Security and CCTV Packages");

    /* Page subTitle */
    this.pageTitleService.setSubTitle(" Great warranties, 0% interest payment plan and no hidden cost, packages to suit all your needs for Security & Surveillance ");

    this.service.getPricingPageContent().
      subscribe(response => {this.pricingContent = response},
               err => console.log(err),
               () => this.pricingContent
             );

   }

  ngOnInit() {  }

}
