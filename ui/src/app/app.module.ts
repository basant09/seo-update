import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SlickModule } from 'ngx-slick';
import { DirectivesModule } from './core/directive/directives.module';

/* Routing */
import { AppRoutingModule } from './app-routing.module';

/* Service */
import { ChkService } from './service/chk.service';

/* components */
import { AppComponent } from './app.component';
import { MainComponent }   from './main/main.component';
import { HomeComponent } from './home/home.component';
import { PricingComponent } from './pricing/pricing.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { MenuItems } from './core/menu/menu-items/menu-items';
import { MenuToggleModule } from './core/menu-toggle.module';
import { PageTitleService } from './core/page-title/page-title.service';
import { WidgetsModule } from './widgets/widgets.module';
import { ParkingManagementComponent } from './parkingManagement/parkingManagement.component';
import { SetupComponent } from './setup-services/setup.component';
import { IndustriesComponent } from './industries/industries.component';
import { CacComponent } from './cac/cac.component';
import { fleetmanagerComponent } from './fleet-manager/fleet-manager.component';
import { fuelstationComponent} from "./fuel-station/fuel-station.component";
import { visitManagementComponent} from "./visitManagement/visitManagement.component";
import { paygoComponent } from './pay-go/pay-go.component';
import { AboutComponent } from './about/about.component';
import { SearchComponent } from './search/search.component';
import { SupportComponent } from './support/support.component';
import { Footer2Component } from './footer2/footer2.component';
import { homepageBannerComponent } from './homepageBanner/homepageBanner.component';
import { sidebarWidgetsComponent } from './sidebarWidgets/sidebarWidgets.component';
import { BlogColumn3Component } from './blog/blogColumn3/blogColumn3.component';
import { BlogDetailComponent } from './blog/blogDetail/blogDetail.component';
import { RequestDemoComponent } from './request-demo/request-demo.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { LprComponent } from './lpr/lpr.component';
import { FuelicDoaComponent } from './fuelic-doa/fuelic-doa.component';
import { EAIComponent } from './eai/eai.component';
import { IntegrationComponent } from './integration/integration.component';
import { RfidComponent } from './rfid/rfid.component';
import { SeoService } from './seo.service';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HomeComponent,
    PricingComponent,
    ContactComponent,
    FooterComponent,
    homepageBannerComponent,
    HeaderComponent,
    MenuComponent,
    ParkingManagementComponent,
    SetupComponent,
    IndustriesComponent,
    CacComponent,
    fleetmanagerComponent,
    fuelstationComponent,
    visitManagementComponent,
    paygoComponent,
    AboutComponent,
    SearchComponent,
    SupportComponent,
    Footer2Component,
    sidebarWidgetsComponent,
    BlogColumn3Component,
    BlogDetailComponent,
    RequestDemoComponent,
    PrivacyComponent,
    TermsComponent,
    LprComponent,
    FuelicDoaComponent,
    EAIComponent,
    IntegrationComponent,
    RfidComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    WidgetsModule,
    MenuToggleModule,
	  DirectivesModule,
    SlickModule.forRoot(),
    ToastModule.forRoot(),
  ],
  providers: [
    MenuItems,
    PageTitleService,
    ChkService,
    Title,
    SeoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
