import { Component, OnInit } from '@angular/core';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
declare var $ : any;

@Component({
  selector: 'chankya-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  /* Variables */
  homeContent    : any;
  services       : any;
  projectGallary : any;
  posts          : any;
  team           : any;
  pricingContent : any;
  contact        : any;
  videoContent   : any;
  mobileFeatured : any;
  testimonial    : any;

  constructor( private service:ChkService, private titleService: Title,
   private meta: Meta ) {

      this.service.getHomeContent().
         subscribe(response => {this.homeContent = response},
                   err      => console.log(err),
                   ()       => this.getContent(this.homeContent)
               );

      this.service.getServices().
         subscribe(response => {this.services = response},
                  err       => console.log(err),
                  ()        =>this.services
               );

      this.service.getPosts().
         subscribe(response => {this.posts = response},
                  err      => console.log(err),
                  ()       => this.posts
               );

      this.service.getProjectGallary().
         subscribe(response => {this.projectGallary = response},
                   err      => console.log(err),
                   ()       => this.projectGallary
               );


      this.service.getTeam().
         subscribe(response => {this.team = response},
                   err      => console.log(err),
                   ()       => this.team
               );

      this.service.getPricingPageContent().
         subscribe(response => {this.pricingContent = response},
                  err       => console.log(err),
                  ()        => this.pricingContent
               );

      this.service.getContactContent().
         subscribe(response => {this.contact = response},
                   err      => console.log(err),
                   ()       => this.contact
               );

      this.service.getHomeTestimonial().
         subscribe(response => {this.testimonial = response},
                   err      => console.log(err),
                   ()       => this.testimonial
               );
      }

  ngOnInit() {
   this.titleService.setTitle('CCTV Supply, Security and Installation Services in Melbourne | Oodak');
   this.meta.addTag({name: 'description', content: 'We provide powerful solutions for visitor & parking management, security, and access control in Melbourne. Our commercial and residential CCTV Surveillance Solutions provide Face Detection, License Plate Recognition, Vandalism Protection, Security Alarms services.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  }


  /*
   * getContent is used for get the home page content.
   * Used variables is videoContent and mobileFeatured.
   */
   getContent(content)
   {
      this.videoContent    = content.video_content;
      this.mobileFeatured  = content.mobile_featured;
   }

   subscribeFormClasses : any = {rowClass:"row", fieldClass:"col-sm-12 col-md-6"}



}
