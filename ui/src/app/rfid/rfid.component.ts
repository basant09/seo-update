import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
@Component({
  selector: 'chankya-rfid',
  templateUrl: './rfid.component.html',
  styleUrls: ['./rfid.component.scss']
})
export class RfidComponent implements OnInit {

  /* Variables */
  rfidContent : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {

    /* Page title */
    this.pageTitleService.setTitle(" RFID Access Controls ");

    /* Page subTitle */
    this.pageTitleService.setSubTitle(" Manage the access of authorised personnels and vehicles ");

    this.service.getRfidContent().
      subscribe(response => {this.rfidContent = response},
                err => console.log(err),
                () => this.rfidContent
            );

  }

  ngOnInit() {
    this.titleService.setTitle('RFID Solutions in Melbourne | RFID Access Controls - Oodak');
   this.meta.addTag({name: 'description', content: 'We offer the RFID solution for vehicles and authorised personnel in Melbourne. They are easy to manage, highly accurate, access controls and access restriction.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

}
