/*
 * Send message
 * Used in another component.
 */
import { Component, OnInit, ViewContainerRef, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChkService } from '../../../service/chk.service';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: '[chankya-sendMessage]',
  templateUrl: './sendMessage.component.html',
  styleUrls: ['./sendMessage.component.scss']
})
export class SendMessageComponent implements OnInit {
   formdata:any;
   time:any;
   showSpinner:any;
   sendMessageForm : FormGroup;

   constructor( private formBuilder : FormBuilder, private service:ChkService, public toastr: ToastsManager, vcr: ViewContainerRef) {
     this.time = new Date();

      this.sendMessageForm = this.formBuilder.group({
         first_name : [null, [Validators.required] ],
         last_name  : [null, [Validators.required] ],
         email      : [null, [Validators.required] ],
         subject    : [null, [Validators.required] ],
         textarea   : [null, [Validators.required] ]
      });
     this.toastr.setRootViewContainerRef(vcr);
   }

   ngOnInit() {
   }

   /*
    * sendMessage
    */
   sendMessage(values:any)
   {
     if(this.sendMessageForm.valid)
     {
       this.formdata = {
         recipient:"sales@oodak.com, manoj.kholia@theavancer.com.au, sudhir.dwivedi@theavancer.com.au",
         subject:values.subject,
         body:"Hi Team,\n" + "\n" + "This message is sent from Contact form with Following detail on "+this.time + "\n" +
         "Email id: " +values.email+ "\n" +
         "Message: "+ values.textarea + "\n" +
         "Name: " + values.first_name +' ' +values.last_name
       }
       this.showSpinner = true;
       this.service.submitRDForm(this.formdata).subscribe(response => {
           this.showSpinner = false;
           this.sendMessageForm.reset();
           this.toastr.success('Thanks for showing your interest. Team Fuelic will get back to you soon!', 'Success!');
         },
         err => console.log(err)
       );
     } else{
       this.toastr.error('Please Fill all the required information!', 'Alert!');
       this.showSpinner = false;
     }
   }

}
