/*
 * Login Fuelic
 * Used in another components.
 */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[chankya-loginFuelic]',
  templateUrl: './loginFuelic.component.html',
  styleUrls: ['./loginFuelic.component.scss']
})
export class LoginFuelicComponent implements OnInit {

   @Input() loginFuelic : any;

   constructor() { }

   ngOnInit() {
   }

}
