import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../core/page-title/page-title.service';
import { ChkService } from '../service/chk.service';
import {Title, Meta} from '@angular/platform-browser';
@Component({
  selector: 'chankya-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent implements OnInit {

  /* Variables */
  setupContent : any;

  constructor(private pageTitleService: PageTitleService, private service:ChkService, private titleService: Title,
    private meta: Meta) {

    /* Page title */
    this.pageTitleService.setTitle(" Business Setup Services ");

    /* Page subTitle */
    this.pageTitleService.setSubTitle(" One stop shop to setup up a new Business or upgrade an existing business with necessary equipments");

    this.service.getBusinessSetupContent().
      subscribe(response => {this.setupContent = response},
                err => console.log(err),
                () => this.setupContent
            );

  }

  ngOnInit() {

    this.titleService.setTitle('Alarms, Intercom Systems Supply and Installation in Melbourne - Oodak');
   this.meta.addTag({name: 'description', content: 'One stop shop to setup up a new Business or upgrade an existing business with necessary equipments such as CCTV Installation, Installation of Security Alarm, Smoke Alarm and Intercom systems in Melbourne.'});
  this.meta.addTag({name: 'robots', content: 'index, follow'});
  }

}
