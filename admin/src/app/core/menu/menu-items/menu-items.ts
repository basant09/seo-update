import { Injectable } from '@angular/core';

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: 'session',
    name: 'Manage Business',
    type: 'sub',
    icon: 'icon-login icons',
    children: [
      { state: 'create-fleet', name: 'Business Detail' },
      { state: 'add-location', name: 'Add Location' },
      { state: 'list-location', name: 'View Location' }
    ]
  },
  {
    state: 'session',
    name: 'Manage User',
    type: 'sub',
    icon: 'icon-login icons',
    children: [
      { state: 'add-user', name: 'Add User' },
      { state: 'list-user', name: 'View User' }
    ]
  },
  {
    state: 'session',
    name: 'Manage Vehicle',
    type: 'sub',
    icon: 'icon-login icons',
    children: [
      { state: 'add-vehicle', name: 'Add Vehicle' },
      { state: 'list-vehicle', name: 'View Vehicle' }
    ]
  },
  {
    state: 'session',
    name: 'Manage Roster',
    type: 'sub',
    icon: 'icon-login icons',
    children: [
      { state: 'create-roster', name: 'Create Roster' },
      { state: 'list-roster', name: 'View Roster' }
    ]
  }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
  add(menu: Menu) {
    // MENUITEMS.push(menu);
  }
}
