export const Constants = {
    authTokenKey: 'token'
};

export const DEFAULT_USER_PIC = 'http://res.cloudinary.com/dshjsk7yg/image/upload/q_auto,f_auto/v1511266654/user/profilePicture/default-user-image.png';
export const LOGO = 'http://res.cloudinary.com/dshjsk7yg/image/upload/q_auto,f_auto/v1511609351/website/your-logo-here.png';