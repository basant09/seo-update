import { Injectable, ViewContainerRef } from '@angular/core';
declare var uploadImageToCloudinary: any;
declare var $: any;
import { CLOUDINARY_API_KEY, CLOUDINARY_BASE_URL, CLOUDIANRY_CLOUD_NAME } from '../../environments/environment';
import { AuthService } from 'app/services/auth.service';
import { ToastrService, ToastrConfig } from 'ngx-toastr';

@Injectable()
export class Utils {

    public inputFieldCheck: InputFieldCheck;
    public image: Image;
    public auth: Auth;
    public toastr: Toastr;
    public sort: Sort;
    public dateUtil: DateUtil;

    constructor(public authService: AuthService, public toastrService: ToastrService) {
        this.inputFieldCheck = new InputFieldCheck();
        this.image = new Image();
        this.auth = new Auth(authService);
        this.toastr = new Toastr(toastrService);
        this.sort = new Sort();
        this.dateUtil = new DateUtil();
    }
}

class InputFieldCheck {

    public isPureNumeric(evt) {
        evt = (evt) ? evt : window.event;
        const charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
}

class Image {

    public uploadProfilePicture(source?: string) {
        if (source === undefined) {
            source = 'userPicAdd';
        }
        const picUrl = uploadImageToCloudinary.init(source, CLOUDIANRY_CLOUD_NAME, CLOUDINARY_API_KEY);
    }

}

class Auth {

    constructor(private authService: AuthService) {

    }

    public isTokenExpired(errorStr) {
        try {
            if (errorStr.json().error.code === 'ERR_TOKEN_EXPIRED') {
                setTimeout(() => {
                    this.authService.logout();
                }, 2000);
            }
        } catch (err) {

        }
    }
}

class Toastr {

    private vcr: ViewContainerRef;

    // public setViewContainerRef(vcr: ViewContainerRef) {
    //     this.vcr = vcr;
    //     this.toastr.setRootViewContainerRef(this.vcr);
    // }

    constructor(public toastrService: ToastrService) {
        // this.toastr.setRootViewContainerRef(this.vcr);
    }

    showSuccess(message: string) {
        this.toastrService.success(message, 'Success!');
    }

    showError(message: string) {
        this.toastrService.error(message, 'Oops!');
    }

    showWarning(message: string) {
        this.toastrService.warning(message, 'Alert!');
    }

    showInfo(message: string) {
        this.toastrService.info(message, 'Info');
    }

}

class Sort {

    sortAsc(element1, element2): number {
        try {
            if (element1.toLowerCase() > element2.toLowerCase()) {
                return 1;
            }
            if (element1.toLowerCase() < element2.toLowerCase()) {
                return -1;
            }
            if (element1.toLowerCase() === element2.toLowerCase()) {
                return 0;
            }
        } catch (error) {
            return 0;
        }
    }

    sortDesc(element1, element2): number {
        try {
            if (element1.toLowerCase() > element2.toLowerCase()) {
                return -1;
            }
            if (element1.toLowerCase() < element2.toLowerCase()) {
                return 1;
            }
            if (element1.toLowerCase() === element2.toLowerCase()) {
                return 0;
            }
        } catch (error) {
            return 0;
        }
    }

}

class DateUtil {

    getHtmlDateFormat(inputDate: Date): string {
        let date = inputDate.getDate().toString();
        let month = (inputDate.getMonth() + 1).toString();
        const year = inputDate.getFullYear().toString();
        if (+month < 10 && month.length === 1) {
            month = '0' + month;
        }
        if (+date < 10 && date.length === 1) {
            date = '0' + date;
        }
        return year + '-' + month + '-' + date;
    }

    getHtmlTimeFormat(timeStr: string): string {
        try {
            if (timeStr) {
                let formatTime = timeStr.split(':');
                return formatTime[0] + ':' + formatTime[1];
            } else {
                return timeStr;
            }
        } catch (err) {
            return timeStr;
        }
    }

    dateMMDDYYYY(inputDate: string): string {
        try {
            if (inputDate) {
                let formatDate = inputDate.split('-');
                return formatDate[1] + '-' + formatDate[2] + '-' + formatDate[0];
            } else {
                return inputDate;
            }
        } catch (err) {
            return inputDate;
        }

    }

}
