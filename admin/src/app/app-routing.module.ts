import { AuthGuard } from './auth/auth.guard';
import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';

import { MainComponent } from './main/main.component';
import { AuthComponent } from './auth/auth.component';

export const AppRoutes: Routes = [{
  path: '',
  redirectTo: 'index',
  pathMatch: 'full'
}];
