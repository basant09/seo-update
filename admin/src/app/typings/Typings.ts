import { Injectable } from '@angular/core';

export interface FireBaseRequestCommand {
    email: string;
    password: string;
    returnSecureToken: boolean;
}

export interface FireBaseResponseCommand {
    status: boolean;
    message: string;
    code: string;
}

export interface LoginResponseCommand {
    businessId: number;
    firstName: string;
    hasPaymentConfigured: boolean;
    lastName: string;
    status: string;
}

export interface UserRegisterCommand {
    userId: string;
    password: string;
    email: string;
    mobileNumber: number;
    firstName: string;
    lastName: string;
    businessName: string;
}

export interface RegisterSuccessCommand {
    businessId: number;
    status: string;
}

export interface ResponseErrorCommand {
    error: ErrorCommand;
}

export interface ErrorCommand {
    code: string
    message: string;
}

export class AddUserCommand {
    firstName: string;
    lastName: string;
    email: string;
    userId: string;
    mobile: number;
    phone: number;
    emergencyContact: number;
    licenseNumber: number;
    licenseIssueDate: string;
    licenseExpiryDate: string;
    employeeId: string;
    employeeType: string;
    fuelGradesRestriction: string[];
    dailyExpenseLimit: number;
    weeklyExpenseLimit: number;
    monthlyExpenseLimit: number;
    image: string;
    gender: string;
}

export interface AddUserSuccessResponseComamnd {
    status: string;
}

export interface UserListDetailsCommand {
    email: string;
    empId: string;
    firstName: string;
    id: string;
    lastName: string;
    licenseNumber: string;
    mobile: string;
    status: string;
    userId: string;
    phone: string;
}

export interface CreateFleetProfileCommand {
    fleetName: string;
    entityType: string;
    ACN: string;
    ABN: string;
}

export interface UpdateFleetProfileCommand extends CreateFleetProfileCommand {
    fleetId: number;
}

export class AddFleetLocationCommand {
    locationName: string;
    isHeadOffice: boolean;
    latitude: string;
    longitude: string;
    addressLine: string;
    city: string;
    suburb: string;
    state: string;
    postCode: string;
    country: string;
    contactPerson: string;
    contactPersonEmail: string;
    contactPersonPhone: string;
    contactPersonMobile: string;
    contactPersonAltContact: string;
}

export interface UpdateFleetLocationCommand extends AddFleetLocationCommand {
    locationId: number;
}

export interface RemoveFleetLocationCommand {
    locationId: number;
}

export interface GetFleetLocationCommand {
    locationId: number;
}

export interface GetFleetUserDetailCommand {
    empInfoId: number;
}

export interface FleetUserDetailCommand extends AddUserCommand {
    fleetUserStatus: string;
    isEmployeeActive: boolean;
    fuelGrades: string[];
}

export interface DeactivateUserCommand {
    userId: string;
    status: string;
}

export interface GetDefaultFuelRestrictionCommand {
    fuelGrades: string[];
    fuelGradesRestriction: string[];
    status: string;
}

export class SetDefaultFuelRestrictionCommand {
    fuelGradesRestriction: string[];
}

export interface DefaultExpenseLimitCommand {
    dailyExpenseLimit: number;
    monthlyExpenseLimit: number;
    status: string;
    weeklyExpenseLimit: number;
}

export interface SetDefaultExpenseLimitCommand {
    dailyExpenseLimit: number;
    monthlyExpenseLimit: number;
    weeklyExpenseLimit: number;
}

export interface AddVehicleCommand {
    type: string;
    brand: string;
    model: string;
    year: string;
    colour: string;
    initialOdometerRead: number;
    fuelType: string;
    registrationNumber: string;
    distanceUnit: string;
    fuelUnit: string;
    vehicleStatus: string;
}

export interface VehicleTypeCommand {
    status: string;
    vehicleTypes: string[];
}

export interface VehicleBrandsCommand {
    vehicleBrands: string[];
    status: string;
}

export interface FuelTypesCommand {
    fuelTypes: string[];
    status: string;
}

export interface DefaultFuleUnitCommand {
    defaultFuelUnit: string;
    fuelUnits: string[];
    status: string;
}

export interface DefaultDistanceUnitCommand {
    defaultDistanceUnit: string;
    distanceUnits: string[];
    status: string;
}

export interface VehicleDetailsCommand {
    brand: string;
    id: number;
    model: string;
    registrationNumber: string;
    vehicleStatus: string;
}

export interface UpdateVehicleCommand extends AddVehicleCommand {
    id: number;
}

export interface DeactiveVehicleCommand {
    id: number;
    vehicleStatus: string;
}

export interface AddVehicleResponseCommand {
    id: number;
    status: string;
}

export interface EmployeeTypeCommand {
    emp_type: string[];
    status: string;
}

export interface VehicleModalCommand {
    status: string;
    vehicleModels: string[];
}

export interface VehicleModalRequestCommand {
    brand: string;
}

export interface GetVehicleDetailsCommand {
    id: number;
}

export interface SupportedCountriesCommand {
    country: string;
    countryCode: string;
    status: string;
}

export interface StatesCommand {
    states: string[];
    status: string;
}

export interface StatesRequestCommand {
    countryCode: string;
}

export class CreateRosterCommand {
    occurrence: string;
    selectedDays: string[];
    startDate: string;
    endDate: string;
    startTime: string;
    endTime: string;
    vehicleId: number;
    userId: string;
    initialOdometerRead: string;
    rosterStatus: string;
    summary: string;
}

export class UpdateRosterCommand {
    id: number;
    occurrence: string;
    selectedDays: string[];
    startDate: string;
    endDate: string;
    startTime: string;
    endTime: string;
    vehicleId: number;
    userId: string;
    initialOdometerRead: string;
    summary: string;
}

export class RosterAvailableUser {
    email: string;
    empId: string;
    firstName: string;
    gender: string;
    id: number;
    image: string;
    lastName: string;
    licenseNumber: string;
    mobile: number;
    phone: string;
    userId: string;
}

export class RosterAvailableVehicle {
    brand: string;
    id: number;
    model: string;
    registrationNumber: string;
}

export interface RosterRequestCommand {
    occurrence: string;
    selectedDays: string[];
    startDate: string;
    endDate: string;
    startTime: string;
    endTime: string;
}

export interface RosterListCommand {
    endDate: string;
    rosterDays: string[];
    rosterId: string;
    rosterStatus: string;
    startDate: string;
    summary: string;
    userId: string;
    userName: string;
    vehicleId: number;
    vehicleRegistration: string;
}

export interface RosterDetilsRequestCommand {
    id: number;
}

export class GetFleetLocationsCommand extends AddFleetLocationCommand {
    locationId: number;
}

export interface FleetProfileDetailCommand extends CreateFleetProfileCommand {
    fleetId: number;
}

export class RosterListRequestCommand {
    occurrence: string;
    selectedDays: string[];
    startDate: string;
    endDate: string;
    startTime: string;
    endTime: string;
}

export class UpdateRosterStatusCommand {
    id: number;
    rosterStatus: string;
}

export class RosterStatusCommand {
    rosterStatus: string[];
    status: string;
}

export class RosterOccurrencesCommand {
    rosterOccurrences: string[];
    status: string;
}
