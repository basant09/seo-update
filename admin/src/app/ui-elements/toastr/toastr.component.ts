import { Component, OnInit, VERSION, ViewEncapsulation } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { fadeInAnimation } from '../../core/route-animation/route.animation';
import { ToastrService, ToastrConfig } from 'ngx-toastr';

@Component({
  selector: 'pap-toastr',
  templateUrl: './toastr.component.html',
  styleUrls: ['./toastr.component.scss']
})
export class ToastrComponent implements OnInit {

  options: ToastrConfig = {
    closeButton: false,
    enableHtml: false,
    extendedTimeOut: 1000,
    messageClass: 'toast-message',
    positionClass: 'toast-top-full-width',
    progressBar: false,
    tapToDismiss: true,
    timeOut: 5000,
    titleClass: 'toast-title',
    toastClass: 'toast',
    toastComponent: ToastrComponent,
    onActivateTick: false
  };

  constructor(private pageTitleService: PageTitleService, public toastrService: ToastrService) {
    // this.options = this.toastrService.toastrConfig;
  }

  ngOnInit() {
  }

  showError(message: string, title?: string) {
    const opt = JSON.parse(JSON.stringify(this.options));
    this.toastrService.error(message, title, opt);
  }

  showSuccess(message: string, title?: string) {
    const opt = JSON.parse(JSON.stringify(this.options));
    this.toastrService.success(message, title, opt);
  }

  showWarning(message: string, title?: string) {
    const opt = JSON.parse(JSON.stringify(this.options));
    this.toastrService.warning(message, title, opt);
  }

  showInfo(message: string, title?: string) {
    const opt = JSON.parse(JSON.stringify(this.options));
    this.toastrService.info(message, title, opt);
  }

  clearToasts() {
    this.toastrService.clear();
  }

}
