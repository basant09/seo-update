import { ToastrComponent } from './toastr/toastr.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { ToastrModule } from 'ngx-toastr';
import { ConfirmationModalComponent } from './confirmation-modal/confirmation-modal.component';


@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, ToastrModule.forRoot()],
  declarations: [ProgressBarComponent, ToastrComponent, ConfirmationModalComponent
  ],
  exports: [ProgressBarComponent, ToastrComponent],
  providers: [ToastrComponent, ConfirmationModalComponent],
  entryComponents: [
    ConfirmationModalComponent
  ]
})

export class UiElementsModule { }
