import { Component, OnInit, ViewEncapsulation, Input, OnChanges } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import { fadeInAnimation } from '../../core/route-animation/route.animation';

@Component({
    selector: 'pap-ms-progress-bar',
    templateUrl: './progress-bar-component.html',
    styleUrls: ['./progress-bar-component.scss'],
    encapsulation: ViewEncapsulation.None,
    host: {
        "[@fadeInAnimation]": 'true'
    },
    animations: [fadeInAnimation]
})
export class ProgressBarComponent implements OnInit, OnChanges {

    isProgressBarVisible: boolean;
    @Input('showProgressBar') showProgressBar: boolean;
    constructor(private pageTitleService: PageTitleService) { }

    ngOnChanges() {
        this.isProgressBarVisible = this.showProgressBar;
    }

    ngOnInit() {
    }
}
