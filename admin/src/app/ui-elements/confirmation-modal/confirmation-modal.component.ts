import { Component, OnInit ,  AfterViewInit , Input} from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'firebase/app';
@Component({
  selector: 'pap-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss']
})
export class ConfirmationModalComponent implements OnInit, AfterViewInit  {
  @Input('heading') heading: string;
  @Input('heading1') heading1: string;
  constructor(public activeModal: NgbActiveModal) {

   }

  ngOnInit() {
  }
  ngAfterViewInit() {

  }

  closeModal() {
    this.activeModal.close(1);

  }
  closeModal1() {

    this.activeModal.close(0);

  }
}
