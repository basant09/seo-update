import { Observable } from 'rxjs/Rx';
import { Error } from 'tslint/lib/error';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Constants } from '../utils/constants.util';
import { RoutingConstantService } from '../services/routingConstants.service';

@Injectable()
export class AuthGuard implements CanActivate {

    authTokenKey = Constants.authTokenKey;

    constructor(private _router: Router, private routingConstantService: RoutingConstantService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const authToken = localStorage.getItem(this.authTokenKey);
        if (authToken !== '' && authToken !== undefined && authToken !== null) {
            return true;
        } else {
            this._router.navigateByUrl(this.routingConstantService.getLoginRouterLink());
            return false;
        }
    }
}
