import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { RoutingConstantService } from '../../services/routingConstants.service';
import { FireBaseResponseCommand, FireBaseRequestCommand, LoginResponseCommand } from '../../typings/Typings';
import { NgForm } from '@angular/forms';
import { FormControl, FormGroup } from '@angular/forms';
import { MessageService } from 'app/services/message.service';

@Component({
  selector: 'pap-ms-loginone-session',
  templateUrl: './loginone-component.html',
  styleUrls: ['./loginone-component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginoneComponent {

  responseMessage: string;
  responseSuccess: boolean;
  responseFail: boolean;
  loginCommand: FireBaseRequestCommand = {
    email: null,
    password: null,
    returnSecureToken: true
  };
  showProgressBar: boolean = false;
  errorMsg = "The Email and Password you've entered is incorrect, please retry";
  rememberMe: boolean = false;
  showPassword: boolean = false;
  count: number;

  constructor(
    private router: Router, private authService: AuthService, private routingConstantService: RoutingConstantService,
    private messageService: MessageService) {
    this.count = 0;
  }

  loginone() {
    this.router.navigate(['/']);
  }

  login(loginForm) {
    if (loginForm.valid) {
      this.showProgressBar = true;
      this.authService.login(this.loginCommand).then((res: FireBaseResponseCommand) => {
        if (res.status) {
          if (this.rememberMe) {
            this.authService.firebasePersistance(this.loginCommand.email, this.loginCommand.password);
          }
          this.authService.getFireBaseAuthToken();
          setTimeout(() => {
            this.userLogin();
          }, 1000);
        } else {
          this.showProgressBar = false;
          this.responseFail = true;
          this.responseMessage = this.errorMsg;
        }
      }).catch(err => {
        this.showProgressBar = false;
        this.responseFail = true;
        this.responseMessage = this.errorMsg;
      });
    } else {
      this.responseFail = true;
      this.responseMessage = this.errorMsg;
    }

  }

  userLogin() {
    this.count += 1;
    this.authService.userLogin().subscribe((res: LoginResponseCommand) => {
      this.showProgressBar = false;
      if (res.status === 'success') {
        this.responseSuccess = false;
        this.responseFail = false;
        const msg: any[] = new Array();
        msg.push('userName');
        msg.push(res.firstName + ' ' + res.lastName);
        this.messageService.sendMessageArray(msg);
        this.router.navigateByUrl(this.routingConstantService.getCreateNewFleetBusinessRouterLink());
      } else {
        this.responseFail = true;
        this.responseMessage = this.errorMsg;
      }
    }, (error) => {
      this.showProgressBar = false;
      this.responseFail = true;
      this.responseMessage = this.errorMsg;
    });
    // } else {
    //   if (this.count === 1) {
    //     this.userLogin();
    //   }
    // }
  }

  removeError() {
    this.responseFail = false;
    this.responseSuccess = false;
    this.responseMessage = '';
  }

}



