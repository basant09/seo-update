import { RegisterComponent } from './register/register.component';
import { LoginoneComponent } from './loginone/loginone.component';
import { AuthRouting } from './auth.routing';
import { AuthGuard } from './auth.guard';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AuthComponent } from './auth.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { UiElementsModule } from '../ui-elements/ui-elements.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AuthRouting,
        UiElementsModule
    ],
    declarations: [
        AuthComponent,
        LoginoneComponent,
        RegisterComponent,
        ForgotPasswordComponent
    ],
    providers: [
        AuthGuard
    ]
})
export class AuthModule { }
