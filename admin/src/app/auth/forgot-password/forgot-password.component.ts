import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'pap-ms-forgot-password',
  templateUrl: './forgot-password-component.html',
  styleUrls: ['./forgot-password-component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ForgotPasswordComponent {

  userEmail: string;
  responseSuccess: boolean = false;
  responseFail: boolean = false;
  responseMessage: string;
  showError: boolean = false;

  constructor(
    private router: Router,private authService: AuthService
  ) {
    this.responseMessage = null;
    this.userEmail = null;
  }

  send(forgotPasswordForm) {
    if (forgotPasswordForm.valid) {
      this.authService.forgotPassword(this.userEmail);
      this.responseMessage = 'An email has been sent to you with the instructions to reset password. Please change your password and login again.';
      this.responseSuccess = true;
    } else {
      this.showError = true;
    }

  }

  removeError() {
    this.showError = false;
    this.responseFail = false;
    this.responseSuccess = false;
  }

}



