import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import {
  UserRegisterCommand, FireBaseRequestCommand, RegisterSuccessCommand, ResponseErrorCommand,
  FireBaseResponseCommand
} from '../../typings/Typings';
import { AuthService } from '../../services/auth.service';
import { RoutingConstantService } from '../../services/routingConstants.service';
import { Utils } from 'app/utils/utils.util';
import { MessageService } from 'app/services/message.service';

@Component({
  selector: 'pap-ms-register-session',
  templateUrl: './register-component.html',
  styleUrls: ['./register-component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RegisterComponent {

  userRegisterCommand: UserRegisterCommand;
  fireBaseRequestCommand: FireBaseRequestCommand;
  registerResponse: RegisterSuccessCommand | ResponseErrorCommand;
  showProgressBar: boolean = false;
  responseFail: boolean = false;
  responseSuccess: boolean = false;
  responseMessage: string = '';
  showError: boolean = false;
  rememberMe: boolean;

  constructor(
    private router: Router, private authService: AuthService, private routingConstantService: RoutingConstantService,
    public utils: Utils, private messageService: MessageService
  ) {
    this.userRegisterCommand = {
      businessName: null,
      email: null,
      firstName: null,
      lastName: null,
      mobileNumber: null,
      password: null,
      userId: null
    };
    this.fireBaseRequestCommand = {
      email: null,
      password: null,
      returnSecureToken: true
    };
    this.rememberMe = false;
  }

  register(registerForm) {
    if (registerForm.valid) {
      this.showProgressBar = true;
      this.userRegisterCommand.userId = this.userRegisterCommand.email;
      this.fireBaseRequestCommand.email = this.userRegisterCommand.email;
      this.fireBaseRequestCommand.password = this.userRegisterCommand.password;

      this.authService.signup(this.fireBaseRequestCommand).then((res: FireBaseResponseCommand) => {
        if (res.status) {
          if (this.rememberMe) {
            this.authService.firebasePersistance(this.fireBaseRequestCommand.email, this.fireBaseRequestCommand.password);
          }
          this.authService.getFireBaseAuthToken();
          setTimeout(() => {
            this.registerBusinessAdmin();
          }, 1000);
        } else {
          this.showProgressBar = false;
          this.responseFail = true;
          this.responseMessage = res.message;
        }
      }).catch((err: FireBaseResponseCommand) => {
        this.showProgressBar = false;
        this.responseFail = true;
        this.responseMessage = err.message;
      });
    } else {
      this.showError = true;
    }
  }

  registerBusinessAdmin() {
    this.authService.registerBusinessAdmin(this.userRegisterCommand).subscribe((res: any) => {
      this.showProgressBar = false;
      if (res.status === 'success') {
        const msg: any[] = new Array();
        msg.push('userName');
        msg.push(res.firstName + ' ' + res.lastName);
        this.messageService.sendMessageArray(msg);
        this.router.navigateByUrl(this.routingConstantService.getDashboardRouterLink());
      } else {
        this.responseFail = true;
        this.responseMessage = res.message
      }
    }, (error) => {
      this.showProgressBar = false;
      this.responseFail = true;
      this.responseMessage = error.message
    });
  }

  removeError() {
    this.responseFail = false;
    this.responseSuccess = false;
    this.responseMessage = '';
  }

}



