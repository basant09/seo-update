import { Utils } from '../../utils/utils.util';
import { MessageService } from '../../services/message.service';
import { RoutingConstantService } from '../../services/routingConstants.service';
import { AuthService } from '../../services/auth.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { fadeInAnimation } from 'app/core/route-animation/route.animation';
import {
  AddUserCommand, ResponseErrorCommand, GetDefaultFuelRestrictionCommand,
  DefaultExpenseLimitCommand,
  EmployeeTypeCommand,
  AddUserSuccessResponseComamnd
} from 'app/typings/Typings';
import { INgxMyDpOptions, IMyDateModel, IMyDate } from 'ngx-mydatepicker';
import { Router } from '@angular/router';
import { DEFAULT_USER_PIC } from '../../utils/constants.util';
import { ConfirmationModalComponent } from '../../ui-elements/confirmation-modal/confirmation-modal.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'pap-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '[@fadeInAnimation]': 'true'
  },
  animations: [fadeInAnimation]
})
export class AddUserComponent implements OnInit {
  addUserCommand: AddUserCommand;
  licenseIssueDate: any;
  licenseExpiryDate: any;
  responseSuccess: boolean = false;
  showError: boolean = false;
  responseFail: boolean = false;
  responseMessage: string;
  todayDate: Date = new Date();
  disableSince: IMyDate;
  issueDateOptions: INgxMyDpOptions;
  expiryDateOptions: INgxMyDpOptions;
  fuelGradesArray: string[] = new Array();
  fuelGradeRestrictionArray: string[] = new Array();
  employeeTypeArray: string[] = new Array();
  defaultFuelRestriction: GetDefaultFuelRestrictionCommand;
  userPic: string;
  genderArray = ['Female', 'Male', 'Other'];
  todayDateValue: string;
  heading: 'xyz';

  constructor(private fb: FormBuilder, private pageTitleService: PageTitleService,
    private authService: AuthService, private router: Router, private routingConstantService: RoutingConstantService,
    private messageService: MessageService, public utils: Utils ,  private modalService: NgbModal) {
    this.addUserCommand = {
      dailyExpenseLimit: null,
      email: null,
      emergencyContact: null,
      employeeId: null,
      employeeType: null,
      fuelGradesRestriction: null,
      firstName: null,
      lastName: null,
      licenseExpiryDate: null,
      licenseIssueDate: null,
      licenseNumber: null,
      mobile: null,
      monthlyExpenseLimit: null,
      phone: null,
      userId: null,
      weeklyExpenseLimit: null,
      gender: null,
      image: null
    };
    this.licenseIssueDate = null;
    this.licenseExpiryDate = null;
    this.fuelGradesArray = [];
    this.fuelGradeRestrictionArray = [];
    this.employeeTypeArray = [];
    this.todayDateValue = null;
  }

  ngOnInit() {
    this.pageTitleService.setTitle('Add User');
    this.userPic = DEFAULT_USER_PIC;
    this.getDefaultFuelRestriction();
    this.getDefaultExpenseLimit();
    this.getEmployeeType();
    const todayDate: Date = new Date();
    this.disableSince = {
      year: todayDate.getFullYear(),
      month: todayDate.getMonth() + 1,
      day: todayDate.getDate()
    };
    this.todayDateValue = this.utils.dateUtil.getHtmlDateFormat(todayDate);
    this.issueDateOptions = {
      dateFormat: 'dd-mm-yyyy',
      markCurrentDay: true,
      disableSince: this.disableSince
    };
    this.expiryDateOptions = {
      dateFormat: 'dd-mm-yyyy',
      markCurrentDay: true,
      disableUntil: this.disableSince
    };
  }

  getDefaultFuelRestriction() {
    this.messageService.showProgressBar(true);
    this.authService.getDefaultFuelRestriction().subscribe((res: GetDefaultFuelRestrictionCommand) => {
      this.messageService.showProgressBar(false);
      if (res.status === 'success') {
        this.fuelGradesArray = res.fuelGrades;
        this.fuelGradesArray.sort(this.utils.sort.sortAsc);
        this.addUserCommand.fuelGradesRestriction = res.fuelGradesRestriction;
      } else {
        this.utils.toastr.showError('No fuel restrictions found');
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getDefaultExpenseLimit() {
    this.messageService.showProgressBar(true);
    this.authService.getDefaultExpenseLimit().subscribe((resp: DefaultExpenseLimitCommand) => {
      this.messageService.showProgressBar(false);
      if (resp.status === 'success') {
        this.addUserCommand.weeklyExpenseLimit = resp.weeklyExpenseLimit;
        this.addUserCommand.dailyExpenseLimit = resp.dailyExpenseLimit;
        this.addUserCommand.monthlyExpenseLimit = resp.monthlyExpenseLimit;
      } else {
        this.utils.toastr.showError('No expense limit found');
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getEmployeeType() {
    this.messageService.showProgressBar(true);
    this.authService.getEmployeeType().subscribe((resp: EmployeeTypeCommand) => {
      this.messageService.showProgressBar(false);
      if (resp.status === 'success') {
        this.employeeTypeArray = resp.emp_type;
        this.employeeTypeArray.sort(this.utils.sort.sortAsc);
      } else {
        this.utils.toastr.showError('No employee type found');
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  addUser(addUserForm) {
    this.addUserCommand.image = document.getElementById('userPictureAdd').getAttribute('src');
    if (this.addUserCommand.image === this.userPic) {
      this.addUserCommand.image = null;
    }
    if (addUserForm.valid) {
      const modalRef = this.modalService.open(ConfirmationModalComponent);
      modalRef.componentInstance.heading = 'User Confirmation';
      modalRef.componentInstance.heading1 = 'Are you sure want to add details ?';
      modalRef.result.then(res1 => {
        if (res1 !== undefined) {
           if (res1 === 1) {
      this.addUserCommand.userId = this.addUserCommand.email;
      let command: AddUserCommand = new AddUserCommand();
      for (let key in this.addUserCommand) {
        command[key] = this.addUserCommand[key];
      }
      command.licenseExpiryDate = this.utils.dateUtil.dateMMDDYYYY(command.licenseExpiryDate);
      command.licenseIssueDate = this.utils.dateUtil.dateMMDDYYYY(command.licenseIssueDate);
      this.messageService.showProgressBar(true);
      this.authService.addUser(command).subscribe((res: AddUserSuccessResponseComamnd ) => {
        this.messageService.showProgressBar(false);
        if (res.status === 'success') {
          this.utils.toastr.showSuccess('User added successfully');
          setTimeout(() => {
            this.router.navigateByUrl(this.routingConstantService.getListUserRouterLink());
          }, 2000);
        } else {
          this.utils.toastr.showError('Something went wrong, Please try again later');
        }
      }, (error) => {
        this.messageService.showProgressBar(false);
        this.utils.toastr.showError(error.json().error.message);
        if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
          setTimeout(() => {
            this.authService.logout();
          }, 2000);
        }
      });
    }
  }
})

    } else {
      this.showError = true
    }
  }

  removeError() {
    this.responseSuccess = false;
    this.responseFail = false;
    // this.showError = false;
  }

  onDateChanged(event: IMyDateModel, source): void {
    if (source === 0) {
      this.addUserCommand.licenseIssueDate = event.formatted;
    } else if (source === 1) {
      this.addUserCommand.licenseExpiryDate = event.formatted;
    }
  }

  uploadImage() {
    this.utils.image.uploadProfilePicture('userPictureAdd');
  }

  setGender(gender: string) {
    this.addUserCommand.gender = gender;
  }

  setEmployeeType(employeeType: string) {
    this.addUserCommand.employeeType = employeeType;
  }

}
