import { AuthGuard } from '../auth/auth.guard';
import { Routes } from '@angular/router';

import { ComingSoonComponent } from './coming-soon/coming-soon.component';
import { LockScreenComponent } from './lockscreen/lockscreen.component';
import { SubscribesComponent } from './subscribes/subscribes.component';
import { UnderMaintanceComponent } from './under-maintance/under-maintance.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AddUserComponent } from './add-user/add-user.component';
import { MainComponent } from 'app/main/main.component';
import { ListUserComponent } from 'app/session/list-user/list-user.component';
import { CreateNewFleetBusinessComponent } from 'app/session/create-new-fleet-business/create-new-fleet-business.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserProfileComponent } from 'app/session/user-profile/user-profile.component';
import { AddVehicleComponent } from './add-vehicle/add-vehicle.component';
import { ListVehicleComponent } from './list-vehicle/list-vehicle.component';
import { UpdateVehicleComponent } from './update-vehicle/update-vehicle.component';
import { CreateRosterComponent } from './create-roster/create-roster.component';
import { ListRosterComponent } from './list-roster/list-roster.component';
import { UpdateRosterComponent } from './update-roster/update-roster.component';
// import { FleetLocationFormComponent } from './fleet-location-form/fleet-location-form.component';
import { ListLocationComponent } from './list-location/list-location.component';
import { AddLocationComponent } from './add-location/add-location.component';
import { EditLocationComponent } from './edit-location/edit-location.component';

export const SessionRoutes: Routes = [{
  path: 'session',
  component: MainComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'coming-soon',
      component: ComingSoonComponent
    }, {
      path: 'lockscreen',
      component: LockScreenComponent
    }, {
      path: 'not-found',
      component: NotFoundComponent
    }, {
      path: 'subscribes',
      component: SubscribesComponent
    }, {
      path: 'undermaintance',
      component: UnderMaintanceComponent
    },
    // {
    //   path: 'fleet-location',
    //   component: FleetLocationFormComponent
    // },
    {
      path: 'list-location',
      component: ListLocationComponent
    },{
      path: 'add-user',
      component: AddUserComponent
    },
    {
      path: 'list-user',
      component: ListUserComponent
    },
    {
      path: 'create-fleet',
      component: CreateNewFleetBusinessComponent
    },
    {
      path: 'add-location',
      component: AddLocationComponent
    },
    {
      path: 'edit-user',
      component: EditUserComponent
    },
    {
      path: 'user-profile',
      component: UserProfileComponent
    },
    {
      path: 'add-vehicle',
      component: AddVehicleComponent
    },
    {
      path: 'list-vehicle',
      component: ListVehicleComponent
    },
    {
      path: 'update-vehicle',
      component: UpdateVehicleComponent
    },
    {
      path: 'create-roster',
      component: CreateRosterComponent
    },
    {
      path: 'list-roster',
      component: ListRosterComponent
    },
    {
      path: 'update-roster',
      component: UpdateRosterComponent
    },
    {
      path: 'edit-location',
      component: EditLocationComponent
    }
  ]
}];
