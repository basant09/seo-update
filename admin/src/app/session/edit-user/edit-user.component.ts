import { MessageService } from '../../services/message.service';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {
  GetFleetUserDetailCommand, FleetUserDetailCommand, AddUserCommand, GetDefaultFuelRestrictionCommand,
  DefaultExpenseLimitCommand, EmployeeTypeCommand
} from 'app/typings/Typings';
import { INgxMyDpOptions, IMyDateModel, IMyDate } from 'ngx-mydatepicker';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { RoutingConstantService } from 'app/services/routingConstants.service';
import { Utils } from 'app/utils/utils.util';
import { DEFAULT_USER_PIC } from 'app/utils/constants.util';
import { ConfirmationModalComponent } from '../../ui-elements/confirmation-modal/confirmation-modal.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'pap-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  empInfoId: number;
  getFleetUserDetailCommand: GetFleetUserDetailCommand;
  fleetUserDetail: FleetUserDetailCommand;
  addUserCommand: AddUserCommand
  licenseIssueDate: any;
  licenseExpiryDate: any;
  responseSuccess: boolean = false;
  showError: boolean = false;
  responseFail: boolean = false;
  responseMessage: string;
  todayDate: Date = new Date();
  disableSince: IMyDate;
  issueDateOptions: INgxMyDpOptions;
  expiryDateOptions: INgxMyDpOptions;
  defaultUserPic: string;
  genderArray = ['Female', 'Male', 'Other'];
  fuelGradesArray: string[] = new Array();
  fuelGradeRestrictionArray: string[] = new Array();
  employeeTypeArray: string[] = new Array();
  defaultFuelRestriction: GetDefaultFuelRestrictionCommand;
  todayDateValue: string;

  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService,
    private pageTitleService: PageTitleService, private routingConstantService: RoutingConstantService,
    private messageService: MessageService, public utils: Utils,  private modalService: NgbModal) {
    this.empInfoId = null;
    this.getFleetUserDetailCommand = {
      empInfoId: null
    }
    this.fleetUserDetail = {
      dailyExpenseLimit: null,
      email: null,
      emergencyContact: null,
      employeeId: null,
      employeeType: null,
      fuelGradesRestriction: null,
      firstName: null,
      lastName: null,
      licenseExpiryDate: null,
      licenseIssueDate: null,
      licenseNumber: null,
      mobile: null,
      monthlyExpenseLimit: null,
      phone: null,
      userId: null,
      weeklyExpenseLimit: null,
      gender: null,
      image: null,
      fleetUserStatus: null,
      fuelGrades: null,
      isEmployeeActive: null
    };
    this.licenseIssueDate = null;
    this.licenseExpiryDate = null;
    this.fuelGradesArray = [];
    this.fuelGradeRestrictionArray = [];
    this.employeeTypeArray = [];
    this.todayDateValue = null;
  }

  ngOnInit() {
    this.pageTitleService.setTitle('Edit User');
    this.route.queryParams.subscribe((params) => {
      if (params != null) {
        this.empInfoId = params['id'];
        this.getFleetUserDetailCommand.empInfoId = params['id'];
        this.getFleetUserDetail();
        this.defaultUserPic = DEFAULT_USER_PIC;
      }
    });
  }

  getFleetUserDetail() {
    this.messageService.showProgressBar(true);
    this.authService.getFleetUserDetail(this.getFleetUserDetailCommand).subscribe((res: FleetUserDetailCommand) => {
      this.messageService.showProgressBar(false);
      this.getEmployeeType();
      this.fleetUserDetail = res;
      const todayDate: Date = new Date();
      this.disableSince = {
        year: todayDate.getFullYear(),
        month: todayDate.getMonth() + 1,
        day: todayDate.getDate()
      };
      this.todayDateValue = this.utils.dateUtil.getHtmlDateFormat(todayDate);
      this.fleetUserDetail.licenseIssueDate = this.utils.dateUtil.getHtmlDateFormat(new Date(this.fleetUserDetail.licenseIssueDate));
      this.fleetUserDetail.licenseExpiryDate = this.utils.dateUtil.getHtmlDateFormat(new Date(this.fleetUserDetail.licenseExpiryDate));
      this.fleetUserDetail.fuelGrades.sort(this.utils.sort.sortAsc);
      this.issueDateOptions = {
        dateFormat: 'dd-mm-yyyy',
        markCurrentDay: true,
        disableSince: this.disableSince
      };
      this.expiryDateOptions = {
        dateFormat: 'dd-mm-yyyy',
        markCurrentDay: true,
        disableUntil: this.disableSince
      };
      const issueDate: Date = new Date(this.fleetUserDetail.licenseIssueDate);
      this.licenseIssueDate = { date: { year: issueDate.getFullYear(), month: issueDate.getMonth() + 1, day: issueDate.getDate() } };
      const expiryDate: Date = new Date(this.fleetUserDetail.licenseExpiryDate);
      this.licenseExpiryDate = { date: { year: expiryDate.getFullYear(), month: expiryDate.getMonth() + 1, day: expiryDate.getDate() } };
      if (this.fleetUserDetail.image === '' || this.fleetUserDetail.image === null) {
        this.fleetUserDetail.image = this.defaultUserPic;
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    })
  }

  getEmployeeType() {
    this.messageService.showProgressBar(true);
    this.authService.getEmployeeType().subscribe((resp: EmployeeTypeCommand) => {
      this.messageService.showProgressBar(false);
      if (resp.status === 'success') {
        this.employeeTypeArray = resp.emp_type;
        this.employeeTypeArray.sort(this.utils.sort.sortAsc);
      } else {
        this.utils.toastr.showError('No employee type found');
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  editUser(editUserForm) {
    this.addUserCommand = {
      dailyExpenseLimit: this.fleetUserDetail.dailyExpenseLimit,
      email: this.fleetUserDetail.email,
      emergencyContact: this.fleetUserDetail.emergencyContact,
      employeeId: this.fleetUserDetail.employeeId,
      employeeType: this.fleetUserDetail.employeeType,
      fuelGradesRestriction: this.fleetUserDetail.fuelGradesRestriction,
      firstName: this.fleetUserDetail.firstName,
      lastName: this.fleetUserDetail.lastName,
      licenseExpiryDate: this.utils.dateUtil.dateMMDDYYYY(this.fleetUserDetail.licenseExpiryDate),
      licenseIssueDate: this.utils.dateUtil.dateMMDDYYYY(this.fleetUserDetail.licenseIssueDate),
      licenseNumber: this.fleetUserDetail.licenseNumber,
      mobile: this.fleetUserDetail.mobile,
      monthlyExpenseLimit: this.fleetUserDetail.monthlyExpenseLimit,
      phone: this.fleetUserDetail.phone,
      userId: this.fleetUserDetail.email,
      weeklyExpenseLimit: this.fleetUserDetail.weeklyExpenseLimit,
      gender: this.fleetUserDetail.gender,
      image: document.getElementById('userPicEdit').getAttribute('src'),
    };
    if (this.addUserCommand.image === this.defaultUserPic) {
      this.addUserCommand.image = null;
    }
    if (editUserForm.valid) {
      const modalRef = this.modalService.open(ConfirmationModalComponent);
      modalRef.componentInstance.heading = 'User Confirmation';
      modalRef.componentInstance.heading1 = 'Are you sure want to update details ?';
      modalRef.result.then(res1 => {
        if (res1 !== undefined) {
         if (res1 === 1) {
      this.addUserCommand.userId = this.addUserCommand.email;
      this.messageService.showProgressBar(true);
      this.authService.updateUser(this.addUserCommand).subscribe((res: any) => {
        this.messageService.showProgressBar(false);
        if (res.status === 'success') {
          this.utils.toastr.showSuccess('User Updated Successfully');
          setTimeout(() => {
            this.router.navigateByUrl(this.routingConstantService.getListUserRouterLink());
          }, 2000);
        } else {
          this.utils.toastr.showError('');
        }
      }, (error) => {
        this.messageService.showProgressBar(false);
        this.utils.toastr.showError(error.json().error.message);
        if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
          setTimeout(() => {
            this.authService.logout();
          }, 2000);
        }
      });
    }
  }
})

    } else {
      this.showError = true
    }
  }

  removeError() {
    this.responseSuccess = false;
    this.responseFail = false;
    this.showError = false;
  }

  onDateChanged(event: IMyDateModel, source): void {
    if (source === 0) {
      this.addUserCommand.licenseIssueDate = event.formatted;
    } else if (source === 1) {
      this.addUserCommand.licenseExpiryDate = event.formatted;
    }
  }

  uploadImage() {
    this.utils.image.uploadProfilePicture('userPicEdit');
  }

  setGender(gender: string) {
    this.fleetUserDetail.gender = gender;
  }

  setEmployeeType(employeeType: string) {
    this.fleetUserDetail.employeeType = employeeType;
  }

  cancelButton() {
    this.router.navigateByUrl(this.routingConstantService.getListUserRouterLink());
  }

}
