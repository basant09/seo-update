import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateNewFleetBusinessComponent } from './create-new-fleet-business.component';

describe('CreateNewFleetBusinessComponent', () => {
  let component: CreateNewFleetBusinessComponent;
  let fixture: ComponentFixture<CreateNewFleetBusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateNewFleetBusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewFleetBusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
