import { MessageService } from '../../services/message.service';
import { RoutingConstantService } from '../../services/routingConstants.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Component, OnInit, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import {
  CreateFleetProfileCommand, AddFleetLocationCommand, FleetProfileDetailCommand,
  UpdateFleetProfileCommand, GetFleetLocationsCommand, DefaultExpenseLimitCommand, SetDefaultExpenseLimitCommand, GetDefaultFuelRestrictionCommand, SetDefaultFuelRestrictionCommand
} from 'app/typings/Typings';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { FleetLocationFormComponent } from '../common/fleet-location-form/fleet-location-form.component';
import { Utils } from '../../utils/utils.util';
import { ConfirmationModalComponent } from '../../ui-elements/confirmation-modal/confirmation-modal.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'pap-create-new-fleet-business',
  templateUrl: './create-new-fleet-business.component.html',
  styleUrls: ['./create-new-fleet-business.component.scss']
})
export class CreateNewFleetBusinessComponent implements OnInit {

  createFleetProfileCommand: CreateFleetProfileCommand;
  responseSuccess: boolean;
  showError: boolean;
  responseFail: boolean;
  responseMessage: string;
  entityTypeError: boolean
  entityTypeArray: string[] = new Array();
  showErrorDropdown: boolean;
  noOfLocations: number;
  visible: boolean;
  visible1: boolean;
  visible2: boolean;
  // addFleetLocation: AddFleetLocationCommand[] = [];
  checkLocationFromErrors: boolean;
  locationFormValidationArray: boolean[] = new Array();
  fleetProfileDetailCommand: FleetProfileDetailCommand;
  isProfileAlreadyCreated: boolean;
  updateFleetProfileCommand: UpdateFleetProfileCommand;
  fleetLocationsCommand: GetFleetLocationsCommand[];
  isAnyLocationPresent: boolean;
  fleetLocationsCommandTemp: GetFleetLocationsCommand[];
  setDefaultExpenseLimitCommand: SetDefaultExpenseLimitCommand;
  fuelGradesArray: string[] = new Array();
  fuelGradeRestrictionArray: string[] = new Array();
  setDefaultFuelRestrictionCommand: SetDefaultFuelRestrictionCommand;

  constructor(private authService: AuthService, private router: Router,
    private routingConstantService: RoutingConstantService, private messageService: MessageService,
    private pageTitleService: PageTitleService, private componentFactoryResolver: ComponentFactoryResolver,
    private viewContainerRef: ViewContainerRef, public utils: Utils, private modalService: NgbModal) {
    this.createFleetProfileCommand = {
      fleetName: null,
      entityType: null,
      ACN: null,
      ABN: null
    };
    this.entityTypeArray = ['Company', 'Sole Trader', 'Trust'];
    this.noOfLocations = 0;
    this.visible = false;
    this.visible1 = false;
    this.visible2 = false;
    this.checkLocationFromErrors = false;
    this.locationFormValidationArray = [];
    this.isProfileAlreadyCreated = false;
    this.fleetProfileDetailCommand = {
      ABN: null,
      ACN: null,
      entityType: null,
      fleetId: null,
      fleetName: null
    };
    this.updateFleetProfileCommand = {
      ABN: null,
      ACN: null,
      entityType: null,
      fleetId: null,
      fleetName: null
    };
    this.isAnyLocationPresent = false;
    this.responseSuccess = false;
    this.showError = false;
    this.responseFail = false;
    this.responseMessage = '';
    this.setDefaultExpenseLimitCommand = {
      dailyExpenseLimit: null,
      monthlyExpenseLimit: null,
      weeklyExpenseLimit: null
    }
    this.fuelGradesArray = [];
    this.fuelGradeRestrictionArray = [];
    this.setDefaultFuelRestrictionCommand = {
      fuelGradesRestriction: []
    }
  }

  ngOnInit() {
    this.getFleetProfileDetails();
  }

  getFleetProfileDetails() {
    setTimeout(() => {
      this.removeError();
    }, 2000);
    this.messageService.showProgressBar(true);
    this.authService.getFleetProfile().subscribe((res: FleetProfileDetailCommand) => {
      this.messageService.showProgressBar(false);
      this.fleetProfileDetailCommand = res;
      this.setFleetProfileDetails();
      this.getDefaultFuelRestriction();
      this.getDefaultExpenseLimit();
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      this.utils.auth.isTokenExpired(error);
    });
  }

  setFleetProfileDetails() {
    if (this.fleetProfileDetailCommand.fleetName !== null) {
      this.isProfileAlreadyCreated = true;
      this.createFleetProfileCommand.ABN = this.fleetProfileDetailCommand.ABN;
      this.createFleetProfileCommand.ACN = this.fleetProfileDetailCommand.ACN;
      this.createFleetProfileCommand.entityType = this.fleetProfileDetailCommand.entityType;
      this.createFleetProfileCommand.fleetName = this.fleetProfileDetailCommand.fleetName;
    }
  }

  createNewFleetBusiness(newFleetForm) {
    this.messageService.checkFleetLocationFormError(true);
    if (newFleetForm.valid) {
      this.messageService.showProgressBar(true);
      this.authService.createProfile(this.createFleetProfileCommand).subscribe((res: any) => {
        this.messageService.showProgressBar(false);
        this.setDefaultFuelRestriction();
        this.setDefaultExpenseLimit();
        this.utils.toastr.showSuccess('Fleet added successfully');
      }, (error) => {
        this.messageService.showProgressBar(false);
        this.utils.toastr.showError(error.json().error.message);
        this.utils.auth.isTokenExpired(error);
      });
    } else {
      this.checkBlankField('', true);
      this.showError = true;
    }
  }

  removeError() {
    this.responseSuccess = false;
    this.responseFail = false;
  }

  setEntityType(entityType: string) {
    this.createFleetProfileCommand.entityType = entityType;
    this.checkBlankField('entityType');
  }

  checkBlankField(field: string, checkAllFields?: boolean) {
    if (checkAllFields) {

    } else {
      checkAllFields = false;
    }
    if (field === 'entityType' || checkAllFields) {
      if (this.createFleetProfileCommand.entityType === null) {
        this.showErrorDropdown = true;
        this.entityTypeError = true;
      } else {
        this.entityTypeError = false;
      }
    }
  }

  setCommandForUpdateProfile() {
    this.updateFleetProfileCommand.ABN = this.createFleetProfileCommand.ABN;
    this.updateFleetProfileCommand.ACN = this.createFleetProfileCommand.ACN;
    this.updateFleetProfileCommand.entityType = this.createFleetProfileCommand.entityType;
    this.updateFleetProfileCommand.fleetName = this.createFleetProfileCommand.fleetName;
    this.updateFleetProfileCommand.fleetId = this.fleetProfileDetailCommand.fleetId;
  }

  updateFleetProfile() {
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.heading = ' Confirmation';
    modalRef.componentInstance.heading1 = 'Are you sure want to update details ?';
    modalRef.result.then(res1 => {
      if (res1 !== undefined) {
        if (res1 === 1) {
          this.setCommandForUpdateProfile();
          this.messageService.showProgressBar(true);
          this.authService.updateProfile(this.updateFleetProfileCommand).subscribe((res: any) => {
            this.messageService.showProgressBar(false);
            if (res.status === 'success') {
              this.setDefaultFuelRestriction();
              this.setDefaultExpenseLimit();
              this.utils.toastr.showSuccess('Fleet updated successfully');
              this.getFleetProfileDetails();
            }
          }, (error) => {
            this.messageService.showProgressBar(false);
            this.utils.toastr.showError(error.json().error.message);
            this.utils.auth.isTokenExpired(error);
          });
        }
      }
    });
  }

  getDefaultExpenseLimit() {
    this.messageService.showProgressBar(true);
    this.authService.getDefaultExpenseLimit().subscribe((resp: DefaultExpenseLimitCommand) => {
      this.messageService.showProgressBar(false);
      if (resp.status === 'success') {
        this.setDefaultExpenseLimitCommand.weeklyExpenseLimit = resp.weeklyExpenseLimit;
        this.setDefaultExpenseLimitCommand.dailyExpenseLimit = resp.dailyExpenseLimit;
        this.setDefaultExpenseLimitCommand.monthlyExpenseLimit = resp.monthlyExpenseLimit;
      } else {
        this.utils.toastr.showError('No expense limit found');
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  setDefaultExpenseLimit() {
    this.messageService.showProgressBar(true);
    this.authService.setDefaultExpenseLimit(this.setDefaultExpenseLimitCommand).subscribe((resp: DefaultExpenseLimitCommand) => {
      this.messageService.showProgressBar(false);
      if (resp.status === 'success') {
      } else {
        this.utils.toastr.showError('No expense limit found');
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getDefaultFuelRestriction() {
    this.messageService.showProgressBar(true);
    this.authService.getDefaultFuelRestriction().subscribe((res: GetDefaultFuelRestrictionCommand) => {
      this.messageService.showProgressBar(false);
      if (res.status === 'success') {
        this.fuelGradesArray = res.fuelGrades;
        this.fuelGradesArray.sort(this.utils.sort.sortAsc);
        this.setDefaultFuelRestrictionCommand.fuelGradesRestriction = res.fuelGradesRestriction;
      } else {
        this.utils.toastr.showError('No fuel restrictions found');
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  setDefaultFuelRestriction() {
    this.messageService.showProgressBar(true);
    this.authService.setDefaultFuelRestriction(this.setDefaultFuelRestrictionCommand).subscribe((res: GetDefaultFuelRestrictionCommand) => {
      this.messageService.showProgressBar(false);
      if (res.status === 'success') {
      } else {
        this.utils.toastr.showError('No fuel restrictions found');
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  searchFilter(event) {
    const val = event.target.value;
    const temp = this.fleetLocationsCommandTemp.filter((d) => {
      return (d.locationName.toLowerCase().indexOf(val) !== -1 || d.addressLine.toLowerCase().indexOf(val) !== -1 || !val);
    });
    this.fleetLocationsCommand = temp;
  }
  filterAscOfName(element1, element2): number {
    try {
      if (element1.locationName > element2.locationName) {
        return -1;
      }
      if (element1.locationName < element2.locationName) {
        return 1;
      }
      if (element1.locationName === element2.locationName) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfContactPerson(element1, element2): number {
    try {
      if (element1.contactPerson > element2.contactPerson) {
        return -1;
      }
      if (element1.contactPerson < element2.contactPerson) {
        return 1;
      }
      if (element1.contactPerson === element2.contactPerson) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfAddress(element1, element2): number {
    try {
      if (element1.addressLine > element2.addressLine) {
        return -1;
      }
      if (element1.addressLine < element2.addressLine) {
        return 1;
      }
      if (element1.addressLine === element2.addressLine) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfName(element1, element2): number {
    try {
      if (element1.locationName > element2.locationName) {
        return 1;
      }
      if (element1.locationName < element2.locationName) {
        return -1;
      }
      if (element1.locationName === element2.locationName) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfAddress(element1, element2): number {
    try {
      if (element1.addressLine > element2.addressLine) {
        return 1;
      }
      if (element1.addressLine < element2.addressLine) {
        return -1;
      }
      if (element1.addressLine === element2.addressLine) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfContactPerson(element1, element2): number {
    try {
      if (element1.contactPerson > element2.contactPerson) {
        return 1;
      }
      if (element1.contactPerson < element2.contactPerson) {
        return -1;
      }
      if (element1.contactPerson === element2.contactPerson) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  sortName(status: boolean) {
    if (status) {
      this.fleetLocationsCommand.sort(this.filterAscOfName);
    } else {
      this.fleetLocationsCommand.sort(this.filterDescOfName);
    }
  }
  sortAddress(status: boolean) {
    if (status) {
      this.fleetLocationsCommand.sort(this.filterAscOfAddress);
    } else {
      this.fleetLocationsCommand.sort(this.filterDescOfAddress);
    }
  }
  sortContactName(status: boolean) {
    if (status) {
      this.fleetLocationsCommand.sort(this.filterAscOfContactPerson);
    } else {
      this.fleetLocationsCommand.sort(this.filterDescOfContactPerson);
    }
  }
  toggleName() {
    this.visible = !this.visible;
  }
  toggleAddress() {
    this.visible1 = !this.visible1;
  }
  toggleContactName() {
    this.visible2 = !this.visible2;
  }
}
