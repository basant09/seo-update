import { Component, OnInit } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { CreateRosterCommand, RosterAvailableUser, RosterRequestCommand, RosterAvailableVehicle, RosterStatusCommand, RosterOccurrencesCommand } from '../../typings/Typings';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DriverListModalComponent } from '../modals/driver-list-modal/driver-list-modal.component';
import { Utils } from 'app/utils/utils.util';
import { MessageService } from 'app/services/message.service';
import { AuthService } from 'app/services/auth.service';
import { promise } from 'selenium-webdriver';
import { DEFAULT_USER_PIC } from 'app/utils/constants.util';
import { VehicleListModalComponent } from '../modals/vehicle-list-modal/vehicle-list-modal.component';
import { Router } from '@angular/router';
import { RoutingConstantService } from 'app/services/routingConstants.service';
import { INgxMyDpOptions, IMyDateModel, IMyDate } from 'ngx-mydatepicker';
declare var $: any;
import { ConfirmationModalComponent } from '../../ui-elements/confirmation-modal/confirmation-modal.component';
// import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'pap-create-roster',
  templateUrl: './create-roster.component.html',
  styleUrls: ['./create-roster.component.scss']
})
export class CreateRosterComponent implements OnInit {

  occourencesArray: string[] = new Array();
  createRosterCommand: CreateRosterCommand;
  dateArray: string[] = new Array();
  timeArray: string[] = new Array();
  isWeekdaysVisible: boolean;
  selectedDaysArray: any[][] = new Array();
  summaryString: string;
  rosterRequestCommand: RosterRequestCommand;
  rosterAvailableUser: RosterAvailableUser[];
  defaultUserPic: string;
  rosterAvailableVehicle: RosterAvailableVehicle[];
  selectedUser: RosterAvailableUser;
  selectedVehicle: RosterAvailableVehicle;
  statusArray: string[] = new Array();
  showErrorDropdown: boolean;
  validateFields: {
    endDate: boolean,
    endTime: boolean,
    initialOdometerRead: boolean,
    occurrence: boolean,
    rosterStatus: boolean,
    selectedDays: boolean,
    startDate: boolean,
    startTime: boolean,
    userId: boolean,
    vehicleId: boolean
  };
  selectDriverError: boolean;
  selectVehicleError: boolean;
  responseSuccess: boolean;
  responseFail: boolean;
  responseMessage: string;
  dateTomorrow: Date;
  dateToday: Date;
  StartDateOptions: INgxMyDpOptions;
  EndDateOptions: INgxMyDpOptions;
  startDate: any;
  endDate: any;
  startDateBkp: any;
  endDateBkp: any;
  validateOccurrence: boolean;
  startDateHtml: any;
  endDateHtml: any;
  startDateBkpHtml: any;
  endDateBkpHtml: any;
  rosterStatusCommand: RosterStatusCommand;
  rosterOccurrencesCommand: RosterOccurrencesCommand;

  constructor(private pageTitleService: PageTitleService, private modalService: NgbModal,
    public utils: Utils, private messageService: MessageService, private authService: AuthService,
    private router: Router, private routingConstantService: RoutingConstantService) {
    this.occourencesArray = [];
    this.statusArray = [];
    this.createRosterCommand = {
      endDate: null,
      endTime: '17:00',
      initialOdometerRead: null,
      occurrence: null,
      rosterStatus: null,
      selectedDays: [],
      startDate: null,
      startTime: '09:00',
      userId: null,
      vehicleId: null,
      summary: null
    };
    this.selectedDaysArray = [['Mon', true, 'Monday'], ['Tue', true, 'Tuesday'], ['Wed', true, 'Wednesday'],
    ['Thur', true, 'Thursday'], ['Fri', true, 'Friday'], ['Sat', false, 'Saturday'], ['Sun', false, 'Sunday']];
    this.dateToday = new Date();
    this.dateTomorrow = new Date();
    this.dateTomorrow.setDate(this.dateToday.getDate() + 1);
    this.dateArray.push(this.dateToday.getDate() + '-' + this.dateToday.getMonth() + 1 + '-' + this.dateToday.getFullYear());
    this.dateArray.push(this.dateTomorrow.getDate() + '-' + this.dateTomorrow.getMonth() + 1 + '-' + this.dateTomorrow.getFullYear());
    this.timeArray.push('00:00');
    this.timeArray.push('00:30');
    for (let i = 1; i < 24; i++) {
      if (i < 10) {
        this.timeArray.push('0' + i + ':00');
        this.timeArray.push('0' + i + ':30');
      } else {
        this.timeArray.push(i + ':00');
        this.timeArray.push(i + ':30');
      }
    }
    this.isWeekdaysVisible = false;
    this.summaryString = '';
    this.rosterRequestCommand = {
      endDate: null,
      endTime: null,
      occurrence: null,
      selectedDays: [],
      startDate: null,
      startTime: null
    };
    this.defaultUserPic = DEFAULT_USER_PIC;
    this.selectedUser = {
      email: null,
      empId: null,
      firstName: null,
      gender: null,
      id: null,
      image: null,
      lastName: null,
      licenseNumber: null,
      mobile: null,
      phone: null,
      userId: null
    };
    this.selectedVehicle = {
      brand: null,
      id: null,
      model: null,
      registrationNumber: null
    };
    this.validateFields = {
      endDate: false,
      endTime: false,
      initialOdometerRead: false,
      occurrence: false,
      rosterStatus: false,
      selectedDays: false,
      startDate: false,
      startTime: false,
      userId: false,
      vehicleId: false
    };
    this.selectDriverError = false;
    this.selectVehicleError = false;
    this.responseFail = false;
    this.responseSuccess = false;
    this.responseMessage = '';
    this.validateOccurrence = true;
    this.startDateBkpHtml = this.utils.dateUtil.getHtmlDateFormat(this.dateToday);
    this.endDateBkpHtml = this.utils.dateUtil.getHtmlDateFormat(this.dateTomorrow);
  }

  ngOnInit() {
    this.pageTitleService.setTitle('Create Roster');
    this.getRosterOccurrences();
    this.getRosterStatus();
  }

  getRosterStatus() {
    this.messageService.showProgressBar(true);
    this.authService.getRosterStatus().subscribe((res: RosterStatusCommand) => {
      this.messageService.showProgressBar(false);
      if (res.status === 'success') {
        this.rosterStatusCommand = res;
        this.statusArray = this.rosterStatusCommand.rosterStatus;
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      this.utils.auth.isTokenExpired(error);
    });
  }

  getRosterOccurrences() {
    this.messageService.showProgressBar(true);
    this.authService.getRosterOccurrences().subscribe((res: RosterOccurrencesCommand) => {
      this.messageService.showProgressBar(false);
      if (res.status === 'success') {
        this.rosterOccurrencesCommand = res;
        this.occourencesArray = this.rosterOccurrencesCommand.rosterOccurrences;
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      this.utils.auth.isTokenExpired(error);
    });
  }

  setOccourences(occurrence) {
    this.createRosterCommand.occurrence = occurrence;
    if (occurrence === 'Today') {
      this.createRosterCommand.startDate = this.startDateBkpHtml
      this.createRosterCommand.endDate = this.startDateBkpHtml
      this.startDateHtml = this.startDateBkpHtml;
      this.endDateHtml = this.startDateBkpHtml;
      this.isWeekdaysVisible = false;
      this.validateFields.selectedDays = false;
      this.selectAllDays(false);
      this.checkBlankField('startDate');
      this.checkBlankField('endDate');
    } else if (occurrence === 'Tomorrow') {
      this.createRosterCommand.startDate = this.endDateBkpHtml
      this.createRosterCommand.endDate = this.endDateBkpHtml
      this.startDateHtml = this.endDateBkpHtml;
      this.endDateHtml = this.endDateBkpHtml;
      this.isWeekdaysVisible = false;
      this.validateFields.selectedDays = false;
      this.selectAllDays(false);
      this.checkBlankField('startDate');
      this.checkBlankField('endDate');
    } else if (occurrence === 'Weekly') {
      this.startDateHtml = this.startDateBkpHtml;
      this.selectAllDays(false, true);
      this.isWeekdaysVisible = true;
      this.endDateHtml = null;
      this.createRosterCommand.startDate = this.startDateHtml;
      this.createRosterCommand.endDate = this.endDateHtml;
      this.checkBlankField('startDate');
      this.validateFields.endDate = false;
      for (let i = 0; i < 5; i++) {
        this.selectedDaysArray[i][1] = true;
      }
    } else if (occurrence === 'Custom') {
      this.startDateHtml = this.startDateBkpHtml;
      this.selectAllDays(false);
      this.isWeekdaysVisible = false;
      this.endDateHtml = null;
      this.createRosterCommand.startDate = this.startDateHtml;
      this.createRosterCommand.endDate = this.endDateHtml;
      this.validateFields.endDate = false;
      this.checkBlankField('startDate');
    }
    this.checkBlankField('occurrence');
    this.selectDriverError = false;
    this.selectVehicleError = false;
    this.setSummaryString();
  }

  parseIDate(date): string {
    if (date) {
      return date.date.day + '-' + date.date.month + '-' + date.date.year
    }
  }

  setStartDate(date) {
    this.createRosterCommand.startDate = this.dateArray[date];
    this.checkBlankField('startDate');
    this.setSummaryString();
  }

  setEndDate(enddate) {
    this.createRosterCommand.endDate = enddate;
    this.checkBlankField('endDate');
    this.setSummaryString();
  }

  setStartTime() {
    this.checkBlankField('startTime');
    this.setSummaryString();
  }

  setEndTime() {
    this.checkBlankField('endTime');
    this.setSummaryString();
  }

  selectAllDays(status: boolean, setDefault?: boolean) {
    for (let i = 0; i < this.selectedDaysArray.length; i++) {
      this.selectedDaysArray[i][1] = status;
    }

    if (setDefault) {
      for (let i = 0; i < this.selectedDaysArray.length - 2; i++) {
        this.selectedDaysArray[i][1] = true;
      }
    }

  }

  daysClickHandler(event, index) {
    if (event.target.checked === true) {
      this.validateFields.selectedDays = false;
    }
    if (this.createRosterCommand.occurrence === 'Weekly') {
      this.selectedDaysArray[index][1] = true;
    } else {
      this.selectedDaysArray[index][1] = event.target.checked;
      this.setSummaryString();
    }

  }

  setSummaryString() {
    this.summaryString = '';
    if (this.createRosterCommand.occurrence === 'Today') {
      this.summaryString = 'Today ' + this.createRosterCommand.startDate + ' from ' +
        this.createRosterCommand.startTime + ' to ' + this.createRosterCommand.endTime;
    } else if (this.createRosterCommand.occurrence === 'Tomorrow') {
      this.summaryString = 'Tomorrow  ' + this.createRosterCommand.startDate + ' from ' +
        this.createRosterCommand.startTime + ' to ' + this.createRosterCommand.endTime;
    } else if (this.createRosterCommand.occurrence === 'Weekly') {
      let sDate = '';
      if (this.createRosterCommand.startDate) {
        sDate = this.createRosterCommand.startDate;
      }
      let eDate = '';
      if (this.createRosterCommand.endDate) {
        eDate = this.createRosterCommand.endDate;
      }
      this.summaryString = 'Weekly from ' + sDate + ' ' + this.createRosterCommand.startTime
        + ' to ' + eDate + ' ' + this.createRosterCommand.endTime;
    } else if (this.createRosterCommand.occurrence === 'Custom') {
      let sDate = '';
      if (this.createRosterCommand.startDate) {
        sDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.startDate);
      }
      let eDate = '';
      if (this.createRosterCommand.endDate) {
        eDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.endDate);
      }
      this.summaryString = this.summaryString.substr(0, this.summaryString.length - 2) +
        'From ' + sDate + ' ' + this.createRosterCommand.startTime
        + ' to ' + eDate + ' ' + this.createRosterCommand.endTime;
    }
  }

  getDayName(number): string {
    switch (number) {
      case 0:
        return 'Sunday';
      case 1:
        return 'Monday';
      case 2:
        return 'Tuesday';
      case 3:
        return 'Wednesday';
      case 4:
        return 'Thursday';
      case 5:
        return 'Friday';
      case 6:
        return 'Saturday';
    }
  }

  setRosterRequestCommand(): boolean {
    this.rosterRequestCommand.endDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.endDate);
    this.rosterRequestCommand.endTime = this.createRosterCommand.endTime;
    this.rosterRequestCommand.occurrence = this.createRosterCommand.occurrence;
    this.rosterRequestCommand.startDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.startDate);
    this.rosterRequestCommand.startTime = this.createRosterCommand.startTime;
    this.rosterRequestCommand.selectedDays = [];
    if (this.rosterRequestCommand.occurrence === 'Today') {
      const dayName = this.getDayName(this.dateToday.getDay());
      this.rosterRequestCommand.selectedDays.push(dayName);
    } else if (this.rosterRequestCommand.occurrence === 'Tomorrow') {
      const dayName = this.getDayName(this.dateTomorrow.getDay());
      this.rosterRequestCommand.selectedDays.push(dayName);
    } else if (this.rosterRequestCommand.occurrence === 'Weekly') {
      for (let i = 0; i < this.selectedDaysArray.length; i++) {
        if (this.selectedDaysArray[i][1]) {
          this.rosterRequestCommand.selectedDays.push(this.selectedDaysArray[i][2]);
        }
      }
    }
    let isCommandValid = true;
    for (const key in this.rosterRequestCommand) {
      if (this.rosterRequestCommand[key] === null || this.rosterRequestCommand[key] === '') {
        isCommandValid = false;
      }
    }
    if (this.rosterRequestCommand.selectedDays.length < 1) {
      isCommandValid = false;
    }
    return isCommandValid;
  }

  getDriverList() {
    this.selectDriverError = !this.setRosterRequestCommand();
    if (this.setRosterRequestCommand()) {
      this.selectDriverError = false;
      this.messageService.showProgressBar(true);
      this.authService.getAvailableUsers(this.rosterRequestCommand).subscribe((users: RosterAvailableUser[]) => {
        this.messageService.showProgressBar(false);
        if (users.length > 0) {
          this.rosterAvailableUser = users;
          const modalRef = this.modalService.open(DriverListModalComponent);
          modalRef.componentInstance.rosterAvailableUser = this.rosterAvailableUser;
          modalRef.componentInstance.selectedUser = this.selectedUser;
          modalRef.result.then(res => {
            if (res !== undefined) {
              this.selectedUser = res;
              this.setUserId();
            }
          });
        } else {

        }
      }, (error) => {
        this.messageService.showProgressBar(false);
        this.utils.toastr.showError(error.json().error.message);
        this.utils.auth.isTokenExpired(error);
      });
    } else {

    }
  }

  getUserImage(image) {
    if (image === null) {
      return this.defaultUserPic;
    } else {
      return image;
    }
  }

  getVehicleList() {
    this.selectVehicleError = !this.setRosterRequestCommand();
    if (this.setRosterRequestCommand()) {
      this.messageService.showProgressBar(true);
      this.authService.getAvailableVehicles(this.rosterRequestCommand).subscribe((users: RosterAvailableVehicle[]) => {
        this.messageService.showProgressBar(false);
        if (users.length > 0) {
          this.rosterAvailableVehicle = users;
          const modalRef = this.modalService.open(VehicleListModalComponent);
          modalRef.componentInstance.rosterAvailableVehicle = this.rosterAvailableVehicle;
          modalRef.componentInstance.selectedVehicle = this.selectedVehicle;
          modalRef.result.then(res => {
            if (res !== undefined) {
              this.selectedVehicle = res;
              this.setVehicleId();
            }
          });
        } else {

        }
      }, (error) => {
        this.messageService.showProgressBar(false);
        this.utils.toastr.showError(error.json().error.message);
        this.utils.auth.isTokenExpired(error);
      });
    } else {

    }
  }

  setVehicleId() {
    if (this.selectedVehicle.id !== null) {
      this.createRosterCommand.vehicleId = this.selectedVehicle.id;
    } else {

    }
    this.checkBlankField('vehicleId');
  }

  setUserId() {
    if (this.selectedUser.id !== null) {
      this.createRosterCommand.userId = this.selectedUser.userId;
    } else {

    }
    this.checkBlankField('userId');
  }

  setStatus(status) {
    this.createRosterCommand.rosterStatus = status;
    this.checkBlankField('rosterStatus');
  }

  occurrenceCheck() {
    setTimeout(() => {
      this.checkBlankField('occurrence');
    }, 1000);
  }

  checkBlankField(field: string, checkAllFields?: boolean) {
    if (checkAllFields) {

    } else {
      checkAllFields = false;
    }
    if (field === 'occurrence' || checkAllFields) {
      if (this.createRosterCommand.occurrence === null) {
        this.showErrorDropdown = true;
        this.validateFields.occurrence = true;
      } else {
        this.validateFields.occurrence = false;
      }
    }
    if (field === 'selectedDays' || checkAllFields) {
      let customDaysSelectedCount = 0;
      if (this.createRosterCommand.occurrence === 'Weekly') {
        for (let i = 0; i < this.selectedDaysArray.length; i++) {
          if (!this.selectedDaysArray[i][1]) {
            customDaysSelectedCount++;
            if (customDaysSelectedCount === this.selectedDaysArray.length) {
              this.showErrorDropdown = true;
              this.validateFields.selectedDays = true;
              this.isWeekdaysVisible = true;
            }
          }
        }
      }
    }
    if (field === 'startDate' || checkAllFields) {
      if (this.createRosterCommand.startDate === null || this.createRosterCommand.startDate === undefined) {
        this.showErrorDropdown = true;
        this.validateFields.startDate = true;
      } else {
        this.validateFields.startDate = false;
      }
    }
    if (field === 'startTime' || checkAllFields) {
      if (this.createRosterCommand.startTime === null) {
        this.showErrorDropdown = true;
        this.validateFields.startTime = true;
      } else {
        this.validateFields.startTime = false;
      }
    }
    if (field === 'endDate' || checkAllFields) {
        if (this.createRosterCommand.endDate === null || this.createRosterCommand.endDate === undefined) {
          this.showErrorDropdown = true;
          this.validateFields.endDate = true;
        } else {
          this.validateFields.endDate = false;
        }
    }
    if (field === 'endTime' || checkAllFields) {
      if (this.createRosterCommand.endTime === null) {
        this.showErrorDropdown = true;
        this.validateFields.endTime = true;
      } else {
        this.validateFields.endTime = false;
      }
    }
    if (field === 'userId' || checkAllFields) {
      if (this.createRosterCommand.userId === null) {
        this.showErrorDropdown = true;
        this.validateFields.userId = true;
      } else {
        this.validateFields.userId = false;
      }
    }
    if (field === 'vehicleId' || checkAllFields) {
      if (this.createRosterCommand.vehicleId === null) {
        this.showErrorDropdown = true;
        this.validateFields.vehicleId = true;
      } else {
        this.validateFields.vehicleId = false;
      }
    }
    if (field === 'rosterStatus' || checkAllFields) {
      if (this.createRosterCommand.rosterStatus === null) {
        this.showErrorDropdown = true;
        this.validateFields.rosterStatus = true;
      } else {
        this.validateFields.rosterStatus = false;
      }
    }
  }

  createRoster() {
    this.createRosterCommand.summary = this.summaryString;
    this.createRosterCommand.selectedDays = [];
    if (this.createRosterCommand.occurrence === 'Weekly') {
      for (let i = 0; i < this.selectedDaysArray.length; i++) {
        if (this.selectedDaysArray[i][1]) {
          this.createRosterCommand.selectedDays.push(this.selectedDaysArray[i][2]);
        }
      }
    }
    this.checkBlankField('', true);
    let isFormValid: boolean = true;
    for (const key in this.validateFields) {
      if (this.validateFields[key] === true) {
        isFormValid = false;
      }
    }
    if (isFormValid) {
      const modalRef = this.modalService.open(ConfirmationModalComponent);
      modalRef.componentInstance.heading = ' Confirmation';
      modalRef.componentInstance.heading1 = 'Are you sure want to add details ?';
      modalRef.result.then(res1 => {
        if (res1 !== undefined) {
          if (res1 === 1) {
            let command: CreateRosterCommand = new CreateRosterCommand();
            for (let key in this.createRosterCommand) {
              command[key] = this.createRosterCommand[key];
            }
            command.startDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.startDate);
            command.endDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.endDate);
            this.messageService.showProgressBar(true);
            this.authService.createRoster(command).subscribe((res: any) => {
              this.messageService.showProgressBar(false);
              if (res.status === 'success') {
                this.utils.toastr.showSuccess('Roster created successfully');
                setTimeout(() => {
                  this.router.navigateByUrl(this.routingConstantService.getListRosterRouterLink());
                }, 2000);
              }
            }, (error) => {
              try {
                this.messageService.showProgressBar(false);
                this.utils.toastr.showError(error.json().error.message);
                this.utils.auth.isTokenExpired(error);
              } catch (error) {
                this.utils.toastr.showError(error.statusText);
              }
            });
          }
        }
      })

    } else {
      this.checkBlankField('', true);
    }
  }

  onDateChanged(source): void {
    if (source === 0) {
      this.createRosterCommand.startDate = this.startDateHtml;
      this.checkBlankField('startDate');
    } else if (source === 1) {
      this.createRosterCommand.endDate = this.endDateHtml;
      this.checkBlankField('endDate');
    }
    this.setSummaryString();
  }

}
