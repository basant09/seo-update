import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverListModalComponent } from './driver-list-modal.component';

describe('DriverListModalComponent', () => {
  let component: DriverListModalComponent;
  let fixture: ComponentFixture<DriverListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
