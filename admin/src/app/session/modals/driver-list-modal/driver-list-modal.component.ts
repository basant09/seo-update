import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RosterAvailableUser } from 'app/typings/Typings';
import { DEFAULT_USER_PIC } from 'app/utils/constants.util';


@Component({
  selector: 'pap-driver-list-modal',
  templateUrl: './driver-list-modal.component.html',
  styles: ['./driver-list-modal.component.scss']
})
export class DriverListModalComponent implements OnInit, AfterViewInit {

  @Input() rosterAvailableUser: RosterAvailableUser[];
  @Input() selectedUser: RosterAvailableUser;
  defaultUserPic: string;
  isUserSelected: boolean;

  constructor(public activeModal: NgbActiveModal) {
    this.defaultUserPic = DEFAULT_USER_PIC;
    this.isUserSelected = false;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {

  }

  selectDriver(user: RosterAvailableUser, isChecked: boolean) {
    if (isChecked) {
      this.activeModal.close(user);
    }
  }

  getUserImage(image) {
    if (image === null) {
      return this.defaultUserPic;
    } else {
      return image;
    }
  }

  checkSelectedUser(user: RosterAvailableUser): boolean {
    let isChecked: boolean = false;
    if (this.selectedUser.id === user.id) {
      isChecked = true;
    }
    return isChecked;
  }

  closeModal() {
    this.activeModal.close(undefined);
  }

}
