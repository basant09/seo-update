import { Component, OnInit, Input } from '@angular/core';
import { RosterAvailableVehicle } from 'app/typings/Typings';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'pap-vehicle-list-modal',
  templateUrl: './vehicle-list-modal.component.html',
  styleUrls: ['./vehicle-list-modal.component.scss']
})
export class VehicleListModalComponent implements OnInit {

  @Input() rosterAvailableVehicle: RosterAvailableVehicle[];
  @Input() selectedVehicle: RosterAvailableVehicle;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  selectVehicle(vehicle: RosterAvailableVehicle, isChecked: boolean) {
    if (isChecked) {
      this.activeModal.close(vehicle);
    }
  }

  checkSelectedveicle(vehicle: RosterAvailableVehicle): boolean {
    let isChecked: boolean = false;
    if (this.selectedVehicle.id === vehicle.id) {
      isChecked = true;
    }
    return isChecked;
  }

  closeModal() {
    this.activeModal.close(undefined);
  }

}
