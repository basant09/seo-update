import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleListModalComponent } from './vehicle-list-modal.component';

describe('VehicleListModalComponent', () => {
  let component: VehicleListModalComponent;
  let fixture: ComponentFixture<VehicleListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
