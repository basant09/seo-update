import { RoutingConstantService } from '../../services/routingConstants.service';
import { Router } from '@angular/router';
import { MessageService } from '../../services/message.service';
import { AuthService } from '../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { VehicleDetailsCommand } from 'app/typings/Typings';
import { Utils } from 'app/utils/utils.util';

@Component({
  selector: 'pap-list-vehicle',
  templateUrl: './list-vehicle.component.html',
  styleUrls: ['./list-vehicle.component.scss']
})
export class ListVehicleComponent implements OnInit {
  vehicleFilterInput: any;
  vehicleDetails: VehicleDetailsCommand[];
  responseFail: boolean = false;
  responseMessage: string;
  responseSuccess: boolean = false;
  visible: boolean;
  visible1: boolean;
  visible2: boolean;
  visible3: boolean;
  vehicleDetailsTemp: VehicleDetailsCommand[];

  constructor(private pageTitleService: PageTitleService, private authService: AuthService, private messageService: MessageService,
    private router: Router, private routingConstantService: RoutingConstantService, public utils: Utils) {
    this.vehicleDetails = [];
    this.visible = false;
    this.visible1 = false;
    this.visible2 = false;
    this.visible3 = false;
  }

  ngOnInit() {
    this.pageTitleService.setTitle('Vehicle List');
    this.loadDetails();
  }

  loadDetails() {
    this.messageService.showProgressBar(true);
    this.authService.getVehicleList().subscribe((res: VehicleDetailsCommand[]) => {
      this.messageService.showProgressBar(false);
      this.vehicleDetails = res;
      this.vehicleDetailsTemp = res;
      if (this.vehicleDetails.length < 1) {
        // this.utils.toastr.showError('No record found');
        this.responseFail = true;
        this.responseMessage = 'No record found';
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    })
  }

  updateVehicle(vehicleId) {
    this.router.navigate([this.routingConstantService.getUpdateVehicleRouterLink()],
      { queryParams: { id: vehicleId } });
  }

  searchFilter() {
    const val = this.vehicleFilterInput;
    const temp = this.vehicleDetailsTemp.filter(function (d) {
      return (d.brand.toLowerCase().indexOf(val) !== -1 || d.model.toLowerCase().indexOf(val) !== -1 ||
        d.registrationNumber.toLowerCase().indexOf(val) !== -1 || d.vehicleStatus.toLowerCase().indexOf(val) !== -1
        || !val);
    });
    this.vehicleDetails = temp;
  }
  searchFilter1(event) {
    const val = event.target.value;
    const temp = this.vehicleDetailsTemp.filter(function (d) {
      return ( d.vehicleStatus.toLowerCase().startsWith(val));
    });
    this.vehicleDetails = temp;
  }
  filterAsc(element1, element2): number {
    try {
      if (element1.brand.toLowerCase() > element2.brand.toLowerCase()) {
        return -1;
      }
      if (element1.brand.toLowerCase() < element2.brand.toLowerCase()) {
        return 1;
      }
      if (element1.brand.toLowerCase() === element2.brand.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAsc1(element1, element2): number {
    try {
      if (element1.model.toLowerCase() > element2.model.toLowerCase()) {
        return -1;
      }
      if (element1.model.toLowerCase() < element2.model.toLowerCase()) {
        return 1;
      }
      if (element1.model.toLowerCase() === element2.model.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAsc2(element1, element2): number {
    try {
      if (element1.registrationNumber.toLowerCase() > element2.registrationNumber.toLowerCase()) {
        return -1;
      }
      if (element1.registrationNumber.toLowerCase() < element2.registrationNumber.toLowerCase()) {
        return 1;
      }
      if (element1.registrationNumber.toLowerCase() === element2.registrationNumber.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAsc3(element1, element2): number {
    try {
      if (element1.vehicleStatus.toLowerCase() > element2.vehicleStatus.toLowerCase()) {
        return -1;
      }
      if (element1.vehicleStatus.toLowerCase() < element2.vehicleStatus.toLowerCase()) {
        return 1;
      }
      if (element1.vehicleStatus.toLowerCase() === element2.vehicleStatus.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDesc(element1, element2): number {
    try {
      if (element1.brand.toLowerCase() > element2.brand.toLowerCase()) {
        return 1;
      }
      if (element1.brand.toLowerCase() < element2.brand.toLowerCase()) {
        return -1;
      }
      if (element1.brand.toLowerCase() === element2.brand.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDesc1(element1, element2): number {
    try {
      if (element1.model.toLowerCase() > element2.model.toLowerCase()) {
        return 1;
      }
      if (element1.model.toLowerCase() < element2.model.toLowerCase()) {
        return -1;
      }
      if (element1.model.toLowerCase() === element2.model.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDesc2(element1, element2): number {
    try {
      if (element1.registrationNumber.toLowerCase() > element2.registrationNumber.toLowerCase()) {
        return 1;
      }
      if (element1.registrationNumber.toLowerCase() < element2.registrationNumber.toLowerCase()) {
        return -1;
      }
      if (element1.registrationNumber.toLowerCase() === element2.registrationNumber.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDesc3(element1, element2): number {
    try {
      if (element1.vehicleStatus.toLowerCase() > element2.vehicleStatus.toLowerCase()) {
        return 1;
      }
      if (element1.vehicleStatus.toLowerCase() < element2.vehicleStatus.toLowerCase()) {
        return -1;
      }
      if (element1.vehicleStatus.toLowerCase() === element2.vehicleStatus.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  sortBrand(status: boolean) {
    if (status) {
      this.vehicleDetails.sort(this.filterAsc);
    } else {
      this.vehicleDetails.sort(this.filterDesc);
    }
  }
  sortModel(status: boolean) {
    if (status) {
      this.vehicleDetails.sort(this.filterAsc1);
    } else {
      this.vehicleDetails.sort(this.filterDesc1);
    }
  }
  sortRegistrationNumber(status: boolean) {
    if (status) {
      this.vehicleDetails.sort(this.filterAsc2);
    } else {
      this.vehicleDetails.sort(this.filterDesc2);
    }
  }
  sortStatus(status: boolean) {
    if (status) {
      this.vehicleDetails.sort(this.filterAsc3);
    } else {
      this.vehicleDetails.sort(this.filterDesc3);
    }
  }
  toggleBrand() {
    this.visible = !this.visible;
  }
  toggleModel() {
    this.visible1 = !this.visible1;
  }
  toggleRegistrationNumber() {
    this.visible2 = !this.visible2;
  }
  toggleStatus() {
    this.visible3 = !this.visible3;
  }
}
