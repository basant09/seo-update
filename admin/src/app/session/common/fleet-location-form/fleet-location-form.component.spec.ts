import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetLocationFormComponent } from './fleet-location-form.component';

describe('FleetLocationFormComponent', () => {
  let component: FleetLocationFormComponent;
  let fixture: ComponentFixture<FleetLocationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FleetLocationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetLocationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
