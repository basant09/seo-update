import { MessageService } from '../../../services/message.service';
import { RoutingConstantService } from '../../../services/routingConstants.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { Component, OnInit, Input, OnChanges, OnDestroy, NgZone } from '@angular/core';
import {
  AddFleetLocationCommand, StatesCommand, SupportedCountriesCommand,
  StatesRequestCommand, UpdateFleetLocationCommand, RemoveFleetLocationCommand, GetFleetLocationsCommand
} from 'app/typings/Typings';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { Utils } from 'app/utils/utils.util';
import { ConfirmationModalComponent } from '../../../ui-elements/confirmation-modal/confirmation-modal.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
declare var initMap: any;
declare var $;
declare var google: any;
declare var self: any;

@Component({
  selector: 'pap-fleet-location-form',
  templateUrl: './fleet-location-form.component.html',
  styleUrls: ['./fleet-location-form.component.scss']
})
export class FleetLocationFormComponent implements OnInit, OnChanges, OnDestroy {
  space: string;
  addFleetLocation: AddFleetLocationCommand;
  responseSuccess: boolean;
  showError: boolean;
  responseFail: boolean;
  responseMessage: string;
  showErrorDropdown: boolean;
  stateArray: string[] = new Array();
  stateError: boolean;
  @Input('locationNumber') locationNumber: number;
  @Input('locationId') locationId: number;
  countryArray: SupportedCountriesCommand[] = new Array();
  countryError: boolean;
  getStateCommand: StatesRequestCommand;
  @Input('cmpRef') cmpRef: any;
  isMapVisible: boolean;
  isFormValid: boolean;
  @Input('formValidationArray') formValidationArray: boolean[];
  markers: any[] = new Array;
  markerPosition: any;
  isNotHeadOffice: boolean;
  updateFleetLocationCommand: UpdateFleetLocationCommand;
  removeFleetLocationCommand: RemoveFleetLocationCommand;
  areLocationDetailsPresent: boolean;
  @Input('locationDetails') locationDetails: GetFleetLocationsCommand;
  fulladdress: string;
  latitude: any;
  longitude: any;

  constructor(private authService: AuthService, private router: Router,
    private routingConstantService: RoutingConstantService, private messageService: MessageService,
    private pageTitleService: PageTitleService, public utils: Utils, private modalService: NgbModal,
    private ngZone: NgZone) {
    this.addFleetLocation = {
      addressLine: null,
      city: null,
      contactPerson: null,
      contactPersonAltContact: null,
      contactPersonEmail: null,
      contactPersonMobile: null,
      contactPersonPhone: null,
      country: null,
      isHeadOffice: null,
      latitude: null,
      locationName: null,
      longitude: null,
      postCode: null,
      state: null,
      suburb: null
    };
    this.getStateCommand = {
      countryCode: null
    };
    this.markers = [];
    this.markerPosition = { lat: 0, lng: 0 };
    this.isFormValid = false;
    this.isNotHeadOffice = false;
    this.updateFleetLocationCommand = {
      addressLine: null,
      city: null,
      contactPerson: null,
      contactPersonAltContact: null,
      contactPersonEmail: null,
      contactPersonMobile: null,
      contactPersonPhone: null,
      country: null,
      isHeadOffice: null,
      latitude: null,
      locationName: null,
      longitude: null,
      postCode: null,
      state: null,
      suburb: null,
      locationId: null
    };
    this.removeFleetLocationCommand = {
      locationId: null
    }
    this.areLocationDetailsPresent = false;
    this.space = ' ';
    this.fulladdress = ' ';
    this.latitude = null;
    this.longitude = null;
  }

  ngOnInit() {
    self = this;
    if (this.locationId !== 0 && this.locationId !== null) {
      this.removeFleetLocationCommand.locationId = this.locationId;
      this.areLocationDetailsPresent = true;
      this.getLocationDetails();
    }
    setTimeout(() => {
      this.showMap(this);
    }, 20);
    this.messageService.checkFleetLocationFormErrorGet().subscribe((message: boolean) => {
      if (message) {
        this.setMarkerPosition();
        this.checkBlankField('', message);
      }
    });
    this.getSupportedCountries();
  }

  getLocationDetails() {
    this.setDetailsCommandToAddCommand();
  }

  updateLocation(fleetLocationForm) {
    this.checkBlankField(null, true);
    if (fleetLocationForm.valid) {
      this.setAddCommandToUpdateCommand();
      const modalRef = this.modalService.open(ConfirmationModalComponent);
      modalRef.componentInstance.heading = ' Confirmation';
      modalRef.componentInstance.heading1 = 'Are you sure want to update location details ?';
      modalRef.result.then(res1 => {
        if (res1 !== undefined) {
          if (res1 === 1) {
            this.messageService.showProgressBar(true);
            this.authService.updateFleetLocation(this.updateFleetLocationCommand).subscribe((res: any) => {
              this.messageService.showProgressBar(false);
              if (res.status === 'success') {
                this.utils.toastr.showSuccess('Location updated successfully');
                this.messageService.sendMessageArray(['reloadLocationList']);
                this.closeForm();
              }
            }, (error) => {
              this.messageService.showProgressBar(false);
              this.utils.toastr.showError(error.json().error.message);
              this.utils.auth.isTokenExpired(error);
            });
          }
        }
      })

    }
  }

  removeLocation() {
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.heading = 'Confirmation';
    modalRef.componentInstance.heading1 = 'Are you sure want to remove location ?';
    modalRef.result.then(res1 => {
      if (res1 !== undefined) {
        if (res1 === 1) {
          this.messageService.showProgressBar(true);
          this.authService.removeFleetLocation(this.removeFleetLocationCommand).subscribe((res: any) => {
            this.messageService.showProgressBar(false);
            this.utils.toastr.showSuccess('Location removed successfully');
            this.messageService.sendMessageArray(['reloadLocationList']);
            this.closeForm();
          }, (error) => {
            this.messageService.showProgressBar(false);
            this.utils.toastr.showError(error.json().error.message);
            this.utils.auth.isTokenExpired(error);
          });
        }
      }
    })
  }

  setDetailsCommandToAddCommand() {

    this.addFleetLocation.addressLine = this.locationDetails.addressLine;
    this.addFleetLocation.city = this.locationDetails.city;
    this.addFleetLocation.contactPerson = this.locationDetails.contactPerson;
    this.addFleetLocation.contactPersonAltContact = this.locationDetails.contactPersonAltContact;
    this.addFleetLocation.contactPersonEmail = this.locationDetails.contactPersonEmail;
    this.addFleetLocation.contactPersonMobile = this.locationDetails.contactPersonMobile;
    this.addFleetLocation.contactPersonPhone = this.locationDetails.contactPersonPhone;
    this.addFleetLocation.country = this.locationDetails.country;

    this.addFleetLocation.latitude = this.locationDetails.latitude;
    this.addFleetLocation.locationName = this.locationDetails.locationName;
    this.addFleetLocation.longitude = this.locationDetails.longitude;
    this.addFleetLocation.postCode = this.locationDetails.postCode;
    this.addFleetLocation.state = this.locationDetails.state;
    this.addFleetLocation.suburb = this.locationDetails.suburb;

    this.addFleetLocation.isHeadOffice = this.locationDetails.isHeadOffice;

    if (this.locationDetails.isHeadOffice) {
      this.isNotHeadOffice = false;
    } else {
      this.isNotHeadOffice = true;
    }
    for (const key in this.addFleetLocation) {
      if (this.addFleetLocation[key] === undefined) {
        this.addFleetLocation[key] = null;
      }
    }
  }

  setAddCommandToUpdateCommand() {
    this.updateFleetLocationCommand.addressLine = this.addFleetLocation.addressLine;
    this.updateFleetLocationCommand.city = this.addFleetLocation.city;
    this.updateFleetLocationCommand.contactPerson = this.addFleetLocation.contactPerson;
    this.updateFleetLocationCommand.contactPersonAltContact = this.addFleetLocation.contactPersonAltContact;
    this.updateFleetLocationCommand.contactPersonEmail = this.addFleetLocation.contactPersonEmail;
    this.updateFleetLocationCommand.contactPersonMobile = this.addFleetLocation.contactPersonMobile;
    this.updateFleetLocationCommand.contactPersonPhone = this.addFleetLocation.contactPersonPhone;
    this.updateFleetLocationCommand.country = this.addFleetLocation.country;
    this.updateFleetLocationCommand.isHeadOffice = this.addFleetLocation.isHeadOffice;
    this.updateFleetLocationCommand.latitude = this.addFleetLocation.latitude;
    this.updateFleetLocationCommand.locationName = this.addFleetLocation.locationName;
    this.updateFleetLocationCommand.longitude = this.addFleetLocation.longitude;
    this.updateFleetLocationCommand.postCode = this.addFleetLocation.postCode;
    this.updateFleetLocationCommand.state = this.addFleetLocation.state;
    this.updateFleetLocationCommand.suburb = this.addFleetLocation.suburb;
    this.updateFleetLocationCommand.locationId = this.locationId;
  }

  removeError() {
    this.responseSuccess = false;
    this.responseFail = false;
  }

  checkBlankField(field: string, checkAllFields?: boolean) {
    if (checkAllFields) {
      this.showError = true;
    } else {
      checkAllFields = false;
    }
    if (field === 'state' || checkAllFields) {
      if (this.addFleetLocation.state === null && this.stateArray.length > 0) {
        this.showErrorDropdown = true;
        this.stateError = true;
      } else {
        this.stateError = false;
      }
    }
    if (field === 'country' || checkAllFields) {
      if (this.addFleetLocation.country === null && this.countryArray.length > 0) {
        this.showErrorDropdown = true;
        this.countryError = true;
      } else {
        this.countryError = false;
      }
    }
    if (checkAllFields) {
      if (this.addFleetLocation.isHeadOffice === null || this.addFleetLocation.addressLine === null ||
        this.addFleetLocation.country === null || this.addFleetLocation.state === null ||
        this.addFleetLocation.city === null || this.addFleetLocation.contactPersonEmail === null ||
        this.addFleetLocation.contactPersonMobile === null || this.addFleetLocation.contactPerson === null) {
        this.isFormValid = false;
      } else {
        this.isFormValid = true;
      }
      this.formValidationArray[this.locationNumber] = this.isFormValid;
    }
  }

  getState(selectedCountry: SupportedCountriesCommand) {
    this.getStateCommand.countryCode = selectedCountry.countryCode;
    this.messageService.showProgressBar(true);
    this.authService.getStates(this.getStateCommand).subscribe((res: StatesCommand) => {
      this.messageService.showProgressBar(false);
      if (res.status === 'success') {
        this.stateArray = res.states;
        this.stateArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      this.utils.auth.isTokenExpired(error);
    });
  }

  setState(state: string) {
    this.addFleetLocation.state = state;
    this.stateError = false
  }

  setCountry(country: SupportedCountriesCommand) {
    this.addFleetLocation.country = country.country;
    this.countryError = false
    if (this.addFleetLocation.country !== null) {
      this.getState(country);
    }
  }

  getCountry(country: string) {
    if (country !== null) {
      try {
        this.stateArray = [];
        this.countryArray.forEach((element, index) => {
          if (country.toLowerCase() === element.country.toLowerCase()) {
            this.setCountry(element);
          }
        });
      } catch (err) {
        console.log(err);
      }
    }
  }

  getSupportedCountries() {
    this.messageService.showProgressBar(true);
    this.authService.getSupportedCountries().subscribe((res: SupportedCountriesCommand[]) => {
      this.messageService.showProgressBar(false);
      if (res.length > 0) {
        this.countryArray = res;
        this.getCountry(this.addFleetLocation.country);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      this.utils.auth.isTokenExpired(error);
    });
  }

  ngOnChanges() {
  }

  ngOnDestroy() {
  }

  closeForm() {
    this.formValidationArray.splice(this.locationNumber, 1);
    this.cmpRef.destroy();
  }

  showMap(localThis) {
    this.initializeMap(localThis);
  }

  toggleMap() {
    if (this.isMapVisible) {
      this.isMapVisible = false;
    } else {
      this.isMapVisible = true;
    }
  }

  setMarkerPosition() {
    if (this.addFleetLocation.latitude === '') {
      this.addFleetLocation.latitude = null;
    }
    if (this.addFleetLocation.longitude === '') {
      this.addFleetLocation.longitude = null;
    }
  }

  setHeadOffice(status: boolean) {
    if (status) {
      this.addFleetLocation.isHeadOffice = true;
      this.isNotHeadOffice = false;
    } else {
      this.addFleetLocation.isHeadOffice = false;
      this.isNotHeadOffice = true;
    }
  }

  returnMapId(): string {
    return 'map' + this.locationNumber;
  }

  returnInputBoxId(): string {
    return 'pac-input' + this.locationNumber;
  }

  initializeMap(localThis) {
    let latitude = -34.397;
    let longitude = 150.644;
    if (this.areLocationDetailsPresent && this.locationDetails.latitude !== undefined && this.locationDetails.longitude !== undefined) {
      latitude = +this.locationDetails.latitude;
      longitude = +this.locationDetails.longitude;
    }
    const map = new google.maps.Map(document.getElementById(this.returnMapId()), {
      center: { lat: latitude, lng: longitude },
      zoom: 8
    });
    const infoWindow = new google.maps.InfoWindow;

    if (this.areLocationDetailsPresent && this.locationDetails.latitude !== undefined && this.locationDetails.longitude !== undefined) {
      const pos = {
        lat: +this.locationDetails.latitude,
        lng: +this.locationDetails.longitude
      };
      infoWindow.setPosition(pos);
      map.setCenter(pos);
      this.setMarker(pos.lat, pos.lng, map);
    } else {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          infoWindow.setPosition(pos);
          map.setCenter(pos);
        }, function () {
          this.handleLocationError(true, infoWindow, map.getCenter(), map);
        });
      } else {
        this.handleLocationError(false, infoWindow, map.getCenter(), map);
      }
    }

    const input = document.getElementById(this.returnInputBoxId());
    const searchBox = new google.maps.places.SearchBox(input);

    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener('bounds_changed', function () {
      searchBox.setBounds(map.getBounds());
    });
    searchBox.addListener('places_changed', function () {
      const places = searchBox.getPlaces();
      if (places.length === 0) {
        return;
      }
      const bounds = new google.maps.LatLngBounds();
      places.forEach(function (place) {
        if (!place.geometry) {
          return;
        }
        const icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        if (place.geometry.viewport) {
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
        localThis.ngZone.run(() => {
          localThis.setMarker(place.geometry.location.lat(), place.geometry.location.lng(), map);
        })
      });
      map.fitBounds(bounds);
    });

    map.addListener('click', (event) => {
      this.setMarker(event.latLng.lat(), event.latLng.lng(), map);
    });
  }

  handleLocationError(browserHasGeolocation, infoWindow, pos, map) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
      'Error: The Geolocation service failed.' :
      'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
  }

  setMarker(lat, lng, map) {
    const markerCoordinates = { lat: lat, lng: lng }
    this.addFleetLocation.latitude = lat;
    this.addFleetLocation.longitude = lng;
    this.markerPosition = markerCoordinates;
    if (this.markers.length > 0) {
      this.markers.forEach(function (marker) {
        marker.setMap(null);
      });
      this.markers = [];
    }
    const markerPos = new google.maps.Marker({
      position: markerCoordinates,
      map: map,
      title: 'Your location'
    });
    this.markers.push(markerPos);
  }

  saveFleetLocation() {
    this.checkBlankField('', true);
    if (this.isFormValid) {
      const modalRef = this.modalService.open(ConfirmationModalComponent);
      modalRef.componentInstance.heading = ' Confirmation';
      modalRef.componentInstance.heading1 = 'Are you sure want to add details ?';
      modalRef.result.then(res1 => {
        if (res1 !== undefined) {
          if (res1 === 1) {
            this.formValidationArray[this.locationNumber] = true;
            this.messageService.showProgressBar(true);
            this.authService.addFleetLocation(this.addFleetLocation).subscribe((res: any) => {
              this.messageService.showProgressBar(false);
              this.messageService.sendMessageArray(['reloadLocationList']);
              this.closeForm();
            }, (error) => {
              this.messageService.showProgressBar(false);
              this.utils.toastr.showError(error.json().error.message);
              this.utils.auth.isTokenExpired(error);
            });
          }
        }
      })
    } else {
      this.checkBlankField('', true);
    }
  }

  cancelButton() {
    this.closeForm();
  }

  addressChange(event) {
    this.addFleetLocation.addressLine = event.target.value
  }

}
