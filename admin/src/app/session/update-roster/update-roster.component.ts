import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'app/services/auth.service';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { RoutingConstantService } from 'app/services/routingConstants.service';
import { MessageService } from 'app/services/message.service';
import { Utils } from 'app/utils/utils.util';
import {
  RosterDetilsRequestCommand, CreateRosterCommand, RosterRequestCommand,
  RosterAvailableUser, RosterAvailableVehicle, UpdateRosterCommand, UpdateRosterStatusCommand, AddUserSuccessResponseComamnd, RosterStatusCommand, RosterOccurrencesCommand
} from 'app/typings/Typings';
import { DEFAULT_USER_PIC } from 'app/utils/constants.util';
import { VehicleListModalComponent } from '../modals/vehicle-list-modal/vehicle-list-modal.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DriverListModalComponent } from '../modals/driver-list-modal/driver-list-modal.component';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
  selector: 'pap-update-roster',
  templateUrl: './update-roster.component.html',
  styleUrls: ['./update-roster.component.scss']
})
export class UpdateRosterComponent implements OnInit {

  rosterDetilsRequestCommand: RosterDetilsRequestCommand;
  rosterDetailCommand: CreateRosterCommand;
  occourencesArray: string[] = new Array();
  dateArray: string[] = new Array();
  timeArray: string[] = new Array();
  isWeekdaysVisible: boolean;
  selectedDaysArray: any[][] = new Array();
  summaryString: string;
  rosterRequestCommand: RosterRequestCommand;
  rosterAvailableUser: RosterAvailableUser[];
  defaultUserPic: string;
  rosterAvailableVehicle: RosterAvailableVehicle[];
  selectedUser: RosterAvailableUser;
  selectedVehicle: RosterAvailableVehicle;
  statusArray: string[] = new Array();
  showErrorDropdown: boolean;
  validateFields: {
    endDate: boolean,
    endTime: boolean,
    initialOdometerRead: boolean,
    occurrence: boolean,
    rosterStatus: boolean,
    selectedDays: boolean,
    startDate: boolean,
    startTime: boolean,
    userId: boolean,
    vehicleId: boolean
  };
  selectDriverError: boolean;
  selectVehicleError: boolean;
  responseSuccess: boolean;
  responseFail: boolean;
  responseMessage: string;
  createRosterCommand: CreateRosterCommand;
  StartDateOptions: INgxMyDpOptions;
  EndDateOptions: INgxMyDpOptions;
  startDate: any;
  endDate: any;
  startDateBkp: any;
  endDateBkp: any;
  startDateHtml: any;
  endDateHtml: any;
  startDateBkpHtml: any;
  endDateBkpHtml: any;
  updateRosterCommand: UpdateRosterCommand;
  rosterStatusCommand: RosterStatusCommand;
  rosterOccurrencesCommand: RosterOccurrencesCommand;

  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService,
    private pageTitleService: PageTitleService, private routingConstantService: RoutingConstantService,
    private messageService: MessageService, public utils: Utils, private modalService: NgbModal) {
    this.rosterDetilsRequestCommand = {
      id: null
    };
    this.rosterDetailCommand = {
      endDate: null,
      endTime: null,
      initialOdometerRead: null,
      occurrence: null,
      rosterStatus: null,
      selectedDays: null,
      startDate: null,
      startTime: null,
      userId: null,
      vehicleId: null,
      summary: null
    };
    this.createRosterCommand = {
      endDate: null,
      endTime: null,
      initialOdometerRead: null,
      occurrence: null,
      rosterStatus: null,
      selectedDays: null,
      startDate: null,
      startTime: null,
      userId: null,
      vehicleId: null,
      summary: null
    };
    this.updateRosterCommand = {
      id: null,
      endDate: null,
      endTime: null,
      initialOdometerRead: null,
      occurrence: null,
      selectedDays: null,
      startDate: null,
      startTime: null,
      userId: null,
      vehicleId: null,
      summary: null
    };
    this.occourencesArray = [];
    this.selectedDaysArray = [['Mon', false, 'Monday'], ['Tue', false, 'Tuesday'], ['Wed', false, 'Wednesday'],
    ['Thur', false, 'Thursday'], ['Fri', false, 'Friday'], ['Sat', false, 'Saturday'], ['Sun', false, 'Sunday']];
    const dateToday: Date = new Date();
    const dateTomorrow: Date = new Date();
    dateTomorrow.setDate(dateToday.getDate() + 1);
    this.dateArray.push(dateToday.getDate() + '-' + dateToday.getMonth() + 1 + '-' + dateToday.getFullYear());
    this.dateArray.push(dateTomorrow.getDate() + '-' + dateTomorrow.getMonth() + 1 + '-' + dateTomorrow.getFullYear());
    this.timeArray.push('00:00');
    this.timeArray.push('00:30');
    for (let i = 1; i < 24; i++) {
      if (i < 10) {
        this.timeArray.push('0' + i + ':00');
        this.timeArray.push('0' + i + ':30');
      } else {
        this.timeArray.push(i + ':00');
        this.timeArray.push(i + ':30');
      }
    }
    this.isWeekdaysVisible = false;
    this.summaryString = '';
    this.rosterRequestCommand = {
      endDate: null,
      endTime: null,
      occurrence: null,
      selectedDays: null,
      startDate: null,
      startTime: null
    };
    this.defaultUserPic = DEFAULT_USER_PIC;
    this.selectedUser = {
      email: null,
      empId: null,
      firstName: null,
      gender: null,
      id: null,
      image: null,
      lastName: null,
      licenseNumber: null,
      mobile: null,
      phone: null,
      userId: null
    };
    this.selectedVehicle = {
      brand: null,
      id: null,
      model: null,
      registrationNumber: null
    };
    this.statusArray = [];
    this.validateFields = {
      endDate: false,
      endTime: false,
      initialOdometerRead: false,
      occurrence: false,
      rosterStatus: false,
      selectedDays: false,
      startDate: false,
      startTime: false,
      userId: false,
      vehicleId: false
    };
    this.selectDriverError = false;
    this.selectVehicleError = false;
    this.responseFail = false;
    this.responseSuccess = false;
    this.responseMessage = '';

    this.endDate = null;
    this.startDate = null;
    this.startDateBkpHtml = this.utils.dateUtil.getHtmlDateFormat(dateToday);
    this.endDateBkpHtml = this.utils.dateUtil.getHtmlDateFormat(dateTomorrow);
  }

  ngOnInit() {
    this.pageTitleService.setTitle('Update Roster');
    this.route.queryParams.subscribe((params) => {
      if (params != null) {
        this.rosterDetilsRequestCommand.id = params['id'];
        this.getRosterDetail();
      }
    });
  }

  getRosterDetail() {
    this.messageService.showProgressBar(true);
    this.authService.getRosterDetails(this.rosterDetilsRequestCommand).subscribe((res: CreateRosterCommand) => {
      this.messageService.showProgressBar(false);
      this.setRosterDetails(res);
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      this.utils.auth.isTokenExpired(error);
    });
  }

  setRosterDetails(res) {
    if (res.status === 'success') {
      for (let key in this.rosterDetailCommand) {
        this.rosterDetailCommand[key] = res[key];
        this.createRosterCommand[key] = res[key];
      }
      this.summaryString = this.rosterDetailCommand.summary;
      if (this.createRosterCommand.occurrence === 'Weekly') {
        this.isWeekdaysVisible = true;
      }

      for (let i = 0; i < this.createRosterCommand.selectedDays.length; i++) {
        for (let j = 0; j < this.selectedDaysArray.length; j++) {
          if (this.createRosterCommand.selectedDays[i] === this.selectedDaysArray[j][2]) {
            this.selectedDaysArray[j][1] = true;
          } else {

          }
        }
      }
      this.startDateHtml = this.createRosterCommand.startDate;
      this.endDateHtml = this.createRosterCommand.endDate;
      this.rosterDetailCommand.startTime = this.utils.dateUtil.getHtmlTimeFormat(this.rosterDetailCommand.startTime);
      this.rosterDetailCommand.endTime = this.utils.dateUtil.getHtmlTimeFormat(this.rosterDetailCommand.endTime);
      this.createRosterCommand.startTime = this.utils.dateUtil.getHtmlTimeFormat(this.createRosterCommand.startTime);
      this.createRosterCommand.endTime = this.utils.dateUtil.getHtmlTimeFormat(this.createRosterCommand.endTime);
      // this.setSummaryString();
      this.getDriverList(true);
      this.getVehicleList(true);
      this.getRosterOccurrences();
      this.getRosterStatus();
    }
  }

  getRosterStatus() {
    this.messageService.showProgressBar(true);
    this.authService.getRosterStatus().subscribe((res: RosterStatusCommand) => {
      this.messageService.showProgressBar(false);
      if (res.status === 'success') {
        this.rosterStatusCommand = res;
        this.statusArray = this.rosterStatusCommand.rosterStatus;
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      this.utils.auth.isTokenExpired(error);
    });
  }

  getRosterOccurrences() {
    this.messageService.showProgressBar(true);
    this.authService.getRosterOccurrences().subscribe((res: RosterOccurrencesCommand) => {
      this.messageService.showProgressBar(false);
      if (res.status === 'success') {
        this.rosterOccurrencesCommand = res;
        this.occourencesArray = this.rosterOccurrencesCommand.rosterOccurrences;
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      this.utils.auth.isTokenExpired(error);
    });
  }

  setOccourences(occurrence) {
    this.createRosterCommand.occurrence = occurrence;
    if (occurrence === 'Today') {
      this.createRosterCommand.startDate = this.startDateBkpHtml;
      this.createRosterCommand.endDate = this.startDateBkpHtml;
      this.startDateHtml = this.startDateBkpHtml;
      this.endDateHtml = this.startDateBkpHtml;
      this.isWeekdaysVisible = false;
      this.validateFields.selectedDays = false;
      this.selectAllDays(false);
      this.checkBlankField('startDate');
      this.checkBlankField('endDate');
    } else if (occurrence === 'Tomorrow') {
      this.createRosterCommand.startDate = this.endDateBkpHtml;
      this.createRosterCommand.endDate = this.endDateBkpHtml;
      this.endDateHtml = this.endDateBkpHtml;
      this.startDateHtml = this.endDateBkpHtml;
      this.isWeekdaysVisible = false;
      this.validateFields.selectedDays = false;
      this.selectAllDays(false);
      this.checkBlankField('startDate');
      this.checkBlankField('endDate');
    } else if (occurrence === 'Weekly') {
      this.isWeekdaysVisible = true;
      this.createRosterCommand.startDate = this.startDateHtml;
      this.createRosterCommand.endDate = this.endDateHtml;
      this.validateFields.endDate = false;
      if (this.createRosterCommand.occurrence && this.createRosterCommand.occurrence !== 'Weekly') {
        this.selectAllDays(false, true);
      }
      this.checkBlankField('startDate');
    } else if (occurrence === 'Custom') {
      this.isWeekdaysVisible = false;
      this.createRosterCommand.startDate = this.startDateHtml
      this.createRosterCommand.endDate = this.endDateHtml;
      this.validateFields.endDate = false;
      this.selectAllDays(false);
      this.checkBlankField('startDate');
    }
    this.checkBlankField('occurrence');
    this.selectDriverError = false;
    this.selectVehicleError = false;
    this.setSummaryString();
  }

  parseIDate(date): string {
    if (date) {
      return date.date.day + '-' + date.date.month + '-' + date.date.year
    }
  }

  setStartDate(date) {
    this.rosterDetailCommand.startDate = date;
    this.checkBlankField('startDate');
    this.setSummaryString();
  }

  setEndDate(date) {
    this.rosterDetailCommand.endDate = date;
    this.checkBlankField('endDate');
    this.setSummaryString();
  }

  setStartTime() {
    this.checkBlankField('startTime');
    this.setSummaryString();
  }

  setEndTime() {
    this.checkBlankField('endTime');
    this.setSummaryString();
  }

  selectAllDays(status: boolean, setDefault?: boolean) {
    for (let i = 0; i < this.selectedDaysArray.length; i++) {
      this.selectedDaysArray[i][1] = status;
    }
    if (setDefault) {
      for (let i = 0; i < this.selectedDaysArray.length; i++) {
        if (i < 5) {
          this.selectedDaysArray[i][1] = true;
        } else {
          this.selectedDaysArray[i][1] = false;
        }
      }
    }
  }

  daysClickHandler(event, index) {
    if (event.target.checked === true) {
      this.validateFields.selectedDays = false;
    }
    if (this.rosterDetailCommand.occurrence === 'Weekly') {
      this.selectedDaysArray[index][1] = true;
    } else {
      this.selectedDaysArray[index][1] = event.target.checked;
      this.setSummaryString();
    }

  }

  setSummaryString() {
    this.summaryString = '';
    if (this.createRosterCommand.occurrence === 'Today') {
      this.summaryString = 'Today ' + this.createRosterCommand.startDate + ' from ' +
        this.createRosterCommand.startTime + ' to ' + this.createRosterCommand.endTime;
    } else if (this.createRosterCommand.occurrence === 'Tomorrow') {
      this.summaryString = 'Tomorrow  ' + this.createRosterCommand.startDate + ' from ' +
        this.createRosterCommand.startTime + ' to ' + this.createRosterCommand.endTime;
    } else if (this.createRosterCommand.occurrence === 'Weekly') {
      let sDate = '';
      if (this.createRosterCommand.startDate) {
        sDate = this.createRosterCommand.startDate;
      }
      let eDate = '';
      if (this.createRosterCommand.endDate) {
        eDate = this.createRosterCommand.endDate;
      }
      this.summaryString = 'Weekly from ' + sDate + ' ' + this.createRosterCommand.startTime
        + ' to ' + eDate + ' ' + this.createRosterCommand.endTime;
    } else if (this.createRosterCommand.occurrence === 'Custom') {
      let sDate = '';
      if (this.createRosterCommand.startDate) {
        sDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.startDate);
      }
      let eDate = '';
      if (this.createRosterCommand.endDate) {
        eDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.endDate);
      }
      this.summaryString = this.summaryString.substr(0, this.summaryString.length - 2) +
        'From ' + sDate + ' ' + this.createRosterCommand.startTime
        + ' to ' + eDate + ' ' + this.createRosterCommand.endTime;

    }
  }

  getDriverList(onLoad?: boolean) {
    this.rosterRequestCommand.endDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.endDate);
    this.rosterRequestCommand.endTime = this.rosterDetailCommand.endTime;
    this.rosterRequestCommand.occurrence = this.rosterDetailCommand.occurrence;
    this.rosterRequestCommand.selectedDays = this.rosterDetailCommand.selectedDays;
    this.rosterRequestCommand.startDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.startDate);
    this.rosterRequestCommand.startTime = this.rosterDetailCommand.startTime;
    let isCommandValid = true;
    for (const key in this.rosterRequestCommand) {
      if (this.rosterRequestCommand[key] === null || this.rosterRequestCommand[key] === '') {
        isCommandValid = false;
      }
    }
    if (isCommandValid) {
      this.selectDriverError = false;
      this.messageService.showProgressBar(true);
      this.authService.getAvailableUsers(this.rosterRequestCommand).subscribe((users: RosterAvailableUser[]) => {
        this.messageService.showProgressBar(false);
        if (users.length > 0) {
          this.rosterAvailableUser = users;
          if (onLoad === true) {
            for (let i = 0; i < this.rosterAvailableUser.length; i++) {
              if (this.rosterAvailableUser[i].userId === this.rosterDetailCommand.userId) {
                this.selectedUser = this.rosterAvailableUser[i];
              }
            }
          } else {
            const modalRef = this.modalService.open(DriverListModalComponent);
            modalRef.componentInstance.rosterAvailableUser = this.rosterAvailableUser;
            modalRef.componentInstance.selectedUser = this.selectedUser;
            modalRef.result.then(res => {
              this.selectedUser = res;
              this.setUserId();
            });
          }

        } else {

        }
      }, (error) => {
        this.messageService.showProgressBar(false);
      });
    } else {
      this.selectDriverError = true;
    }
  }

  getUserImage(image) {
    if (image === null) {
      return this.defaultUserPic;
    } else {
      return image;
    }
  }

  getVehicleList(onLoad?: boolean) {
    this.rosterRequestCommand.endDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.endDate);
    this.rosterRequestCommand.endTime = this.rosterDetailCommand.endTime;
    this.rosterRequestCommand.occurrence = this.rosterDetailCommand.occurrence;
    this.rosterRequestCommand.selectedDays = this.rosterDetailCommand.selectedDays;
    this.rosterRequestCommand.startDate = this.utils.dateUtil.dateMMDDYYYY(this.createRosterCommand.startDate);
    this.rosterRequestCommand.startTime = this.rosterDetailCommand.startTime;
    this.messageService.showProgressBar(true);
    this.authService.getAvailableVehicles(this.rosterRequestCommand).subscribe((users: RosterAvailableVehicle[]) => {
      this.messageService.showProgressBar(false);
      if (users.length > 0) {
        this.rosterAvailableVehicle = users;
        if (onLoad === true) {
          for (let i = 0; i < this.rosterAvailableVehicle.length; i++) {
            if (this.rosterAvailableVehicle[i].id === this.rosterDetailCommand.vehicleId) {
              this.selectedVehicle = this.rosterAvailableVehicle[i];
            }
          }
        } else {
          const modalRef = this.modalService.open(VehicleListModalComponent);
          modalRef.componentInstance.rosterAvailableVehicle = this.rosterAvailableVehicle;
          modalRef.componentInstance.selectedVehicle = this.selectedVehicle;
          modalRef.result.then(res => {
            this.selectedVehicle = res;
            this.setVehicleId();
          });
        }

      } else {

      }
    }, (error) => {
      this.messageService.showProgressBar(false);
    });
  }

  setVehicleId() {
    if (this.selectedVehicle.id !== null) {
      this.rosterDetailCommand.vehicleId = this.selectedVehicle.id;
    } else {

    }
    this.checkBlankField('vehicleId');
  }

  setUserId() {
    if (this.selectedUser.id !== null) {
      this.rosterDetailCommand.userId = this.selectedUser.userId;
    } else {

    }
    this.checkBlankField('userId');
  }

  setStatus(status) {
    this.rosterDetailCommand.rosterStatus = status;
    this.checkBlankField('rosterStatus');
  }

  occurrenceCheck() {
    this.checkBlankField('occurrence');
  }

  checkBlankField(field: string, checkAllFields?: boolean) {
    if (checkAllFields) {

    } else {
      checkAllFields = false;
    }
    if (field === 'occurrence' || checkAllFields) {
      if (this.createRosterCommand.occurrence === null) {
        this.showErrorDropdown = true;
        this.validateFields.occurrence = true;
      } else {
        this.validateFields.occurrence = false;
      }
    }
    if (field === 'selectedDays' || checkAllFields) {
      let customDaysSelectedCount = 0;
      for (let i = 0; i < this.selectedDaysArray.length; i++) {
        if (this.createRosterCommand.occurrence === 'Custom') {
          if (!this.selectedDaysArray[i][1]) {
            customDaysSelectedCount++;
            if (customDaysSelectedCount === this.selectedDaysArray.length) {
              this.showErrorDropdown = true;
              this.validateFields.selectedDays = true;
              this.isWeekdaysVisible = true;
            }
          }
        }
      }
    }
    if (field === 'startDate' || checkAllFields) {
      if (this.createRosterCommand.startDate === null || this.createRosterCommand.startDate === undefined) {
        this.showErrorDropdown = true;
        this.validateFields.startDate = true;
      } else {
        this.validateFields.startDate = false;
      }
    }
    if (field === 'startTime' || checkAllFields) {
      if (this.createRosterCommand.startTime === null) {
        this.showErrorDropdown = true;
        this.validateFields.startTime = true;
      } else {
        this.validateFields.startTime = false;
      }
    }
    if (field === 'endDate' || checkAllFields) {
      if (this.createRosterCommand.occurrence === 'Today' ||
        this.createRosterCommand.occurrence === 'Tomorrow' || this.createRosterCommand.occurrence === null) {
        if (this.createRosterCommand.endDate === null || this.createRosterCommand.endDate === undefined) {
          this.showErrorDropdown = true;
          this.validateFields.endDate = true;
        } else {
          this.validateFields.endDate = false;
        }
      } else {
        this.validateFields.endDate = false;
      }
    }
    if (field === 'endTime' || checkAllFields) {
      if (this.createRosterCommand.endTime === null) {
        this.showErrorDropdown = true;
        this.validateFields.endTime = true;
      } else {
        this.validateFields.endTime = false;
      }
    }
    if (field === 'userId' || checkAllFields) {
      if (this.createRosterCommand.userId === null) {
        this.showErrorDropdown = true;
        this.validateFields.userId = true;
      } else {
        this.validateFields.userId = false;
      }
    }
    if (field === 'vehicleId' || checkAllFields) {
      if (this.createRosterCommand.vehicleId === null) {
        this.showErrorDropdown = true;
        this.validateFields.vehicleId = true;
      } else {
        this.validateFields.vehicleId = false;
      }
    }
    if (field === 'rosterStatus' || checkAllFields) {
      if (this.createRosterCommand.rosterStatus === null) {
        this.showErrorDropdown = true;
        this.validateFields.rosterStatus = true;
      } else {
        this.validateFields.rosterStatus = false;
      }
    }
  }

  onDateChanged(source): void {
    if (source === 0) {
      this.createRosterCommand.startDate = this.startDateHtml;
      this.checkBlankField('startDate');
    } else if (source === 1) {
      this.createRosterCommand.endDate = this.endDateHtml;
      this.checkBlankField('endDate');
    }
    this.setSummaryString();
  }

  updateRoster() {
    this.createRosterCommand.summary = this.summaryString;
    this.createRosterCommand.selectedDays = [];
    if (this.createRosterCommand.occurrence === 'Weekly') {
      for (let i = 0; i < this.selectedDaysArray.length; i++) {
        if (this.selectedDaysArray[i][1]) {
          this.createRosterCommand.selectedDays.push(this.selectedDaysArray[i][2]);
        }
      }
    }
    this.checkBlankField('', true);
    let isFormValid: boolean = true;
    for (const key in this.validateFields) {
      if (this.validateFields[key] === false) {
        isFormValid = true;
      }
    }
    if (isFormValid) {
      let command: UpdateRosterCommand = new UpdateRosterCommand();
      for (let temp in this.updateRosterCommand) {
        command[temp] = this.createRosterCommand[temp];
      }
      command.id = this.rosterDetilsRequestCommand.id;
      command.startDate = this.utils.dateUtil.dateMMDDYYYY(command.startDate);
      command.endDate = this.utils.dateUtil.dateMMDDYYYY(command.endDate);
      this.messageService.showProgressBar(true);
      this.authService.updateRoster(command).subscribe((res: any) => {
        this.messageService.showProgressBar(false);
        if (res.status === 'success') {
          let updateRosterStatusCommand: UpdateRosterStatusCommand = new UpdateRosterStatusCommand();
          updateRosterStatusCommand.id = this.rosterDetilsRequestCommand.id;
          updateRosterStatusCommand.rosterStatus = this.createRosterCommand.rosterStatus;
          this.authService.updateRosterStatus(updateRosterStatusCommand).subscribe((res: AddUserSuccessResponseComamnd) => {
            if (res.status === 'success') {
              this.utils.toastr.showSuccess('Roster updated successfully');
              setTimeout(() => {
                this.router.navigateByUrl(this.routingConstantService.getListRosterRouterLink());
              }, 2000);
            }
          });
        } else {
          this.utils.toastr.showError('');
        }
      }, (error) => {
        try {
          this.messageService.showProgressBar(false);
          this.utils.toastr.showError(error.json().error.message);
          this.utils.auth.isTokenExpired(error);
        } catch (error) {
          this.utils.toastr.showError(error.statusText);
        }
      });
    }
  }

  cancelUpdate() {
    this.router.navigateByUrl(this.routingConstantService.getListRosterRouterLink());
  }

}
