import { Component, OnInit } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { RosterListCommand, RosterListRequestCommand, RosterStatusCommand } from 'app/typings/Typings';
import { AuthService } from 'app/services/auth.service';
import { Router } from '@angular/router';
import { RoutingConstantService } from 'app/services/routingConstants.service';
import { MessageService } from 'app/services/message.service';
import { Utils } from 'app/utils/utils.util';
// import { DataTablesModule } from 'angular-datatables';
@Component({
  selector: 'pap-list-roster',
  templateUrl: './list-roster.component.html',
  styleUrls: ['./list-roster.component.scss']
})
export class ListRosterComponent implements OnInit {
  title = 'Roster List';
  rosterFilterInput: any;
  rosterList: RosterListCommand[];
  rosterListTemp: RosterListCommand[];
  rosterListRequestCommand: RosterListRequestCommand;
  responseFail: boolean;
  responseSuccess: boolean;
  responseMessage: string;
  visible: boolean;
  visible1: boolean;
  visible2: boolean;
  count: number;
  count1: number;
  count2: number;
  occourencesArray: string[];
  selectedDaysArray: string[];
  dateFuture: any;
  datePast: any;
  dateToday: any;
  rosterStatusCommand: RosterStatusCommand;
  statusArray: string[];

  constructor(private pageTitleService: PageTitleService, private authService: AuthService,
    private router: Router, private routingConstantService: RoutingConstantService,
    private messageService: MessageService, public utils: Utils) {
    this.rosterListRequestCommand = {
      endDate: null,
      endTime: null,
      occurrence: null,
      selectedDays: null,
      startDate: null,
      startTime: null
    };
    this.responseFail = false;
    this.responseSuccess = false;
    this.responseMessage = '';
    this.count = 0;
    this.count1 = 0;
    this.count2 = 0;
    this.visible = false;
    this.visible1 = false;
    this.visible2 = false;
    this.occourencesArray = [];
    this.selectedDaysArray = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    this.dateToday = new Date();
    this.dateFuture = new Date();
    this.datePast = new Date();
    this.dateFuture.setDate(this.dateToday.getDate() + 180);
    this.datePast.setDate(this.dateToday.getDate() - 180);
    this.dateFuture = this.utils.dateUtil.getHtmlDateFormat(this.dateFuture);
    this.dateFuture = this.utils.dateUtil.dateMMDDYYYY(this.dateFuture);
    this.datePast = this.utils.dateUtil.getHtmlDateFormat(this.datePast);
    this.datePast = this.utils.dateUtil.dateMMDDYYYY(this.datePast);
    this.statusArray = [];
  }

  ngOnInit(): void {
    this.pageTitleService.setTitle('Roster List');
    let defaultCommand: RosterListRequestCommand = new RosterListRequestCommand();
    defaultCommand = {
      endDate: this.dateFuture,
      endTime: '23:59',
      occurrence: null,
      selectedDays: [],
      startDate: this.datePast,
      startTime: '00:00',
    };
    this.getRosterList(defaultCommand);
    this.getRosterStatus();
  }

  dateChange() {
  }

  getRosterStatus() {
    this.messageService.showProgressBar(true);
    this.authService.getRosterStatus().subscribe((res: RosterStatusCommand) => {
      this.messageService.showProgressBar(false);
      if (res.status === 'success') {
        this.rosterStatusCommand = res;
        this.statusArray = this.rosterStatusCommand.rosterStatus;
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      this.utils.auth.isTokenExpired(error);
    });
  }

  getRosterList(command: RosterListRequestCommand) {
    this.messageService.showProgressBar(true);
    this.authService.getRosterList(command).subscribe((res: RosterListCommand[]) => {
      this.messageService.showProgressBar(false);
      this.rosterList = res;
      this.rosterListTemp = res;
      if (this.rosterList.length < 0) {
        this.responseFail = true;
        this.responseMessage = 'No Records Found';
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.responseFail = true;
      this.responseMessage = error.json().error.message;
      this.utils.auth.isTokenExpired(error);
    })
  }

  updateRoster(rosterId) {
    this.router.navigate([this.routingConstantService.getUpdateRosterRouterLink()],
      { queryParams: { id: rosterId } });
  }

  filterRosterList() {
    let command: RosterListRequestCommand = new RosterListRequestCommand();
    command.endDate = this.utils.dateUtil.dateMMDDYYYY(this.rosterListRequestCommand.endDate);
    command.startDate = this.utils.dateUtil.dateMMDDYYYY(this.rosterListRequestCommand.startDate);
    command.endTime = this.rosterListRequestCommand.endTime;
    command.startTime = this.rosterListRequestCommand.startTime;
    command.selectedDays = this.rosterListRequestCommand.selectedDays;
    command.occurrence = this.rosterListRequestCommand.occurrence;
    this.getRosterList(command);
  }

  searchFilter() {
    const val = this.rosterFilterInput;
    const temp = this.rosterListTemp.filter(function (d) {
      return (d.userName.toLowerCase().indexOf(val) !== -1 || d.rosterStatus.indexOf(val) !== -1 ||
        d.userId.toLowerCase().indexOf(val) !== -1 || d.vehicleRegistration.indexOf(val) !== -1 || !val);
    });
    this.rosterList = temp;
  }
  searchFilter1(event) {
    const val = event.target.value;
    if (val !== '') {
      const temp = this.rosterListTemp.filter(function (d) {
        return (d.rosterStatus.startsWith(val));
      });
      this.rosterList = temp;
    } else {
      this.rosterList = this.rosterListTemp;
    }

  }

  filterAscOfStatus(element1, element2): number {
    try {
      if (element1.rosterStatus > element2.rosterStatus) {
        return -1;
      }
      if (element1.rosterStatus < element2.rosterStatus) {
        return 1;
      }
      if (element1.rosterStatus === element2.rosterStatus) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }

  filterAscOfUserId(element1, element2): number {
    try {
      if (element1.userId > element2.userId) {
        return -1;
      }
      if (element1.userId < element2.userId) {
        return 1;
      }
      if (element1.userId === element2.userId) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }

  filterAscOfDriverName(element1, element2): number {
    try {
      if (element1.userName > element2.userName) {
        return -1;
      }
      if (element1.userName < element2.userName) {
        return 1;
      }
      if (element1.userName === element2.userName) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }

  filterAscOfVehicleNumber(element1, element2): number {
    try {
      if (element1.vehicleRegistration > element2.vehicleRegistration) {
        return -1;
      }
      if (element1.vehicleRegistration < element2.vehicleRegistration) {
        return 1;
      }
      if (element1.vehicleRegistration === element2.vehicleRegistration) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }

  filterDescOfStatus(element1, element2): number {
    try {
      if (element1.rosterStatus > element2.rosterStatus) {
        return 1;
      }
      if (element1.rosterStatus < element2.rosterStatus) {
        return -1;
      }
      if (element1.rosterStatus === element2.rosterStatus) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }

  filterDescOfUserId(element1, element2): number {
    try {
      if (element1.userId > element2.userId) {
        return 1;
      }
      if (element1.userId < element2.userId) {
        return -1;
      }
      if (element1.userId === element2.userId) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }

  filterDescOfDriverName(element1, element2): number {
    try {
      if (element1.userName > element2.userName) {
        return 1;
      }
      if (element1.userName < element2.userName) {
        return -1;
      }
      if (element1.userName === element2.userName) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }

  filterDescOfVehicleNumber(element1, element2): number {
    try {
      if (element1.vehicleRegistration > element2.vehicleRegistration) {
        return 1;
      }
      if (element1.vehicleRegistration < element2.vehicleRegistration) {
        return -1;
      }
      if (element1.vehicleRegistration === element2.vehicleRegistration) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }

  sortStatus(status: boolean) {
    if (status) {
      this.rosterList.sort(this.filterAscOfStatus);
    } else {
      this.rosterList.sort(this.filterDescOfStatus);
    }
  }
  sortUserID(status: boolean) {
    if (status) {
      this.rosterList.sort(this.filterAscOfUserId);
    } else {
      this.rosterList.sort(this.filterDescOfUserId);
    }
  }

  sortDriverName(status: boolean) {
    if (status) {
      this.rosterList.sort(this.filterAscOfDriverName);
    } else {
      this.rosterList.sort(this.filterDescOfDriverName);
    }
  }

  sortVehicleNumber(status: boolean) {
    if (status) {
      this.rosterList.sort(this.filterAscOfVehicleNumber);
    } else {
      this.rosterList.sort(this.filterDescOfVehicleNumber);
    }
  }

  toggle() {
    this.visible = !this.visible;
  }
  toggleStatus() {
    this.visible1 = !this.visible1;
  }
  toggleDriverName() {
    this.visible2 = !this.visible2;
  }

  toggleVehicleNumber() {
    this.visible = !this.visible;
  }


}
