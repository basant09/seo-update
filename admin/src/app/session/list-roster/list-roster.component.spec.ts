import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRosterComponent } from './list-roster.component';

describe('ListRosterComponent', () => {
  let component: ListRosterComponent;
  let fixture: ComponentFixture<ListRosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
