import { Component, OnInit } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import {
  AddVehicleCommand, VehicleTypeCommand, VehicleBrandsCommand, FuelTypesCommand, DefaultDistanceUnitCommand,
  DefaultFuleUnitCommand, AddVehicleResponseCommand, VehicleModalRequestCommand, VehicleModalCommand
} from 'app/typings/Typings';
import { Utils } from '../../utils/utils.util';
import { MessageService } from '../../services/message.service';
import { RoutingConstantService } from '../../services/routingConstants.service';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfirmationModalComponent } from '../../ui-elements/confirmation-modal/confirmation-modal.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'pap-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.scss']
})
export class AddVehicleComponent implements OnInit {
  addVehicleCommand: AddVehicleCommand;
  responseSuccess: boolean = false;
  showError: boolean = false;
  responseFail: boolean = false;
  responseMessage: string;
  vehicleTypeArray: string[] = new Array();
  vehicleBrandArray: string[] = new Array();
  yearArray: number[] = new Array();
  fuelTypeArray: string[] = new Array();
  distanceUnitArray: string[] = new Array();
  fuelUnitArray: string[] = new Array();
  statusArray: string[] = new Array();
  modalArray: string[] = new Array();
  vehicleModalRequestCommand: VehicleModalRequestCommand;
  brandError: boolean;
  modelError: boolean;
  typeError: boolean;
  registrationNoError: boolean;

  constructor(private pageTitleService: PageTitleService, public utils: Utils, private messageService: MessageService,
    private routingConstantService: RoutingConstantService, private authService: AuthService, private modalService: NgbModal,
    private router: Router) {
    this.statusArray = ['Active', 'Inactive'];
    this.addVehicleCommand = {
      brand: null,
      colour: null,
      distanceUnit: null,
      fuelType: null,
      fuelUnit: null,
      initialOdometerRead: null,
      model: null,
      registrationNumber: null,
      type: null,
      vehicleStatus: this.statusArray[0],
      year: null

    }
    this.vehicleTypeArray = [];
    this.vehicleBrandArray = [];
    this.yearArray = [];
    this.fuelTypeArray = [];
    this.modalArray = [];
    this.vehicleModalRequestCommand = {
      brand: null
    }
  }

  ngOnInit() {
    this.pageTitleService.setTitle('Add Vehicle');
    this.loadDetails();
  }

  loadDetails() {
    this.getVehicleTypes();
    this.getVehicleBrands();
    this.getYearList();
    this.getFuelTypes();
    this.getDefaultDistanceUnit();
    this.getDefaultFuelUnit();
  }

  getVehicleTypes() {
    this.messageService.showProgressBar(true);
    this.authService.getVehicleTypes().subscribe((type: VehicleTypeCommand) => {
      this.messageService.showProgressBar(false);
      if (type.status === 'success') {
        this.vehicleTypeArray = type.vehicleTypes;
        this.vehicleTypeArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getVehicleBrands() {
    this.messageService.showProgressBar(true);
    this.authService.getVehicleBrands().subscribe((type: VehicleBrandsCommand) => {
      this.messageService.showProgressBar(false);
      if (type.status === 'success') {
        this.vehicleBrandArray = type.vehicleBrands;
        this.vehicleBrandArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getYearList() {
    const todayDate: Date = new Date();
    const currentYear = todayDate.getFullYear();
    for (let i = 0; i < 30; i++) {
      this.yearArray[i] = currentYear - i;
    }
  }

  getFuelTypes() {
    this.messageService.showProgressBar(true);
    this.authService.getFuelTypes().subscribe((type: FuelTypesCommand) => {
      this.messageService.showProgressBar(false);
      if (type.status === 'success') {
        this.fuelTypeArray = type.fuelTypes;
        this.fuelTypeArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getDefaultDistanceUnit() {
    this.messageService.showProgressBar(true);
    this.authService.getDefaultDistanceUnit().subscribe((type: DefaultDistanceUnitCommand) => {
      this.messageService.showProgressBar(false);
      if (type.status === 'success') {
        this.distanceUnitArray = type.distanceUnits;
        this.distanceUnitArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getDefaultFuelUnit() {
    this.messageService.showProgressBar(true);
    this.authService.getDefaultFuelUnit().subscribe((type: DefaultFuleUnitCommand) => {
      this.messageService.showProgressBar(false);
      if (type.status === 'success') {
        this.fuelUnitArray = type.fuelUnits;
        this.fuelUnitArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  removeError() {
    this.responseSuccess = false;
    this.responseFail = false;
    this.registrationNoError = false;
  }

  addVehicle(addVehicleForm) {

    this.checkBlankField('', true);

    if (addVehicleForm.valid && !this.brandError && !this.modelError && !this.registrationNoError && !this.typeError) {
      const modalRef = this.modalService.open(ConfirmationModalComponent);
      modalRef.componentInstance.heading = ' Confirmation';
      modalRef.componentInstance.heading1 = 'Are you sure want to add details ?';
      modalRef.result.then(res1 => {
        if (res1 !== undefined) {

           if (res1 === 1) {

            this.messageService.showProgressBar(true);
      this.authService.addVehicle(this.addVehicleCommand).subscribe((res: AddVehicleResponseCommand) => {
        this.messageService.showProgressBar(false);
        if (res.status === 'success') {
          this.utils.toastr.showSuccess('Vehicle Added Successfully');
          setTimeout(() => {
            this.router.navigateByUrl(this.routingConstantService.getListVehicleRouterLink());
          }, 2000);
        } else {
          this.utils.toastr.showError('Something went wrong, Please try again later');
        }

      }, (error) => {
        this.messageService.showProgressBar(false);
        this.utils.toastr.showError(error.json().error.message);
        if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
          setTimeout(() => {
            this.authService.logout();
          }, 2000);
        } else {
          this.showError = true;
        }
      });
    }
        }
      });
   }
  }

  getModal() {
    this.vehicleModalRequestCommand.brand = this.addVehicleCommand.brand;
    if (this.vehicleModalRequestCommand.brand) {
      this.messageService.showProgressBar(true);
      this.authService.getVehicleModels(this.vehicleModalRequestCommand).subscribe((type: VehicleModalCommand) => {
        this.messageService.showProgressBar(false);
        if (type.status === 'success') {
          this.modalArray = type.vehicleModels;
          this.modalArray.sort(this.utils.sort.sortAsc);
        }
      }, (error) => {
        this.messageService.showProgressBar(false);
        this.utils.toastr.showError(error.json().error.message);
        if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
          setTimeout(() => {
            this.authService.logout();
          }, 2000);
        }
      });
    }
  }

  setVehicleType(type: string) {
    this.addVehicleCommand.type = type;
    this.checkBlankField('type');
  }

  setVehicleBrand(brand: string) {
    this.addVehicleCommand.brand = brand;
    this.checkBlankField('brand');
    this.getModal();
  }

  setVehicleModel(model: string) {
    this.addVehicleCommand.model = model;
    this.checkBlankField('model');
  }

  setVehicleYear(year: string) {
    this.addVehicleCommand.year = year;
  }

  setFuelType(fuelType: string) {
    this.addVehicleCommand.fuelType = fuelType;
  }

  setFuelUnit(fuelUnit: string) {
    this.addVehicleCommand.fuelUnit = fuelUnit;
  }

  setDistanceUnit(distanceUnit: string) {
    this.addVehicleCommand.distanceUnit = distanceUnit;
  }

  setStatus(status: string) {
    this.addVehicleCommand.vehicleStatus = status;
  }

  checkBlankField(field: string, checkAllFields?: boolean) {
    if (checkAllFields) {

    } else {
      checkAllFields = false;
    }
    if (field === 'type' || checkAllFields) {
      if (this.addVehicleCommand.type === null) {
        this.showError = true;
        this.typeError = true;
      } else {
        this.typeError = false;
      }
    }
    if (field === 'brand' || checkAllFields) {
      if (this.addVehicleCommand.brand === null) {
        this.showError = true;
        this.brandError = true;
      } else {
        this.brandError = false;
      }
    }
    if (field === 'model' || checkAllFields) {
      if (this.addVehicleCommand.model === null) {
        this.showError = true;
        this.modelError = true;
      } else {
        this.modelError = false;
      }
    }
    if (field === 'registrationNo' || checkAllFields) {
      if (this.addVehicleCommand.registrationNumber === null) {
        this.showError = true;
        this.registrationNoError = true;
      } else {
        this.registrationNoError = false;
      }
    }
  }

}
