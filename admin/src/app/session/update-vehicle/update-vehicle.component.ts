import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import {
  UpdateVehicleCommand, VehicleTypeCommand, VehicleBrandsCommand, FuelTypesCommand,
  DefaultDistanceUnitCommand, DefaultFuleUnitCommand, VehicleModalCommand, VehicleModalRequestCommand,
  GetVehicleDetailsCommand, AddVehicleCommand
} from 'app/typings/Typings';
import { MessageService } from 'app/services/message.service';
import { AuthService } from 'app/services/auth.service';
import { RoutingConstantService } from 'app/services/routingConstants.service';
import { Utils } from '../../utils/utils.util';
import { ConfirmationModalComponent } from '../../ui-elements/confirmation-modal/confirmation-modal.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'pap-update-vehicle',
  templateUrl: './update-vehicle.component.html',
  styleUrls: ['./update-vehicle.component.scss']
})
export class UpdateVehicleComponent implements OnInit {
 value: number;
  updateVehicleCommand: UpdateVehicleCommand;
  responseSuccess: boolean = false;
  showError: boolean = false;
  responseFail: boolean = false;
  responseMessage: string;
  vehicleTypeArray: string[] = new Array();
  vehicleBrandArray: string[] = new Array();
  yearArray: number[] = new Array();
  fuelTypeArray: string[] = new Array();
  distanceUnitArray: string[] = new Array();
  fuelUnitArray: string[] = new Array();
  statusArray: string[] = new Array();
  modalArray: string[] = new Array();
  vehicleModalRequestCommand: VehicleModalRequestCommand;
  getVehicleDetailsCommand: GetVehicleDetailsCommand;
  brandError: boolean;
  modelError: boolean;
  registrationNoError: boolean;
  typeError: boolean;

  constructor(private pageTitleService: PageTitleService, private route: ActivatedRoute, private messageService: MessageService,
    private authService: AuthService, private router: Router, private routingConstantService: RoutingConstantService, 
    private modalService: NgbModal,
    public utils: Utils) {
      this.value = 0;
    this.statusArray = ['Active', 'Inactive']
    this.updateVehicleCommand = {
      brand: null,
      colour: null,
      distanceUnit: null,
      fuelType: null,
      fuelUnit: null,
      id: null,
      initialOdometerRead: null,
      model: null,
      registrationNumber: null,
      type: null,
      vehicleStatus: null,
      year: null,
    };
    this.vehicleModalRequestCommand = {
      brand: null
    };
    this.getVehicleDetailsCommand = {
      id: null
    };
    this.registrationNoError = false;
    this.modelError = false;
    this.brandError = false;
  }

  ngOnInit() {
    this.pageTitleService.setTitle('Update Vehicle');
    this.route.queryParams.subscribe((params) => {
      if (params != null) {
        this.updateVehicleCommand.id = params['id'];
        this.getVehicleDetailsCommand.id = params['id'];
        this.getVehicleDetail();
        this.loadDetails();
      }
    });
  }

  getVehicleDetail() {
    // this.loadDetails();
    this.messageService.showProgressBar(true);
    this.authService.getVehicleDetails(this.getVehicleDetailsCommand).subscribe((detail: AddVehicleCommand) => {
      this.messageService.showProgressBar(false);
      this.updateVehicleCommand.brand = detail.brand;
      this.updateVehicleCommand.colour = detail.colour;
      this.updateVehicleCommand.distanceUnit = detail.distanceUnit;
      this.updateVehicleCommand.fuelType = detail.fuelType;
      this.updateVehicleCommand.fuelUnit = detail.fuelUnit;
      this.updateVehicleCommand.initialOdometerRead = detail.initialOdometerRead;
      this.updateVehicleCommand.model = detail.model;
      this.updateVehicleCommand.registrationNumber = detail.registrationNumber;
      this.updateVehicleCommand.type = detail.type;
      this.updateVehicleCommand.vehicleStatus = detail.vehicleStatus;
      this.updateVehicleCommand.year = detail.year;
      this.getModal();
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  loadDetails() {
    this.getVehicleTypes();
    this.getVehicleBrands();
    this.getYearList();
    this.getFuelTypes();
    this.getDefaultDistanceUnit();
    this.getDefaultFuelUnit();
  }

  getVehicleTypes() {
    this.messageService.showProgressBar(true);
    this.authService.getVehicleTypes().subscribe((type: VehicleTypeCommand) => {
      this.messageService.showProgressBar(false);
      if (type.status === 'success') {
        this.vehicleTypeArray = type.vehicleTypes;
        this.vehicleTypeArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getVehicleBrands() {
    this.messageService.showProgressBar(true);
    this.authService.getVehicleBrands().subscribe((type: VehicleBrandsCommand) => {
      this.messageService.showProgressBar(false);
      if (type.status === 'success') {
        this.vehicleBrandArray = type.vehicleBrands;
        this.vehicleBrandArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getYearList() {
    const todayDate: Date = new Date();
    const currentYear = todayDate.getFullYear();
    for (let i = 0; i < 30; i++) {
      this.yearArray[i] = currentYear - i;
    }
  }

  getFuelTypes() {
    this.messageService.showProgressBar(true);
    this.authService.getFuelTypes().subscribe((type: FuelTypesCommand) => {
      this.messageService.showProgressBar(false);
      if (type.status === 'success') {
        this.fuelTypeArray = type.fuelTypes;
        this.fuelTypeArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getDefaultDistanceUnit() {
    this.messageService.showProgressBar(true);
    this.authService.getDefaultDistanceUnit().subscribe((type: DefaultDistanceUnitCommand) => {
      this.messageService.showProgressBar(false);
      if (type.status === 'success') {
        this.distanceUnitArray = type.distanceUnits;
        this.distanceUnitArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  getDefaultFuelUnit() {
    this.messageService.showProgressBar(true);
    this.authService.getDefaultFuelUnit().subscribe((type: DefaultFuleUnitCommand) => {
      this.messageService.showProgressBar(false);
      if (type.status === 'success') {
        this.fuelUnitArray = type.fuelUnits;
        this.fuelUnitArray.sort(this.utils.sort.sortAsc);
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  removeError() {
    this.responseSuccess = false;
    this.responseFail = false;
    // this.showError = false;
  }

  updateVehicle(updateVehicleForm) {
    if (updateVehicleForm.valid) {
      const modalRef = this.modalService.open(ConfirmationModalComponent);
      modalRef.componentInstance.heading = ' Vehicle Confirmation';
      modalRef.componentInstance.heading1 = 'Are you sure want to update details ?';
      modalRef.result.then(res => {
        if (res !== undefined) {
          this.value = res;
          if (this.value === 1) {

      this.messageService.showProgressBar(true);
      this.authService.updateVehicle(this.updateVehicleCommand).subscribe((res: any) => {
        this.messageService.showProgressBar(false);
        if (res.status === 'success') {
          this.utils.toastr.showSuccess('Vehicle Updated Successfully');
          setTimeout(() => {
            this.router.navigateByUrl(this.routingConstantService.getListVehicleRouterLink());
          }, 2000);
        }
      }, (error) => {
        this.messageService.showProgressBar(false);
        this.utils.toastr.showError(error.json().error.message);
        if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
          setTimeout(() => {
            this.authService.logout();
          }, 2000);
        }
      });
    }
  }
});
    } else {
      this.showError = true;
    }
  }

  getModal() {
    this.vehicleModalRequestCommand.brand = this.updateVehicleCommand.brand;
    if (this.vehicleModalRequestCommand.brand) {
      this.messageService.showProgressBar(true);
      this.authService.getVehicleModels(this.vehicleModalRequestCommand).subscribe((type: VehicleModalCommand) => {
        this.messageService.showProgressBar(false);
        if (type.status === 'success') {
          this.modalArray = type.vehicleModels;
        }
      }, (error) => {
        try {
          this.messageService.showProgressBar(false);
          this.utils.toastr.showError(error.json().error.message);
          if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
            setTimeout(() => {
              this.authService.logout();
            }, 2000);
          }
        } catch (error) {
          this.responseFail = false;
        }
      });
    }
  }

  setVehicleType(type: string) {
    this.updateVehicleCommand.type = type;
    this.checkBlankField('type');
  }

  setVehicleBrand(brand: string) {
    this.updateVehicleCommand.brand = brand;
    // this.checkBlankField('brand');
    this.getModal();
  }

  setVehicleModel(model: string) {
    this.updateVehicleCommand.model = model;
    this.checkBlankField('model');
  }

  setVehicleYear(year: string) {
    this.updateVehicleCommand.year = year;
  }

  setFuelType(fuelType: string) {
    this.updateVehicleCommand.fuelType = fuelType;
  }

  setFuelUnit(fuelUnit: string) {
    this.updateVehicleCommand.fuelUnit = fuelUnit;
  }

  setDistanceUnit(distanceUnit: string) {
    this.updateVehicleCommand.distanceUnit = distanceUnit;
  }

  setStatus(status: string) {
    this.updateVehicleCommand.vehicleStatus = status;
  }

  checkBlankField(field: string, checkAllFields?: boolean) {
    if (checkAllFields) {

    } else {
      checkAllFields = false;
    }
    if (field === 'type' || checkAllFields) {
      if (this.updateVehicleCommand.type === null) {
        this.showError = true;
        this.typeError = true;
      } else {
        this.typeError = false;
      }
    }
    if (field === 'brand' || checkAllFields) {
      if (this.updateVehicleCommand.brand === null) {
        this.showError = true;
        this.brandError = true;
      } else {
        this.brandError = false;
      }
    }
    if (field === 'model' || checkAllFields) {
      if (this.updateVehicleCommand.model === null) {
        this.showError = true;
        this.modelError = true;
      } else {
        this.modelError = false;
      }
    }
    if (field === 'registrationNo' || checkAllFields) {
      if (this.updateVehicleCommand.registrationNumber === null) {
        this.showError = true;
        this.registrationNoError = true;
      } else {
        this.registrationNoError = false;
      }
    }
  }

  cancelButton() {
    this.router.navigateByUrl(this.routingConstantService.getListVehicleRouterLink());
  }

}
