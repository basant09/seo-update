import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ComingSoonComponent } from './coming-soon/coming-soon.component';
import { LockScreenComponent } from './lockscreen/lockscreen.component';
import { SubscribesComponent } from './subscribes/subscribes.component';
import { UnderMaintanceComponent } from './under-maintance/under-maintance.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SessionRoutes } from './session.routing';
import { AddUserComponent } from './add-user/add-user.component';
import { ListUserComponent } from './list-user/list-user.component';
import { CreateNewFleetBusinessComponent } from './create-new-fleet-business/create-new-fleet-business.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UiElementsModule } from '../ui-elements/ui-elements.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AddVehicleComponent } from './add-vehicle/add-vehicle.component';
import { ListVehicleComponent } from './list-vehicle/list-vehicle.component';
import { UpdateVehicleComponent } from './update-vehicle/update-vehicle.component';
import { FleetLocationFormComponent } from './common/fleet-location-form/fleet-location-form.component';
import { ListLocationComponent } from './list-location/list-location.component';
import { CreateRosterComponent } from './create-roster/create-roster.component';
import { ListRosterComponent } from './list-roster/list-roster.component';
import { UpdateRosterComponent } from './update-roster/update-roster.component';
import { DriverListModalComponent } from './modals/driver-list-modal/driver-list-modal.component';
import { VehicleListModalComponent } from './modals/vehicle-list-modal/vehicle-list-modal.component';
import { AddLocationComponent } from './add-location/add-location.component';
import { EditLocationComponent } from './edit-location/edit-location.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    UiElementsModule,
    NgxMyDatePickerModule.forRoot(),
    RouterModule.forChild(SessionRoutes)
  ],
  declarations: [
    ComingSoonComponent,
    LockScreenComponent,
    SubscribesComponent,
    UnderMaintanceComponent,
    NotFoundComponent,
    AddUserComponent,
    ListUserComponent,
    CreateNewFleetBusinessComponent,
    AddLocationComponent,
    EditUserComponent,
    UserProfileComponent,
    AddVehicleComponent,
    ListVehicleComponent,
    UpdateVehicleComponent,
    FleetLocationFormComponent,
    ListLocationComponent,
    CreateRosterComponent,
    ListRosterComponent,
    UpdateRosterComponent,
    DriverListModalComponent,
    VehicleListModalComponent,
    EditLocationComponent
  ],
  exports: [
    RouterModule
  ],
  entryComponents: [
    FleetLocationFormComponent,
    DriverListModalComponent,
    VehicleListModalComponent
  ]
})

export class SessionDemoModule { }
