import { MessageService } from '../../services/message.service';
import { RoutingConstantService } from '../../services/routingConstants.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { UserListDetailsCommand, DeactivateUserCommand } from '../../typings/Typings';
import { AuthService } from 'app/services/auth.service';
import { fadeInAnimation } from 'app/core/route-animation/route.animation';
import { ToastrComponent } from 'app/ui-elements/toastr/toastr.component';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { Utils } from 'app/utils/utils.util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationModalComponent } from '../../ui-elements/confirmation-modal/confirmation-modal.component';
import {any} from "codelyzer/util/function";

declare var $: any;
this.userFilterInput = any;

export interface TableHeading {
  name: string;
  email: string;
  userId: string;
  mobile: string;
  phone: string;
  status: string;
  licenseNumber: string;
  employeeId: string;

  // count: number;
  // count1: number;
  // count2: number;
  // count3: number;
}

@Component({
  selector: 'pap-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: { '[@fadeInAnimation]': 'true' },
  animations: [fadeInAnimation]
})

export class ListUserComponent implements OnInit {
  userFilterInput: any;
  visible: boolean;
  visible1: boolean;
  visible2: boolean;
  visible3: boolean;
  visible4: boolean;
  visible5: boolean;
  visible6: boolean;
  visible7: boolean;
  @ViewChild('myTable') table: any;

  userListDetailsCommand: UserListDetailsCommand[];
  temp: UserListDetailsCommand[];
  public columns: Array<any> = [
    { name: 'Name' },
    { name: 'Email' },
    { name: 'User ID' },
    { name: 'Mobile' },
    { name: 'Phone' },
    { name: 'Status' },
    { name: 'License Number' },
    { name: 'Employee ID' },
    { name: 'Edit' },
    { name: 'Deactive' }
  ];
  responseFail: boolean = false;
  responseMessage: string;
  responseSuccess: boolean = false;
  deactivateUserCommand: DeactivateUserCommand;

  constructor(private authService: AuthService, private router: Router, private routingConstantService: RoutingConstantService,
    private messageService: MessageService, private toastr: ToastrComponent, private pageTitleService: PageTitleService,
    private util: Utils, private modalService: NgbModal) {
    this.deactivateUserCommand = {
      userId: null,
      status: null
    }

    this.visible = false;
    this.visible1 = false;
    this.visible2 = false;
    this.visible3 = false;
    this.visible4 = false;
    this.visible5 = false;
    this.visible6 = false;
    this.visible7 = false;
  }

  ngOnInit() {
    this.pageTitleService.setTitle('User List');
    // $('.table').footable();
    this.loadDetails();
  }

  loadDetails() {
    this.removeError();
    this.messageService.showProgressBar(true);
    this.authService.listUser().subscribe((res: UserListDetailsCommand[]) => {
      this.userListDetailsCommand = res;
      this.userListDetailsCommand.forEach((element, index) => {
        if (!element.empId) {
          this.userListDetailsCommand[index].empId = '';
        }
        if (!element.mobile) {
          this.userListDetailsCommand[index].mobile = '';
        }
      });
      this.temp = this.userListDetailsCommand;
      this.messageService.showProgressBar(false);
      if (this.userListDetailsCommand.length < 1) {
        this.responseFail = true;
        this.responseMessage = 'No record found'
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      // this.responseFail = true;
      // this.responseMessage = error.json().error.message;
      this.util.toastr.showError(error.json().error.message);
      if (error.json().error.code === 'ERR_TOKEN_EXPIRED') {
        setTimeout(() => {
          this.authService.logout();
        }, 2000);
      }
    });
  }

  updateFilter() {
    const val = this.userFilterInput;
    if (val) {
      const temp = this.temp.filter(function (d) {
        return (d.firstName.toLowerCase().indexOf(val) !== -1 || d.lastName.toLowerCase().indexOf(val) !== -1 ||
          d.email.toLowerCase().indexOf(val) !== -1 || d.mobile.toLowerCase().indexOf(val) !== -1 ||
          d.status.toLowerCase().indexOf(val) !== -1 || d.licenseNumber.toLowerCase().indexOf(val) !== -1 ||
          d.empId.toLowerCase().indexOf(val) !== -1 || !val);
      });
      this.userListDetailsCommand = temp;
    }
  }
  searchFilter1(event) {
    const val = event.target.value;
    const temp = this.temp.filter(function (d) {
      return (d.status.toLowerCase().startsWith(val));
    });
    this.userListDetailsCommand = temp;
  }
  editUser(empId) {
    this.router.navigate([this.routingConstantService.getEditUserRouterLink()],
      { queryParams: { id: empId } });
  }

  updateStatus(userId) {
    this.removeError();
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.heading = 'Confirmation';
    modalRef.componentInstance.heading1 = 'Are you sure want to deactive user?';
    modalRef.result.then(res1 => {
      if (res1 === 1 ) {
        this.deactivateUserCommand.userId = userId;
        this.deactivateUserCommand.status = 'inactive';
        this.messageService.showProgressBar(true);
        this.authService.updateStatus(this.deactivateUserCommand).subscribe((res: any) => {
          this.messageService.showProgressBar(false);
          if (res.status === 'success') {
            this.util.toastr.showSuccess('User deactivated');
            setTimeout(() => {
              this.loadDetails();
            }, 2000);
          }
        }, (error) => {
          this.messageService.showProgressBar(false);
          this.util.toastr.showError(error.json().error.message);
        });
      }
    });

  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
  }

  removeError() {
    this.responseSuccess = false;
    this.responseFail = false;
    this.responseMessage = '';
  }

  filterAscOfEmail(element1, element2): number {
    try {
      if (element1.email.toLowerCase() > element2.email.toLowerCase()) {
        return -1;
      }
      if (element1.email.toLowerCase() < element2.email.toLowerCase()) {
        return 1;
      }
      if (element1.email.toLowerCase() === element2.email.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfName(element1, element2): number {
    try {
      if (element1.firstName.toLowerCase() > element2.firstName.toLowerCase()) {
        return -1;
      }
      if (element1.firstName.toLowerCase() < element2.firstName.toLowerCase()) {
        return 1;
      }
      if (element1.firstName.toLowerCase() === element2.firstName.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfUserId(element1, element2): number {
    try {
      if (element1.userId.toLowerCase() > element2.userId.toLowerCase()) {
        return -1;
      }
      if (element1.userId.toLowerCase() < element2.userId.toLowerCase()) {
        return 1;
      }
      if (element1.userId.toLowerCase() === element2.userId.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfMobile(element1, element2): number {
    try {
      if (element1.mobile.toLowerCase() > element2.mobile.toLowerCase()) {
        return -1;
      }
      if (element1.mobile.toLowerCase() < element2.mobile.toLowerCase()) {
        return 1;
      }
      if (element1.mobile.toLowerCase() === element2.mobile.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfPhone(element1, element2): number {
    try {
      if (element1.phone.toLowerCase() > element2.phone.toLowerCase()) {
        return -1;
      }
      if (element1.phone.toLowerCase() < element2.phone.toLowerCase()) {
        return 1;
      }
      if (element1.phone.toLowerCase() === element2.phone.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfStatus(element1, element2): number {
    try {
      if (element1.status.toLowerCase() > element2.status.toLowerCase()) {
        return -1;
      }
      if (element1.status.toLowerCase() < element2.status.toLowerCase()) {
        return 1;
      }
      if (element1.status.toLowerCase() === element2.status.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfLicenseNumber(element1, element2): number {
    try {
      if (element1.licenseNumber.toLowerCase() > element2.licenseNumber.toLowerCase()) {
        return -1;
      }
      if (element1.licenseNumber.toLowerCase() < element2.licenseNumber.toLowerCase()) {
        return 1;
      }
      if (element1.licenseNumber.toLowerCase() === element2.licenseNumber.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfEmployeeID(element1, element2): number {
    try {
      if (element1.empId.toLowerCase() > element2.empId.toLowerCase()) {
        return -1;
      }
      if (element1.empId.toLowerCase() < element2.empId.toLowerCase()) {
        return 1;
      }
      if (element1.empId.toLowerCase() === element2.empId.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }

  filterDescOfEmail(element1, element2): number {
    try {
      if (element1.email.toLowerCase() > element2.email.toLowerCase()) {
        return 1;
      }
      if (element1.email.toLowerCase() < element2.email.toLowerCase()) {
        return -1;
      }
      if (element1.email.toLowerCase() === element2.email.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfName(element1, element2): number {
    try {
      if (element1.firstName.toLowerCase() > element2.firstName.toLowerCase()) {
        return 1;
      }
      if (element1.firstName.toLowerCase() < element2.firstName.toLowerCase()) {
        return -1;
      }
      if (element1.firstName.toLowerCase() === element2.firstName.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfUserId(element1, element2): number {
    try {
      if (element1.userId.toLowerCase() > element2.userId.toLowerCase()) {
        return 1;
      }
      if (element1.userId.toLowerCase() < element2.userId.toLowerCase()) {
        return -1;
      }
      if (element1.userId.toLowerCase() === element2.userId.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfMobile(element1, element2): number {
    try {
      if (element1.mobile.toLowerCase() > element2.mobile.toLowerCase()) {
        return 1;
      }
      if (element1.mobile.toLowerCase() < element2.mobile.toLowerCase()) {
        return -1;
      }
      if (element1.mobile.toLowerCase() === element2.mobile.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfPhone(element1, element2): number {
    try {
      if (element1.phone.toLowerCase() > element2.phone.toLowerCase()) {
        return 1;
      }
      if (element1.phone.toLowerCase() < element2.phone.toLowerCase()) {
        return -1;
      }
      if (element1.phone.toLowerCase() === element2.phone.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfStatus(element1, element2): number {
    try {
      if (element1.status.toLowerCase() > element2.status.toLowerCase()) {
        return 1;
      }
      if (element1.status.toLowerCase() < element2.status.toLowerCase()) {
        return -1;
      }
      if (element1.status.toLowerCase() === element2.status.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfLicenseNumber(element1, element2): number {
    try {
      if (element1.licenseNumber.toLowerCase() > element2.licenseNumber.toLowerCase()) {
        return 1;
      }
      if (element1.licenseNumber.toLowerCase() < element2.licenseNumber.toLowerCase()) {
        return -1;
      }
      if (element1.licenseNumber.toLowerCase() === element2.licenseNumber.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfEmployeeID(element1, element2): number {
    try {
      if (element1.empId.toLowerCase() > element2.empId.toLowerCase()) {
        return 1;
      }
      if (element1.empId.toLowerCase() < element2.empId.toLowerCase()) {
        return -1;
      }
      if (element1.empId.toLowerCase() === element2.empId.toLowerCase()) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  sortEmail(status: boolean) {
    if (status) {
      this.userListDetailsCommand.sort(this.filterAscOfEmail);
    } else {
      this.userListDetailsCommand.sort(this.filterDescOfEmail);
    }
  }

  sortName(status: boolean) {
    if (status) {
      this.userListDetailsCommand.sort(this.filterAscOfName);
    } else {
      this.userListDetailsCommand.sort(this.filterDescOfName);
    }
  }
  sortUserId(status: boolean) {
    if (status) {
      this.userListDetailsCommand.sort(this.filterAscOfUserId);
    } else {
      this.userListDetailsCommand.sort(this.filterDescOfUserId);
    }
  }
  sortMobile(status: boolean) {
    if (status) {
      this.userListDetailsCommand.sort(this.filterAscOfMobile);
    } else {
      this.userListDetailsCommand.sort(this.filterDescOfMobile);
    }
  }
  sortPhone(status: boolean) {
    if (status) {
      this.userListDetailsCommand.sort(this.filterAscOfPhone);
    } else {
      this.userListDetailsCommand.sort(this.filterDescOfPhone);
    }
  }
  sortStatus(status: boolean) {
    if (status) {
      this.userListDetailsCommand.sort(this.filterAscOfStatus);
    } else {
      this.userListDetailsCommand.sort(this.filterDescOfStatus);
    }
  }
  sortLicenseNumber(status: boolean) {
    if (status) {
      this.userListDetailsCommand.sort(this.filterAscOfLicenseNumber);
    } else {
      this.userListDetailsCommand.sort(this.filterDescOfLicenseNumber);
    }
  }
  sortEmployeeID(status: boolean) {
    if (status) {
      this.userListDetailsCommand.sort(this.filterAscOfEmployeeID);
    } else {
      this.userListDetailsCommand.sort(this.filterDescOfEmployeeID);
    }
  }
  // sortName1() {
  //   this.count = this.count + 1;
  //   if ( this.count % 2 === 0) {
  //      this.sortName(true);
  //   } else {
  //     this.sortName(false);
  //   }
  // }
  // sortUserId1() {
  //   this.count1 = this.count1 + 1;
  //   if ( this.count1 % 2 === 0) {
  //      this.sortUserId(true);
  //   } else {
  //     this.sortUserId(false);
  //   }
  //  }
  //  sortEmail1() {
  //   this.count2 = this.count2 + 1;
  //   if ( this.count2 % 2 === 0) {
  //      this.sortEmail(true);
  //   } else {
  //     this.sortEmail(false);
  //   }
  //  }
  //  sortMobile1() {
  //   this.count2 = this.count2 + 1;
  //   if ( this.count2 % 2 === 0) {
  //      this.sortMobile(true);
  //   } else {
  //     this.sortMobile(false);
  //   }
  //  }
  //  sortPhone1() {
  //   this.count3 = this.count3 + 1;
  //   if ( this.count3 % 2 === 0) {
  //      this.sortPhone(true);
  //   } else {
  //     this.sortPhone(false);
  //   }
  //  }
  //  sortStatus1() {
  //   this.count4 = this.count4 + 1;
  //   if ( this.count4 % 2 === 0) {
  //      this.sortStatus(true);
  //   } else {
  //     this.sortStatus(false);
  //   }
  //  }
  //  sortLicenseNumber1() {
  //   this.count5 = this.count5 + 1;
  //   if ( this.count5 % 2 === 0) {
  //      this.sortLicenseNumber(true);
  //   } else {
  //     this.sortLicenseNumber(false);
  //   }
  // }
  // sortEmployeeID1() {
  //   this.count6 = this.count6 + 1;
  //   if ( this.count6 % 2 === 0) {
  //      this.sortEmployeeID(true);
  //   } else {
  //     this.sortEmployeeID(false);
  //   }
  // }
  toggleName() {
    this.visible = !this.visible;
  }
  toggleEmail() {
    this.visible1 = !this.visible1;
  }
  toggleUserId() {
    this.visible2 = !this.visible2;
  }
  toggleMobile() {
    this.visible3 = !this.visible3;
  }
  togglePhone() {
    this.visible4 = !this.visible4;
  }
  toggleStatus() {
    this.visible5 = !this.visible5;
  }
  toggleLicenseNumber() {
    this.visible6 = !this.visible6;
  }
  toggleEmployeeID() {
    this.visible7 = !this.visible7;
  }
}
