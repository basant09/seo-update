import { Component, OnInit, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { MessageService } from '../../services/message.service';
import { RoutingConstantService } from '../../services/routingConstants.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import {
  GetFleetLocationsCommand, RemoveFleetLocationCommand
} from 'app/typings/Typings';
import { PageTitleService } from 'app/core/page-title/page-title.service';
import { Utils } from '../../utils/utils.util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationModalComponent } from '../../ui-elements/confirmation-modal/confirmation-modal.component';

@Component({
  selector: 'pap-list-location',
  templateUrl: './list-location.component.html',
  styleUrls: ['./list-location.component.scss']
})
export class ListLocationComponent implements OnInit {
  responseSuccess: boolean;
  showError: boolean;
  responseFail: boolean;
  responseMessage: string;
  noOfLocations: number;
  visible: boolean;
  visible1: boolean;
  visible2: boolean;
  fleetLocationsCommand: GetFleetLocationsCommand[];
  isAnyLocationPresent: boolean;
  fleetLocationsCommandTemp: GetFleetLocationsCommand[];
  removeFleetLocationCommand: RemoveFleetLocationCommand;

  constructor(private authService: AuthService, private router: Router,
    private routingConstantService: RoutingConstantService, private messageService: MessageService,
    private pageTitleService: PageTitleService, private componentFactoryResolver: ComponentFactoryResolver,
    private viewContainerRef: ViewContainerRef, public utils: Utils, private modalService: NgbModal) {
    this.noOfLocations = 0;
    this.visible = false;
    this.visible1 = false;
    this.visible2 = false;
    this.removeFleetLocationCommand = {
      locationId: null
    }
  }


  ngOnInit() {
    this.pageTitleService.setTitle('View Location');
    this.getProfileLocationList();
  }



  getProfileLocationList() {
    setTimeout(() => {
      this.removeError();
    }, 2000);
    this.messageService.showProgressBar(true);
    this.authService.getFleetLocations().subscribe((res: GetFleetLocationsCommand[]) => {
      this.messageService.showProgressBar(false);
      this.fleetLocationsCommand = res;
      this.fleetLocationsCommandTemp = res;
      if (this.fleetLocationsCommand.length > 0) {
        this.isAnyLocationPresent = true;
      }
    }, (error) => {
      this.messageService.showProgressBar(false);
      this.utils.toastr.showError(error.json().error.message);
      this.utils.auth.isTokenExpired(error);
    });
  }

  getLocationDetailById(locationId): GetFleetLocationsCommand {
    let location: GetFleetLocationsCommand;
    if (locationId === 0 || locationId === null || locationId === undefined) {
      location = {
        addressLine: null,
        city: null,
        contactPerson: null,
        contactPersonAltContact: null,
        contactPersonEmail: null,
        contactPersonMobile: null,
        contactPersonPhone: null,
        country: null,
        isHeadOffice: null,
        latitude: null,
        locationName: null,
        longitude: null,
        postCode: null,
        state: null,
        suburb: null,
        locationId: null
      };
    } else {
      this.fleetLocationsCommand.forEach((element, index) => {
        if (element.locationId === locationId) {
          location = element;
        }
      });
    }
    return location;
  }

  updateLocation(locationId) {
    this.router.navigate([this.routingConstantService.getEditLocationRouterLink()],
      { queryParams: { id: locationId } });
  }

  removeLocation(locationId) {
    this.removeFleetLocationCommand.locationId = locationId;
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.heading = 'Confirmation';
    modalRef.componentInstance.heading1 = 'Are you sure want to remove location ?';
    modalRef.result.then(res1 => {
      if (res1 !== undefined) {
        if (res1 === 1) {
          this.messageService.showProgressBar(true);
          this.authService.removeFleetLocation(this.removeFleetLocationCommand).subscribe((res: any) => {
            this.messageService.showProgressBar(false);
            this.utils.toastr.showSuccess('Location removed successfully');
            this.getProfileLocationList();
          }, (error) => {
            this.messageService.showProgressBar(false);
            this.utils.toastr.showError(error.json().error.message);
            this.utils.auth.isTokenExpired(error);
          });
        }
      }
    })
  }

  removeError() {
    this.responseSuccess = false;
    this.responseFail = false;
  }
  searchFilter(event) {
    const val = event.target.value;
    const temp = this.fleetLocationsCommandTemp.filter((d) => {
      return (d.locationName.toLowerCase().indexOf(val) !== -1 || d.addressLine.toLowerCase().indexOf(val) !== -1 || !val);
    });
    this.fleetLocationsCommand = temp;
  }
  filterAscOfName(element1, element2): number {
    try {
      if (element1.locationName > element2.locationName) {
        return -1;
      }
      if (element1.locationName < element2.locationName) {
        return 1;
      }
      if (element1.locationName === element2.locationName) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfContactPerson(element1, element2): number {
    try {
      if (element1.contactPerson > element2.contactPerson) {
        return -1;
      }
      if (element1.contactPerson < element2.contactPerson) {
        return 1;
      }
      if (element1.contactPerson === element2.contactPerson) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterAscOfAddress(element1, element2): number {
    try {
      if (element1.addressLine > element2.addressLine) {
        return -1;
      }
      if (element1.addressLine < element2.addressLine) {
        return 1;
      }
      if (element1.addressLine === element2.addressLine) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfName(element1, element2): number {
    try {
      if (element1.locationName > element2.locationName) {
        return 1;
      }
      if (element1.locationName < element2.locationName) {
        return -1;
      }
      if (element1.locationName === element2.locationName) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfAddress(element1, element2): number {
    try {
      if (element1.addressLine > element2.addressLine) {
        return 1;
      }
      if (element1.addressLine < element2.addressLine) {
        return -1;
      }
      if (element1.addressLine === element2.addressLine) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  filterDescOfContactPerson(element1, element2): number {
    try {
      if (element1.contactPerson > element2.contactPerson) {
        return 1;
      }
      if (element1.contactPerson < element2.contactPerson) {
        return -1;
      }
      if (element1.contactPerson === element2.contactPerson) {
        return 0;
      }
    } catch (error) {
      return 0;
    }
  }
  sortName(status: boolean) {
    if (status) {
      this.fleetLocationsCommand.sort(this.filterAscOfName);
    } else {
      this.fleetLocationsCommand.sort(this.filterDescOfName);
    }
  }
  sortAddress(status: boolean) {
    if (status) {
      this.fleetLocationsCommand.sort(this.filterAscOfAddress);
    } else {
      this.fleetLocationsCommand.sort(this.filterDescOfAddress);
    }
  }
  sortContactName(status: boolean) {
    if (status) {
      this.fleetLocationsCommand.sort(this.filterAscOfContactPerson);
    } else {
      this.fleetLocationsCommand.sort(this.filterDescOfContactPerson);
    }
  }
  toggleName() {
    this.visible = !this.visible;
  }
  toggleAddress() {
    this.visible1 = !this.visible1;
  }
  toggleContactName() {
    this.visible2 = !this.visible2;
  }

}
