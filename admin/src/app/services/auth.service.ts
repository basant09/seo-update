import { Injectable, OnInit } from '@angular/core';
import { UrlService } from './url.service';
import { Observable } from 'rxjs/Observable';
import { BaseHttpService } from './baseHttp.service';
import { Constants } from '../utils/constants.util';
import { CustomQueryEncoder } from '../utils/customQueryEncoder.util';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {
    FireBaseResponseCommand, FireBaseRequestCommand, LoginResponseCommand, UserRegisterCommand,
    AddUserCommand, UserListDetailsCommand, CreateFleetProfileCommand, GetFleetUserDetailCommand,
    FleetUserDetailCommand, DeactivateUserCommand, GetDefaultFuelRestrictionCommand, DefaultExpenseLimitCommand,
    VehicleTypeCommand, VehicleBrandsCommand, FuelTypesCommand, DefaultDistanceUnitCommand, DefaultFuleUnitCommand,
    VehicleDetailsCommand, AddVehicleCommand, AddVehicleResponseCommand, UpdateVehicleCommand, EmployeeTypeCommand,
    VehicleModalRequestCommand, VehicleModalCommand, GetVehicleDetailsCommand, SupportedCountriesCommand,
    StatesCommand, StatesRequestCommand, RosterAvailableUser, RosterAvailableVehicle, RosterRequestCommand,
    RosterListCommand,
    RosterListRequestCommand,
    RosterDetilsRequestCommand,
    CreateRosterCommand,
    UpdateFleetProfileCommand,
    AddFleetLocationCommand,
    UpdateFleetLocationCommand,
    RemoveFleetLocationCommand,
    FleetProfileDetailCommand,
    GetFleetLocationsCommand,
    UpdateRosterCommand,
    UpdateRosterStatusCommand,
    AddUserSuccessResponseComamnd,
    RosterStatusCommand,
    RosterOccurrencesCommand,
    GetFleetLocationCommand,
    SetDefaultFuelRestrictionCommand,
    SetDefaultExpenseLimitCommand
} from '../typings/Typings';
import { RoutingConstantService } from 'app/services/routingConstants.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthService extends BaseHttpService implements OnInit {

    user: Observable<firebase.User>;
    private authToken: string;
    private readonly authTokenKey = Constants.authTokenKey;
    fireBaseResponseCommand: FireBaseResponseCommand;

    constructor(private urlService: UrlService, private http: Http, private firebaseAuth: AngularFireAuth,
        private routingConstantService: RoutingConstantService, private router: Router) {
        super();
        this.user = firebaseAuth.authState;
        this.authToken = localStorage.getItem(this.authTokenKey);
        this.fireBaseResponseCommand = {
            message: null,
            status: null,
            code: null
        }
    }

    ngOnInit() { }

    public getAuthToken(): any {
        return this.authToken;
    }

    public tokenFallback(): boolean {
        let status: boolean;
        const user = this.firebaseAuth.auth.currentUser;
        user.getIdToken().then(res => {
            this.authToken = res;
            localStorage.setItem(this.authTokenKey, res);
            this.authToken = res;
        });
        if (this.authToken !== null) {
            status = true;
        } else {
            status = false;
        }
        return status;
    }

    public getFireBaseAuthToken() {
        const user = this.firebaseAuth.auth.currentUser;
        user.getIdToken().then(res => {
            this.authToken = res;
            this.setAuthToken(res);
        });
    }

    public setAuthToken(token: string) {
        if (token !== null && token !== '') {
            localStorage.setItem(this.authTokenKey, token);
            this.authToken = token;
        }
    }

    public removeAuthToken() {
        localStorage.removeItem(this.authTokenKey);
        this.firebaseAuth.auth.signOut();
        this.authToken = null;
    }

    public getAuthHeaders(getTokenFromFireBase?: boolean): Headers {
        const headers = new Headers();
        if (this.authToken != null) {
            headers.append(this.authTokenKey, this.authToken);
        } else {
            const user = this.firebaseAuth.auth.currentUser;
            user.getIdToken().then(res => {
                headers.append(this.authTokenKey, res);
            });
        }
        return headers;
    }

    forgotPassword(email: string) {
        this.firebaseAuth.auth.sendPasswordResetEmail(email).then((res: any) => {
        }).catch((error) => {
            console.log(error);
        });
    }

    firebasePersistance(email: string, password: string) {
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
            .then(function () {
                return firebase.auth().signInWithEmailAndPassword(email, password);
            })
            .catch(function (error) {
            });
    }

    signup(command: FireBaseRequestCommand): Promise<FireBaseResponseCommand> {
        return this.firebaseAuth.auth.createUserWithEmailAndPassword(command.email, command.password).then(value => {
            this.getFireBaseAuthToken();
            this.fireBaseResponseCommand.code = '';
            this.fireBaseResponseCommand.status = true;
            this.fireBaseResponseCommand.message = '';
            return this.fireBaseResponseCommand;
        }).catch(err => {
            this.fireBaseResponseCommand.status = false;
            this.fireBaseResponseCommand.code = err.code;
            this.fireBaseResponseCommand.message = err.message;
            return this.fireBaseResponseCommand;
        });
    }

    login(command: FireBaseRequestCommand): Promise<FireBaseResponseCommand> {
        return this.firebaseAuth.auth.signInWithEmailAndPassword(command.email.toString(), command.password.toString()).then(value => {
            this.getFireBaseAuthToken();
            this.fireBaseResponseCommand.status = true;
            this.fireBaseResponseCommand.message = 'Login Successfull';
            return this.fireBaseResponseCommand;
        }).catch(error => {
            this.fireBaseResponseCommand.status = false;
            this.fireBaseResponseCommand.message = error.message;
            this.fireBaseResponseCommand.code = error.code;
            return this.fireBaseResponseCommand;
        });
    }

    userLogin(): Observable<LoginResponseCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.userLogin(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error;
        });
    }

    logout() {
        this.firebaseAuth.auth.signOut();
        this.removeAuthToken();
        this.router.navigateByUrl(this.routingConstantService.getLoginRouterLink());
    }

    registerBusinessAdmin(command: UserRegisterCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.registerBusinessAdmin(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error;
        });
    }

    deleteUserFromFireBase() {
        const user = this.firebaseAuth.auth.currentUser;
        user.delete();
    }

    sendPasswordResetEmail(email) {
        this.firebaseAuth.auth.sendPasswordResetEmail(email).then(res => {
        }).catch(error => {
        });
    }

    addUser(command: AddUserCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.addUser(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    listUser(): Observable<UserListDetailsCommand[]> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.listUser(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
        });
    }

    createFleetProfile(command: CreateFleetProfileCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.createFleetProfile(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })
    }

    getFleetUserDetail(command: GetFleetUserDetailCommand): Observable<FleetUserDetailCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.getFleetUserDetail(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })
    }

    updateUser(command: AddUserCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.editUser(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    updateStatus(command: DeactivateUserCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.updateStatus(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getDefaultFuelRestriction(): Observable<GetDefaultFuelRestrictionCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getDefaultFuelRestriction(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getDefaultExpenseLimit(): Observable<DefaultExpenseLimitCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getDefaultExpenseLimit(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getVehicleTypes(): Observable<VehicleTypeCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getVehicleTypes(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getVehicleBrands(): Observable<VehicleBrandsCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getVehicleBrands(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getFuelTypes(): Observable<FuelTypesCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getFuelTypes(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getDefaultDistanceUnit(): Observable<DefaultDistanceUnitCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getDefaultDistanceUnit(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getDefaultFuelUnit(): Observable<DefaultFuleUnitCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getDefaultFuelUnit(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getVehicleList(): Observable<VehicleDetailsCommand[]> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getVehicleList(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    addVehicle(command: AddVehicleCommand): Observable<AddVehicleResponseCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.addVehicle(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    updateVehicle(command: UpdateVehicleCommand): Observable<AddVehicleResponseCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.updateVehicle(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getEmployeeType(): Observable<EmployeeTypeCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getEmployeeType(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getVehicleModels(command: VehicleModalRequestCommand): Observable<VehicleModalCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.getVehicleModels(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getVehicleDetails(command: GetVehicleDetailsCommand): Observable<AddVehicleCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.getVehicleDetails(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getSupportedCountries(): Observable<SupportedCountriesCommand[]> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getSupportedCountries(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getStates(command: StatesRequestCommand): Observable<StatesCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.getStates(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getAvailableUsers(command: RosterRequestCommand): Observable<RosterAvailableUser[]> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.getAvailableUsers(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })
    }

    getAvailableVehicles(command: RosterRequestCommand): Observable<RosterAvailableVehicle[]> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.getAvailableVehicles(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })
    }

    getRosterList(command: RosterListRequestCommand): Observable<RosterListCommand[]> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.getRosterList(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getRosterDetails(command: RosterDetilsRequestCommand): Observable<CreateRosterCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.getRosterDetails(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    createRoster(command: CreateRosterCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.createRoster(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    updateRoster(command: UpdateRosterCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.updateRoster(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    createProfile(command: CreateFleetProfileCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.createProfile(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    updateProfile(command: UpdateFleetProfileCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.updateProfile(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    addFleetLocation(command: AddFleetLocationCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.addFleetLocation(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    updateFleetLocation(command: UpdateFleetLocationCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.updateFleetLocation(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    removeFleetLocation(command: RemoveFleetLocationCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.removeFleetLocation(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getFleetProfile(): Observable<FleetProfileDetailCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getFleetProfile(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        });
    }

    getFleetLocations(): Observable<GetFleetLocationsCommand[]> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getFleetLocations(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })

    }

    updateRosterStatus(command: UpdateRosterStatusCommand): Observable<AddUserSuccessResponseComamnd> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.updateRosterStatus(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })

    }

    getRosterStatus(): Observable<RosterStatusCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getRosterStatus(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })
    }

    getRosterOccurrences(): Observable<RosterOccurrencesCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.get(this.urlService.auth.getRosterOccurrences(), requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })
    }

    getFleetLocationById(command: GetFleetLocationCommand): Observable<AddFleetLocationCommand> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.getFleetLocationById(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })
    }

    setDefaultFuelRestriction(command: SetDefaultFuelRestrictionCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.setDefaultFuelRestriction(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })
    }

    setDefaultExpenseLimit(command: SetDefaultExpenseLimitCommand): Observable<any> {
        const requestOptions = new RequestOptions();
        requestOptions.headers = this.getAuthHeaders();
        return this.http.post(this.urlService.auth.setDefaultExpenseLimit(), command, requestOptions).map((res: Response) => {
            return res.json();
        }, (error) => {
            return error.json();
        })
    }

}
