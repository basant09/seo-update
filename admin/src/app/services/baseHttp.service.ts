import { Headers } from '@angular/http';

export class BaseHttpService {

    loginHeaders(): Headers {
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return headers;
    }

}
