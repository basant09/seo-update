import { Injectable } from '@angular/core';
import { SERVER_URL, SERVER_BASE_URL } from '../../environments/environment';

@Injectable()
export class UrlService {

    public auth: Auth;

    constructor() {
        this.auth = new Auth();
    }
}

class Auth {
    private readonly fleet = 'fleet';
    private readonly customer = 'customer';
    private readonly vehicle = 'vehicle';
    private readonly profile = 'profile';
    private readonly common = 'common';
    private readonly roster = 'roster';

    public userLogin(): string {
        return `${SERVER_URL}/${this.fleet}/${this.customer}/userLogin`;
    }

    public registerBusinessAdmin(): string {
        return `${SERVER_URL}/${this.fleet}/${this.customer}/registerBusinessAdmin`;
    }

    public addUser(): string {
        return `${SERVER_URL}/${this.fleet}/${this.customer}/addUser`;
    }

    public listUser(): string {
        return `${SERVER_URL}/${this.fleet}/${this.customer}/getUserList`;
    }

    public createFleetProfile(): string {
        return `${SERVER_URL}/${this.fleet}/${this.customer}/createFleetProfile`;
    }

    public getFleetUserDetail(): string {
        return `${SERVER_URL}/${this.fleet}/${this.customer}/getUserDetails`;
    }

    public editUser(): string {
        return `${SERVER_URL}/${this.fleet}/${this.customer}/updateUser`;
    }

    public updateStatus(): string {
        return `${SERVER_URL}/${this.fleet}/${this.customer}/updateStatus`;
    }

    public getDefaultFuelRestriction(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/getDefaultFuelRestriction`;
    }

    public getDefaultExpenseLimit(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/getDefaultExpenseLimit`;
    }

    public addVehicle(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/addVehicle`;
    }

    public getVehicleTypes(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/getVehicleTypes`;
    }

    public getVehicleBrands(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/getVehicleBrands`;
    }

    public getFuelTypes(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/getFuelTypes`;
    }

    public getDefaultFuelUnit(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/getDefaultFuelUnit`;
    }

    public getDefaultDistanceUnit(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/getDefaultDistanceUnit`;
    }

    public getVehicleList(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/getVehicleList`;
    }

    public updateVehicle(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/updateVehicle`;
    }

    public deactivateVehicle(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/deactivateVehicle`;
    }

    public getEmployeeType(): string {
        return `${SERVER_URL}/${this.fleet}/${this.customer}/getEmpType`;
    }

    public getVehicleModels(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/getVehicleModels`;
    }

    public getVehicleDetails(): string {
        return `${SERVER_URL}/${this.fleet}/${this.vehicle}/getVehicleDetails`;
    }

    public getSupportedCountries(): string {
        return `${SERVER_URL}/${this.common}/getSupportedCountries`;
    }

    public getStates(): string {
        return `${SERVER_URL}/${this.common}/getStates`;
    }

    public getAvailableUsers(): string {
        return `${SERVER_URL}/${this.fleet}/${this.roster}/getAvailableUsers`;
    }

    public getAvailableVehicles(): string {
        return `${SERVER_URL}/${this.fleet}/${this.roster}/getAvailableVehicles`;
    }

    public createRoster(): string {
        return `${SERVER_URL}/${this.fleet}/${this.roster}/createRoster`;
    }

    public getRosterList(): string {
        return `${SERVER_URL}/${this.fleet}/${this.roster}/getRosterList`;
    }

    public updateRoster(): string {
        return `${SERVER_URL}/${this.fleet}/${this.roster}/updateRoster`;
    }

    public getRosterDetails(): string {
        return `${SERVER_URL}/${this.fleet}/${this.roster}/getRosterDetails`;
    }

    public createProfile(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/createProfile`;
    }

    public addFleetLocation(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/addFleetLocation`;
    }

    public updateFleetLocation(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/updateFleetLocation`;
    }

    public removeFleetLocation(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/removeFleetLocation`;
    }

    public updateProfile(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/updateProfile`;
    }

    public getFleetProfile(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/getProfile`;
    }

    public getFleetLocations(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/getFleetLocations`;
    }

    public updateRosterStatus(): string {
        return `${SERVER_URL}/${this.fleet}/${this.roster}/updateRosterStatus`;
    }

    public getRosterStatus(): string {
        return `${SERVER_URL}/${this.fleet}/${this.roster}/getRosterStatuses`;
    }

    public getRosterOccurrences(): string {
        return `${SERVER_URL}/${this.fleet}/${this.roster}/getRosterOccurrences`;
    }    
    
    public getFleetLocationById(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/getFleetLocationById`;
    }

    public setDefaultExpenseLimit(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/setDefaultExpenseLimit`;
    }

    public setDefaultFuelRestriction(): string {
        return `${SERVER_URL}/${this.fleet}/${this.profile}/setDefaultFuelRestriction`;
    }
}

