import { Injectable } from '@angular/core';

@Injectable()
export class RoutingConstantService {

    private _BASE_URL = '/';
    private _loginRouterLink = '/login';
    private _dashboardRouterLink = '/dashboard';
    private _addUserRouterLink = '/session/add-user';
    private _listUserRouterLink = '/session/list-user';
    private _editUserRouterLink = '/session/edit-user';
    private _createNewFleetBusinessRouterLink = '/session/create-fleet';
    private _listFleetBusinessRouterLink = '/session/list-fleet';
    private _userProfileRouterLink = '/session/user-profile';
    private _addVehicleRouterLink = '/session/add-vehicle';
    private _listVehicleRouterLink = '/session/list-vehicle';
    private _updateVehicleRouterLink = '/session/update-vehicle';
    private _createRosterRouterLink = '/session/create-roster';
    private _listRosterRouterLink = '/session/list-roster';
    private _updateRosterRouterLink = '/session/update-roster';
    private _listLocationRouterLink = '/session/list-location';
    private _editLocationRouterLink = '/session/edit-location';


    public getBaseUrlRouterLink(): string {
        return this._BASE_URL;
    }

    public getLoginRouterLink(): string {
        return this._loginRouterLink;
    }

    public getDashboardRouterLink(): string {
        return this._dashboardRouterLink;
    }

    public getAddUserRouterLink(): string {
        return this._addUserRouterLink;
    }

    public getListUserRouterLink(): string {
        return this._listUserRouterLink;
    }

    public getEditUserRouterLink(): string {
        return this._editUserRouterLink;
    }

    public getCreateNewFleetBusinessRouterLink(): string {
        return this._createNewFleetBusinessRouterLink;
    }

    public getListFleetBusinessRouterLink(): string {
        return this._listFleetBusinessRouterLink;
    }

    public getUserProfileRouterLink(): string {
        return this._userProfileRouterLink;
    }

    public getAddVehicleRouterLink(): string {
        return this._addVehicleRouterLink;
    }

    public getListVehicleRouterLink(): string {
        return this._listVehicleRouterLink;
    }

    public getUpdateVehicleRouterLink(): string {
        return this._updateVehicleRouterLink;
    }

    public getCreateRosterRouterLink(): string {
        return this._createRosterRouterLink;
    }

    public getListRosterRouterLink(): string {
        return this._listRosterRouterLink;
    }

    public getUpdateRosterRouterLink(): string {
        return this._updateRosterRouterLink;
    }

    public getListLocationRouterLink(): string {
        return this._listLocationRouterLink;
    }

    public getEditLocationRouterLink(): string {
        return this._editLocationRouterLink;
    }
}
