import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class MessageService {

    private progressBar = new Subject<any>();
    private fleetLocationFormError = new Subject<any>();
    private fleetLocationFormValid = new Subject<any>();
    private messageArray = new Subject<string[]>();

    progressBar$ = this.progressBar.asObservable();
    fleetLocationFormError$ = this.fleetLocationFormError.asObservable();
    fleetLocationFormValid$ = this.fleetLocationFormValid.asObservable();
    messageArray$ = this.messageArray.asObservable();


    showProgressBar(message: boolean) {
        this.progressBar.next(message);
    }

    showProgressBarGet(): Observable<boolean> {
        return this.progressBar.asObservable();
    }

    checkFleetLocationFormError(message: boolean) {
        this.fleetLocationFormError.next(message);
    }

    checkFleetLocationFormErrorGet(): Observable<boolean> {
        return this.fleetLocationFormError.asObservable();
    }

    fleetLocationFormValidation(message: boolean) {
        this.fleetLocationFormValid.next(message);
    }

    isFleetLocationFormValid(): Observable<boolean> {
        return this.fleetLocationFormValid.asObservable();
    }

    sendMessageArray(array: string[]) {
        this.messageArray.next(array);
    }

    getMessageArray(): Observable<string[]> {
        return this.messageArray.asObservable();
    }
}
