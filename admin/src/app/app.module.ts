import { DashboardModule } from './dashboard/dashboard.module';
import { AuthModule } from './auth/auth.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule, Http } from '@angular/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AgmCoreModule } from '@agm/core';

import { PerfectScrollbarModule, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { SidebarModule } from 'ng-sidebar';
import { Ng2BreadcrumbModule, BreadcrumbService } from 'ng2-breadcrumb/ng2-breadcrumb';
import 'hammerjs';

import { ChankyaAppComponent } from './app.component';
import { AppRoutes } from './app-routing.module';
import { MainComponent } from './main/main.component';
import { AuthComponent } from './auth/auth.component';
import { MenuToggleModule } from './core/menu/menu-toggle.module';
import { MenuItems } from './core/menu/menu-items/menu-items';
import { PageTitleService } from './core/page-title/page-title.service';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthService } from './services/auth.service';
import { UrlService } from './services/url.service';
import { BaseHttpService } from './services/baseHttp.service';
import { RoutingConstantService } from './services/routingConstants.service';
import { CustomFormsModule } from 'ng2-validation';
import { UiElementsModule } from './ui-elements/ui-elements.module';
import { SessionDemoModule } from './session/session.module';
import { MessageService } from 'app/services/message.service';
import { Utils } from 'app/utils/utils.util';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

export function createTranslateLoader(http: Http) {
	return new TranslateStaticLoader(http, 'assets/i18n', '.json');
}

const perfectScrollbarConfig: PerfectScrollbarConfigInterface = {
	suppressScrollX: true
};

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		MaterialModule,
		SidebarModule.forRoot(),
		RouterModule.forRoot(AppRoutes),
		FlexLayoutModule,
		Ng2BreadcrumbModule.forRoot(),
		AgmCoreModule.forRoot({ apiKey: 'AIzaSyBtdO5k6CRntAMJCF-H5uZjTCoSGX95cdk' }),
		PerfectScrollbarModule.forRoot(perfectScrollbarConfig),
		MenuToggleModule,
		HttpModule,
		TranslateModule.forRoot({
			provide: TranslateLoader,
			useFactory: (createTranslateLoader),
			deps: [Http]
		}),
		AngularFireModule.initializeApp(environment.firebase),
		AngularFireAuthModule,
		CustomFormsModule,
		UiElementsModule,
		SessionDemoModule,
		AuthModule,
		DashboardModule,
		ToastrModule,
		NgbModule.forRoot()
	],
	exports: [RouterModule],
	declarations: [
		ChankyaAppComponent,
		MainComponent,
	],
	entryComponents: [
	],
	bootstrap: [ChankyaAppComponent],
	providers: [
		MenuItems,
		BreadcrumbService,
		PageTitleService,
		AuthService,
		BaseHttpService,
		UrlService,
		RoutingConstantService,
		MessageService,
		Utils
	]
})
export class ChankyaAppModule { }
