import { MainComponent } from '../main/main.component';
import { AuthGuard } from '../auth/auth.guard';
import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard-v1/dashboard.component';

export const DashboardRoutes: Routes = [{
  path: 'dashboard',
  component: MainComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: '',
      component: DashboardComponent
    }
  ]
}];

