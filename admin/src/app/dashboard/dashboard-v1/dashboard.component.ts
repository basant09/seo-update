import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { PageTitleService } from '../../core/page-title/page-title.service';
import {fadeInAnimation} from "../../core/route-animation/route.animation";
import * as Ps from 'perfect-scrollbar';
import { Router } from '@angular/router';
import { RoutingConstantService } from '../../services/routingConstants.service';

function getNewTime(d){
  let h = (d.getHours()<10?'0':'') + d.getHours(),
      m = (d.getMinutes()<10?'0':'') + d.getMinutes(),
      s = (d.getSeconds()<10?'0':'') + d.getSeconds(),
      time = h + ":" + m + ":" + s;
  return time;
}

@Component({
  selector: 'pap-ms-dashboard',
  templateUrl:'./dashboard-component.html',
  styleUrls: ['./dashboard-component.scss'],
  encapsulation: ViewEncapsulation.None,
    host: {
        "[@fadeInAnimation]": 'true'
    },
    animations: [ fadeInAnimation ]
})
export class DashboardComponent implements OnInit {

  constructor( private pageTitleService: PageTitleService, private router: Router, private routingConstantService: RoutingConstantService) {

  }

  ngOnInit() {
    this.pageTitleService.setTitle("Home");
    this.router.navigateByUrl(this.routingConstantService.getCreateNewFleetBusinessRouterLink());
  }

}














